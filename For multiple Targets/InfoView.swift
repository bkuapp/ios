//
//  InfoView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Info Ansicht, welche bei den WIdgets verwendet wird, um dort Fehlermeldungen oder andere Informationen anzuzeigen.
 */
struct InfoView: View {
    /**
     Die Größe der Ansicht, welche der Größe des Widgets entspricht.
     */
    let size: CGSize
    /**
     Der Text der Ansicht, welcher dem Nutzer angezeigt wird.
     */
    let text: String
    
    /**
     Der Körper von der Info Anicht, welche die Struktur der Info Ansicht wiedergibt.
     */
    var body: some View {
        Text(text)
        .multilineTextAlignment(.center)
        .font(.system(size: 20))
        .frame(height: size.height)
    }
}
