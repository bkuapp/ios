//
//  NetworkHandler.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import UIKit

/**
 Lädt Daten vom Server.
 */
class NetworkHandler {
    /**
    Der Typ der Antwortsmethode, welche am Ende des Ladevorgangs aufgerufen wird.
    */
    typealias Action = (Data?) -> ()
    
    /**
     Führt eine bestimmte Serveranfrage aus und lädt die Daten, welche als Antwort zurückkommen.
     - Parameters:
        - typeIndex: der Typ des Serveranfrage.
        - queryParams: die Parameter, welche mit dem Request verschickt werden.
        - completitionHandler: die Methode, welche aufgerufen wird, wenn die Antwort vom Server vollständig runtergeladen wurde.
     */
    static func load(_ typeIndex: Int, queryParams: [String: Any], completitionHandler: Action? = nil) {
        
        var queryParams = queryParams
        queryParams["type"] = typeIndex
        
        guard let url = URL(string: "https://app.bkukr.de") else {
            return
        }
        
        let paramsString = queryParams.percentEscaped()
        
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = paramsString.data(using: .utf8)
        
        if (!queryParams.keys.contains("password")) {
            NSLog(paramsString)
        }
        // Abfrage an dem Server
        URLSession.shared.dataTask(with: request) { (data, _, _) in
            if let handler = completitionHandler {
                handler(data)
            }
        }.resume()
    }
}
