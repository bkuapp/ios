//
//  JSONHandler.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import UIKit
import Network

/**
 Kovertiert eine JSON Antwort vom Server zu einem Objekt.
 */
public class JSONHandler {
    /**
     Der Typ der Antwortsmethode, welche am Ende des Konvertiervorgangs aufgerufen wird.
     
     - parameter T: der Typ des Objektes zu welchem die JSON Antwort konvertiert werden soll.
     */
    typealias Action<T> = (T) -> ()
    
    /**
     Konvertiert eine Serverantwort für den angegeben Typ zu einem Objekt vom Typ T und ruft mit ihm die Abschlussmethode auf.

     - Parameters:
        - T: Der Typ des Objektes, welches aus der JSON Antwort konvertiert wird.
        - typeIndex: Der Typ der Serveranfrage.
        - type: Der Typ des Objektes, welches aus der JSON Antwort konvertiert wird. Kann anstelle von T gesetzt werden.
        - queryParams: Die Parameter, welche mit der Anfrage übermittelt werden.
        - completitionHandler: Die Methode, welche aufgerufen wird, wenn die Anfrage erfolgreich konvertiert wurde.
        - errorHandler: Die Methode, welche aufgerufen wird, wenn der Server einen Fehler zurück gibt oder während des Konvertierens ein Fehler auftrat.
     */
    static func load<T: Decodable>(_ typeIndex: Int, as type: T.Type = T.self, queryParams: [String: Any], completitionHandler: Action<T>? = nil, errorHandler: @escaping (_: String) -> ()) {
        
        // Nutzerstandards sind bei WatchOS die standardmäßigen, bei iOS sind diese aber geteilt, welche auch noch von den Widgets benutzt werden
        #if os(watchOS)
        let defaults: UserDefaults? = UserDefaults.standard
        #else
        let defaults: UserDefaults? = UserDefaults.shared
        #endif
        
        var queryParams = queryParams
        if (![1, 2].contains(typeIndex)) {
            queryParams["jwt"] = defaults?.string(forKey: "token") ?? ""
        }
        
        // Überprüfung, ob eine Netzwerkverbindung besteht
        if (Reachability.isConnectedToNetwork) {
            NetworkHandler.load(typeIndex, queryParams: queryParams) { (data: Data?) in
                if let handler = completitionHandler {
                    if let data = data {
                        do {
                            // Prüfen, ob ein Fehler vorliegt
                            let error = try JSONDecoder().decode(ErrorResult.self, from: data).error
                            NSLog("Error during loading data from the database: \(error)")
                            
                            // Benachrichtigungstokens existieren bei WatchOS nicht. Stattdessen ist dort im selbem Feld der Server Token.
                            #if !os(watchOS)
                            NetworkHandler.load(2, queryParams: ["token": UserDefaults.standard.string(forKey: "token") ?? ""], completitionHandler: nil)
                            #endif
                            defaults?.set(false, forKey: "loggedIn")
                            errorHandler("invalid token")
                        }
                        catch {
                            // Die Serverantwort konvertieren
                            do {
                                let resultString = String(data: data, encoding: .utf8) ?? ""
                                NSLog("request result: ", resultString)
                                let result = try JSONDecoder().decode(RequestResult<T>.self, from: data)
                                defaults?.set(result.token, forKey: "token")
                                NSLog("token: ", result.token)
                                DispatchQueue.main.async {
                                    handler(result.res)
                                }
                            }
                            // Fehler bei Konvertieren
                            catch {
                                NSLog("Error at parsing the json for index \(typeIndex)")
                                errorHandler("parsing error")
                            }
                        }
                    }
                    else {
                        errorHandler("server error")
                    }
                }
            }
        }
        // Keine Netzwerkverbindung
        else {
            errorHandler("no network")
        }
    }
}
