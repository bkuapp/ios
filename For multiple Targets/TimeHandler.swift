//
//  TimeHandler.swift
//  BKU Schedule Widget
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Kovertiert die Schulstunden zu Zeiten oder verarbeitet sie in anderer Weise.
 */
class TimeHandler {
    /**
     Konvertiert ein Schulstundenstart zu einer Uhrzeit.
     - Parameter start: die Startstunde, welche kovertiert werden soll.
     - Returns: der Stundenwert als ein Zeitstring.
    */
    static func buildStartTime(start: Int) -> String {
        switch start {
        case 1:
            return "7:30"
        case 2:
            return "8:15"
        case 3:
            return "9:25"
        case 4:
            return "10:10"
        case 5:
            return "11:15"
        case 6:
            return "12:00"
        case 7:
            return "13:00"
        case 8:
            return "13:45"
        case 9:
            return "14:45"
        case 10:
            return "15:30"
        case 11:
            return "16:15"
        case 12:
            return "17:00"
        case 13:
            return "17:45"
        case 14:
            return "18:40"
        case 15:
            return "19:25"
        case 16:
            return "20:15"
        case 17:
            return "21:00"
        default:
            return ""
        }
    }

    /**
     Konvertiert ein Schulstundenende zu einer Uhrzeit.
     - Parameter end: die Endstunde, welche kovertiert werden soll.
     - Returns: der Stundenwert als ein Zeitstring.
    */
    static func buildEndTime(end: Int) -> String {
        switch end {
        case 1:
            return "8:15"
        case 2:
            return "9:00"
        case 3:
            return "10:10"
        case 4:
            return "10:55"
        case 5:
            return "12:00"
        case 6:
            return "12:45"
        case 7:
            return "13:45"
        case 8:
            return "14:30"
        case 9:
            return "15:30"
        case 10:
            return "16:15"
        case 11:
            return "17:00"
        case 12:
            return "17:45"
        case 13:
            return "18:30"
        case 14:
            return "19:25"
        case 15:
            return "20:10"
        case 16:
            return "21:00"
        case 17:
            return "21:45"
        default:
            return ""
        }
    }
}
