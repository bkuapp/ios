//
//  DetailTeacher.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Die Struktur, welche die Informationen des Lehrers oder der Klasse in der Detailansicht speichert.
 */
struct DetailPerson: Hashable, Codable {
    /**
     Der Name des Lehrers oder der Klasse.
     */
    var name: String
    /**
     Die Email des Lehrers oder der Klasse.
     */
    var email: String
    /**
     Die Namen der Kurse des Lehrers oder der Klasse, welche der eingeloggte Nutzer mit der Person hat.
     */
    var courses: [String]
}
