//
//  Exam.swift
//  BKU Exam Widget
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Die Struktur, welche die Informationen einer Klausur speichert.
 */
struct Exam: Hashable, Codable {
    /**
     Die ID der Klausur.
     */
    var id: Int
    /**
     Der Name des Kurses, über den die Klausur geht.
     */
    var course: String
    /**
     Das Datum, an dem die Klausur geschrieben wird.
     */
    var date: String
    /**
     Die Uhrzeit, zu der die Klausur geschrieben wird.
     */
    var time: String
    /**
     Der Raum, in dem die Klausur geschrieben wird.
     */
    var room: String
}
