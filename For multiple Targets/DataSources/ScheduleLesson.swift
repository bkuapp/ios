//
//  ScheduleLesson.swift
//  BKU Schedule Widget
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Die Struktur, welche benutzt wird, um eine einzelne Schulstunde für das Stundenplanwidget oder die Stundenliste auf der Apple Watch zu speichern.
 */
struct ScheduleLesson: Hashable, Codable {
    /**
     Die ID der Schulstunde.
     */
    var id: Int
    /**
     Die Anrede des Lehrers (Herr oder Frau), welcher die Stunde hält (nur für Schüler).
     */
    var title: String
    /**
     Der Nachname des Lehrers, welcher die Stunde hält (nur für Schüler).
     */
    var lastName: String
    /**
     Der Name der Klasse, welche an der Stunde teilnimmt (nur für Lehrer).
     */
    var nameOfClass: String
    /**
     Die Schulstunde zu der der Kurs anfängt.
     */
    var start: Int
    /**
     Die Schulstunde zu der der Kurs endet.
     */
    var end: Int
    /**
     Der Name des Faches, welches unterrichtet wird.
     */
    var course: String
    /**
     Der Name des Raumes, in dem der Unterricht stattfindet.
     */
    var room: String?
    /**
     Ob die Stunde vertreten wird.
     */
    var represented: Bool
    /**
     Ob die Stunde ausfällt.
     */
    var isCancelled: Bool
}
