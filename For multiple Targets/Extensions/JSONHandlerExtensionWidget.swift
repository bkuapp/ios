//
//  JSONHandlerExtensionWidget.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension JSONHandler {
    /**
     Konvertiert eine Serverantwort für den angegeben Typ zu einem Objekt vom Typ T und ruft mit ihm die Abschlussmethode auf. Behandelt die Fehlermeldungen für die Widgets.

     - Parameters:
        - T: Der Typ des Objektes, welches aus der JSON Antwort konvertiert wird.
        - typeIndex: Der Typ von der Serveranfrage.
        - type: Der Typ des Objektes, welches aus der JSON Antwort konvertiert wird. Kann anstelle von T gesetzt werden.
        - queryParams: Die Parameter, welche mit der Anfrage übermittelt werden.
        - completitionHandler: Die Methode, welche aufgerufen wird, wenn die Anfrage erfolgreich konvertiert wurde.
    */
    static func load<T: Decodable>(_ typeIndex: Int, as type: T.Type = T.self, queryParams: [String: Any] = [:], completitionHandler: Action<T>? = nil) {
        load(typeIndex, queryParams: queryParams, completitionHandler: completitionHandler) { error in
            if (error == "invalid token") {
                UserDefaults.shared?.set(false, forKey: "loggedIn")
            }
        }
    }
}
