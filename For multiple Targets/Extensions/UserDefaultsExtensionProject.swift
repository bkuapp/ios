//
//  UserDefaultsExtensionProject.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension UserDefaults {
    /**
     Ob der momentan eingeloggte Nutzer ein TG Schüler ist.
     */
    static var isTGStudent: Bool {
        return userName.starts(with: "tg", ignoreCase: true)
    }
    
    /**
     Ob der momentan eingeloggte Nutzer ein Lehrer ist.
     */
    static var isTeacher: Bool {
        return userName.starts(with: "l-", ignoreCase: true)
    }
}
