//
//  StringExtension.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension String {
    /**
     Der lokalisierte String des aktuellen Strings.
     */
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: .main, value: "", comment: "")
    }
    
    /**
     Gibt zurück, ob der aktuelle String mit dem angegeben String startet.
     
     - Parameters:
        - with: Der String, mit welcher der andere String starten soll
        - ignoreCase: Ob Groß- und Kleinschreibung bei dem Vergleich berücksichtigt werden soll.
     - Returns: Ob der String mit dem angegeben String anfängt.
     */
    func starts(with start: String, ignoreCase: Bool) -> Bool {
        if (ignoreCase) {
            return compare(start, options: .caseInsensitive, range: start.startIndex..<start.endIndex, locale: Locale.german) == .orderedSame
        }
        return starts(with: start)
    }
}
