# BKU-iOS-SwiftUI

BKU App für iOS, iPadOS und WatchOS entwickelt mit SwiftUI.

Zeigt eingeloggten Schülern und Lehrern den Stundenplan, den Vertretungsplan, den Klausurplan (nur für TG-Schüler), die Personenliste (Lehrer für Schüler und Klassen sowie Lehrer für Lehrer)m die Raumliste (nur für Lehrer) und Neuigkeiten.

Die App benutzt eine JSON API auf PHP-Basis für den Datentransfer und Firebase, um Benachrichtigungen zu schicken. Unterstützt werden alle iOS und iPadOS Geräte ab Version 13.0 und alle WatchOS Modelle ab Version 6.0.

Pods (Firebase) können mit  `pod install` vom Terminal installiert werden. Danach muss man das Project über die .xcworkspace Datei öffnen.

[Dokumentation](https://app.bkukr.de/docs/ios)
