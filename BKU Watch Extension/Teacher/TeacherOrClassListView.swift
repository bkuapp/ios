//
//  TeacherOrClassListView.swift
//  BKU Watch Extension
//
//  Created by Sven Op de Hipt on 03.10.19.
//  Copyright © 2019 Berufskolleg Uerdingen. All rights reserved.
//

import SwiftUI

struct TeacherOrClassListView: View {
    @EnvironmentObject private var model: Model
    
    @Binding var teachersOrClasses : [[TeacherOrClass]]
    
    var body: some View {
        List(teachersOrClasses.first ?? [], id: \.self) { teacherOrClass in
            NavigationLink(destination: TeacherOrClassDetailView(id: teacherOrClass.id)) {
                TeacherOrClassRowView(teacherOrClass: teacherOrClass)
            }
        }
        .navigationBarTitle("teacher list")
    }
}

struct TeacherListView_Previews: PreviewProvider {
    static var previews: some View {
        TeacherOrClassListView(teachersOrClasses: .constant([]))
    }
}
