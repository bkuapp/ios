//
//  TeacherOrClassDetailView.swift
//  BKU Watch Extension
//
//  Created by Sven Op de Hipt on 04.10.19.
//  Copyright © 2019 Berufskolleg Uerdingen. All rights reserved.
//

import SwiftUI

struct TeacherOrClassDetailView: View {
    let id: Int
    
    @State private var teacherOrClass: DetailTeacherOrClass? = nil
    
    var body: some View {
        GeometryReader { geometry in
            VStack(spacing: 0) {
                HStack(spacing: 0) {
                    Text("courses")
                        .frame(width: geometry.size.width * 0.4, height:  CGFloat((self.teacherOrClass?.courses.count ?? 1) * 20 + 10))
                        .border(Color.white)
                        .padding(.trailing, -1)
                    VStack {
                        ForEach(self.teacherOrClass?.courses ?? [] , id: \.self) { course in
                            Text(course)
                        }
                    }
                    .frame(width: geometry.size.width * 0.6, height:  CGFloat((self.teacherOrClass?.courses.count ?? 1) * 20 + 10))
                        .border(Color.white)
                }
                HStack(spacing: 0) {
                    Text("email")
                        .frame(width: geometry.size.width * 0.4, height: 100)
                        .border(Color.white)
                        .padding(.trailing, -1)
                    Text(self.teacherOrClass?.email.replacingOccurrences(of: ".", with: ".\n").replacingOccurrences(of: "-", with: "-\n").replacingOccurrences(of: "@", with: "@\n") ?? "")
                        .multilineTextAlignment(.center)
                        .frame(width: geometry.size.width * 0.6, height: 100)
                        .border(Color.white)
                }
                .padding(.top, -1)
            }
            .font(.system(size: 14))
        }
        .navigationBarTitle(teacherOrClass?.name ?? "")
        .onAppear {
            var params: [String: Any] = [
                "id": self.id
            ]
            
            if (UserDefaults.isTeacher) {
                params["personType"] = "class"
            }
            
            JSONHandler.load(6, queryParams: params) { teacher in
                self.teacherOrClass = teacher
            }
        }
    }
}

struct TeacherDetailView_Previews: PreviewProvider {
    static var previews: some View {
        TeacherOrClassDetailView(id: 1)
    }
}
