//
//  MainView.swift
//  BKU Watch Extension
//
//  Created by Sven Op de Hipt on 03.10.19.//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Hauptansicht, welche den Stundenplan, Vertretungsplan, Klausurplan oder die Personenliste anzeigt.
 */
struct MainView: View {
    /**
     Der Typ der Ansicht, welche aktuell gezeigt wird (Stundenplan, Vertretungsplan, Klausurplan, Personenliste).
     */
    @State private var viewType = ViewType.schedule
    
    /**
     Die Kurse, welche aktuell im Stundenplan angezeigt werden.
     */
    @State private var lessons: [ScheduleLesson] = []
    /**
     Die Vertretungen, welche aktuell im Vertretungsplan angezeigt werden.
     */
    @State private var representations: [Representation] = []
    /**
     Die Klausuren, welche aktuell im Klausurplan angezeigt werden.
     */
    @State private var exams: [Exam] = []
    /**
     Die Personen, welche aktuell in der Personenlist angezeigt werden.
     */
    @State private var persons: [[Person]] = []
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        Group {
            if (viewType == .schedule) {
                ScheduleView(lessons: $lessons)
            } else if (viewType == .representation) {
                RepresentationView(representations: $representations)
            } else if (viewType == .examList) {
                ExamPlanView(exams: $exams)
            } else {
                PersonListView(persons: $persons)
            }
        }
        .contextMenu(ContextMenu(menuItems: {
            Group {
                // Stundenplan
                if (viewType != .schedule) {
                    MenuItemView(viewType: .schedule, imageName: "calendar", text: "schedule", currentViewType: $viewType)
                }
                // Vertretungsplan
                if (viewType != .representation) {
                    MenuItemView(viewType: .representation, imageName: "shuffle", text: "representation", currentViewType: $viewType)
                }
                // Klausurplan
                if (viewType != .examList && UserDefaults.isTGStudent) {
                    MenuItemView(viewType: .examList, imageName: "book.fill", text: "exam plan", currentViewType: $viewType)
                }
                // Personenliste
                if (viewType != .personList) {
                    MenuItemView(viewType: .personList, imageName: "person.2.fill", text: "teacher list", currentViewType: $viewType)
                }
                // Neuladen
                Button(action: loadData) {
                    VStack {
                        Image(systemName: "arrow.clockwise")
                            .font(.title)
                        Text("reload")
                    }
                }
            }
        }))
        .onAppear {
            if (!Model.shared.initialLoad) {
                self.loadData()
                Model.shared.initialLoad = false
            }
        }
    }
    
    /**
     Lädt die Daten vom Server.
     */
    func loadData() {
        JSONHandler.load(200) { lessons in
            self.lessons = lessons
        }
        JSONHandler.load(201) { representations in
            self.representations = representations
        }
        JSONHandler.load(203) { exams in
            self.exams = exams
        }
        JSONHandler.load(5) { teachers in
            self.persons = teachers
        }
    }
}
