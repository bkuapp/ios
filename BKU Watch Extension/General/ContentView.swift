//
//  ContentView.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche entweder die Loginansicht oder die Hauptansicht enthält.
 */
struct ContentView: View {
    /**
     Das aktuelle Model der gesamten App.
     */
    @EnvironmentObject private var model: Model
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        Group {
            if (model.loggedIn) {
                MainView()
            }
            else {
                LoginView()
            }
        }
        .alert(isPresented: self.$model.showAlert) {
            Alert(title: Text(model.alertTitle), message: Text(model.alertMessage), dismissButton: .default(Text("OK")))
        }
    }
}
