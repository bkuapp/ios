//
//  MenuItemView.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche ein Eintrag im Menu beinhaltet.
 */
struct MenuItemView: View {
    /**
     Der Typ der Ansicht, welche beim Klick auf den Eintrag ausgewählt werden soll.
     */
    let viewType: ViewType
    /**
     Der Name des Bildes, welches im Eintrag angezeigt werden soll.
     */
    let imageName: String
    /**
     Der Text, welcher im Eintrag angezeigt werden soll.
     */
    let text: String
    
    /**
     Die Ansicht, welche aktuell angezeigt wird.
     */
    @Binding var currentViewType: ViewType
    
    var body: some View {
        Button(action: {
            self.currentViewType = self.viewType
        }) {
            VStack{
                Image(systemName: imageName)
                    .font(.title)
                Text(text.localized)
            }
        }
    }
}
