//
//  ExtensionDelegate.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Das Delegat, welches die Lebenszyklus der Apple Watch überwacht.
 */
class ExtensionDelegate: NSObject, WKExtensionDelegate {
    /**
     Wird aufgerufen, wenn die App vollständig gestartet ist.
     */
    func applicationDidFinishLaunching() {
        // Wird hier aufgerufen, da das Objekt vorher nicht erzeugt wird.
        let _ = ConnectivityHandler.shared
        registerUserDefaults()
    }
    
    /**
     Wird aufgerufen, wenn die App in den Vordergrund getreten ist.
     */
    func applicationWillEnterForeground() {
        Model.shared.phoneTokenInvalid = false
        Model.shared.initialLoad = false
    }
    
    /**
     Initialisiert die Nutzerstandards. Wird beim Appstart aufgerufen.
     */
    private func registerUserDefaults() {
        UserDefaults.standard.register(defaults: ["username": ""])
        UserDefaults.standard.register(defaults: ["loggedIn": false])
        UserDefaults.standard.register(defaults: ["token": ""])
    }
}
