//
//  Wrapper.swift
//  BKU Watch Extension
//
//  Created by Sven Op de Hipt on 06.10.19.
//  Copyright © 2019 Berufskolleg Uerdingen. All rights reserved.
//

import SwiftUI

struct Wrapper {
    static var shared = Wrapper()
    
    private init() {}
    
    @State var loggedIn = LoginStatus.unknown
}
