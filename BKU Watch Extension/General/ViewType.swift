//
//  ViewType.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Die Typen der Ansichten, welche in der Hauptansicht angezeigt werden können.
 */
enum ViewType {
    /**
     Der Typ, welche ausgewäht ist, wenn der Stundenplan angezeigt wird.
     */
    case schedule
    /**
     Der Typ, welche ausgewäht ist, wenn der Vertretungsplan angezeigt wird.
     */
    case representation
    /**
     Der Typ, welche ausgewäht ist, wenn der Klausurplan angezeigt wird.
     */
    case examList
    /**
     Der Typ, welche ausgewäht ist, wenn die Personenliste angezeigt wird.
     */
    case personList
}
