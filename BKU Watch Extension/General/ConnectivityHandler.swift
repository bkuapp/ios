//
//  ConnectivityHandler.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WatchConnectivity

/**
 Der Handler, welcher Nachrichten an das verbunden iPhone schickt und welche von ihm empfängt.
 */
class ConnectivityHandler : NSObject, WCSessionDelegate {
    /**
     Die Singleton Instanz des Konnektivitäts-Handler.
     */
    static let shared = ConnectivityHandler()
    
    /**
     Die aktuelle Sitzung mit dem iPhone.
     */
    private var session = WCSession.default

    /**
     Der Konstruktur des Handlers, welcher die Sitzung instanziert.
     */
    private override init() {
        super.init()

        session.delegate = self
        session.activate()
    }

    /**
     Wird aufgerufen, wenn die Sitzung mit dem iPhone mit oder ohne Erfolg aktiviert wurde.
    
     - Parameters:
        - session: Die Sitzung mit der iPhone.
        - activationState: Der Status der Aktivierung.
        - error: Der Fehler, welcher bei der Aktivierung aufgetreten ist. Ist null, wenn kein Fehler aufgertreten ist.
    */
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        NSLog("%@", "activationDidCompleteWith activationState:\(activationState) error:\(String(describing: error))")
    }
    
    /**
     Wird aufgerufen, wenn eine Nachricht vom iPhone eingetroffen ist.
    
     - Parameters:
        - session: Die Sitzung mit der Apple Watch.
        - message: Die Nachricht, welche eingetroffen ist.
     */
    func session(_ session: WCSession, didReceiveMessage message: [String: Any]) {
        dataReceived(message)
    }
    
    /**
     Wird aufgerufen, wenn Nutzerinformationen vom iPhone eingetroffen ist.
    
     - Parameters:
        - session: Die Sitzung mit der Apple Watch.
        - message: Die Nutzerinformationen, welche eingetroffen sind.
     */
    func session(_ session: WCSession, didReceiveUserInfo userInfo: [String: Any]) {
        dataReceived(userInfo)
    }
    
    /**
     Sendet die Login-Nachricht zum iPhone um die Logindaten zu erhalten.
     */
    func sendLoginMessage() {
        sendMessage(message: ["key": "loggedIn"], completitionHandler: { result in
            self.dataReceived(result)
        })
    }
    
    /**
     Sendet eine Nachricht an das iPhone.
    
     - Parameters:
        - message: Die Nachricht, welche gesendet werden soll.
        - completitionHandler: Die Methode, welche aufgerufen wird, wenn die Antwort vom iPhone eintrifft.
        - counter: Die Anzah an Versuchen, wie häufig der Vorgang erneut versucht werden soll, wenn er fehlschlägt.
     */
    private func sendMessage(message: [String: Any], completitionHandler: @escaping (_: [String: Any]) -> (), retrys: Int = 5) {
        if (session.isCompanionAppInstalled && session.isReachable) {
            if (session.activationState == .activated) {
                session.sendMessage(message, replyHandler: { result in
                    completitionHandler(result)
                }, errorHandler: { error in
                    if (retrys > 0) {
                        self.sendMessage(message: message, completitionHandler: completitionHandler, retrys: retrys - 1)
                    }
                })
            }
            else if (retrys > 0) {
                session.activate()
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                    self.sendMessage(message: message, completitionHandler: completitionHandler, retrys: retrys - 1)
                }
            }
        }
    }
    
    /**
     Verarbeitet die Daten, welche vom iPhone geschickt wurden sind.
     
     - Parameter data: Die Daten, welcher verarbeitet werden.
     */
    private func dataReceived(_ data: [String: Any]) {
        if let error = data["error"] as? String {
            Model.shared.phoneTokenInvalid = true
            Model.shared.alertTitle = "error".localized
            Model.shared.alertMessage = error
            Model.shared.showAlert = true
            
            return
        }
        
        if (data["type"] as? String == "loggedIn") {
            if let username = data["username"] as? String {
                UserDefaults.standard.set(username, forKey: "username")
            }
            if let loggedIn = data["loggedIn"] as? Bool {
                UserDefaults.standard.set(loggedIn, forKey: "loggedIn")
                DispatchQueue.main.async {
                    Model.shared.loggedIn = loggedIn
                    Model.shared.initialLoad = false
                }
            }
            if let token = data["token"] as? String {
                UserDefaults.standard.set(token, forKey: "token")
            }
        }
    }
}
