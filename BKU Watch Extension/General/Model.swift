//
//  Model.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Das Model der App, welches den Loginstatus und die aktuelle Fehlermeldung beinhaltet.
 */
class Model: ObservableObject {
    /**
     Die Singleton Instanz des Konnektivitäts-Handler.
     */
    static let shared = Model()
    
    /**
     Der Kostruktur des Models, welcher nichts macht.
     */
    private init() {}
    
    /**
     Ob ein Nutzer eingeloggt ist.
     */
    @Published var loggedIn = UserDefaults.standard.bool(forKey: "loggedIn")
    
    /**
     Ob der JWT Token vom iPhone ebenfalls ungültig ist.
     */
    @Published var phoneTokenInvalid = false
    /**
     Ob gerade eine Fehlermeldung angezeigt werden soll.
     */
    @Published var showAlert = false
    
    /**
     Der Titel der Fehlermeldung, welche gerade angezeigt wird.
     */
    @Published var alertTitle = ""
    /**
     Die Nachricht der Fehlermeldung, welche gerade angezeigt wird.
     */
    @Published var alertMessage = ""
    
    /**
     Ob die Daten seit Appstart schon einmal vom Server geladen wurden.
     */
    @Published var initialLoad = false
}
