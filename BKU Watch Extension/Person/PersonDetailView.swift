//
//  PersonDetailView.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die detailierten Personeninformationen (Klassen oder Lehrer).
 */
struct PersonDetailView: View {
    /**
     Die ID der Person, welche angezeigt werden soll.
     */
    let id: Int
    
    /**
     Die Person, welche angezeigt wird.
     */
    @State private var person: DetailPerson? = nil
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        GeometryReader { geometry in
            ScrollView {
                VStack(spacing: 0) {
                    HStack(spacing: 0) {
                        Text("courses")
                            .frame(width: geometry.size.width * 0.4, height:  CGFloat((self.person?.courses.count ?? 1) * 20 + 10))
                            .border(Color.white)
                            .padding(.trailing, -1)
                        VStack {
                            ForEach(self.person?.courses ?? [] , id: \.self) { course in
                                Text(course)
                            }
                        }
                        .frame(width: geometry.size.width * 0.6, height:  CGFloat((self.person?.courses.count ?? 1) * 20 + 10))
                            .border(Color.white)
                    }
                    HStack(spacing: 0) {
                        Text("email")
                            .frame(width: geometry.size.width * 0.4, height: 100)
                            .border(Color.white)
                            .padding(.trailing, -1)
                        Text(self.person?.email.replacingOccurrences(of: ".", with: ".\n").replacingOccurrences(of: "-", with: "-\n").replacingOccurrences(of: "@", with: "@\n") ?? "")
                            .multilineTextAlignment(.center)
                            .frame(width: geometry.size.width * 0.6, height: 100)
                            .border(Color.white)
                    }
                    .padding(.top, -1)
                }
                .font(.system(size: 14))
            }
        }
        .navigationBarTitle(person?.name ?? "")
        .onAppear {
            if (self.person == nil) {
                var params: [String: Any] = [
                    "id": self.id
                ]
                
                if (UserDefaults.isTeacher) {
                    params["personType"] = "class"
                }
                
                JSONHandler.load(6, queryParams: params) { teacher in
                    self.person = teacher
                }
            }
        }
    }
}
