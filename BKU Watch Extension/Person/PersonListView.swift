//
//  PersonListView.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die Personenliste mit den Lehrern und/oder Klassen anzeigt.
 */
struct PersonListView: View {
    /**
     Die Person (Lehrer oder Klassen), welche in der Personenliste angezeigt werden.
     */
    @Binding var persons : [[Person]]
    
    /**
     Der Körper der Ansicht, welcher dessen Strktur beinhaltet.
     */
    var body: some View {
        List(persons.first ?? [], id: \.self) { teacherOrClass in
            NavigationLink(destination: PersonDetailView(id: teacherOrClass.id)) {
                PersonRowView(person: teacherOrClass)
            }
        }
        .navigationBarTitle("teacher list")
    }
}
