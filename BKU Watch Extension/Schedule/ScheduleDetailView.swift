//
//  ScheduleDetailView.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche detailierte Informationen zu einer Stunde anzeigt.
 */
struct ScheduleDetailView: View {
    /**
     Die ID der Stunde, welche angezeigt werden soll.
     */
    let id: Int
    
    /**
     Die Stunde, welche im Moment angezeigt wird.
     */
    @State private var lesson: DetailScheduleLesson? = nil
    
    /**
     Der Körper der Ansicht, welcher dessen Strukut beinhaltet.
     */
    var body: some View {
        GeometryReader { geometry in
            ScrollView {
                VStack(spacing: 0) {
                    HStack(spacing: 0) {
                        Group {
                            if (UserDefaults.isTeacher) {
                                Text("class")
                            }
                            else {
                                Text("teacher")
                            }
                        }
                        .frame(width: geometry.size.width * 0.4, height: CGFloat((self.lesson?.name.split(separator: " ").count ?? 1) * 20 + 10))
                            .border(Color.white)
                            .padding(.trailing, -1)
                        Text(String(self.lesson?.name.replacingOccurrences(of: " ", with: "\n") ?? ""))
                            .multilineTextAlignment(.center)
                            .frame(width: geometry.size.width * 0.6, height: CGFloat((self.lesson?.name.split(separator: " ").count ?? 1) * 20 + 10))
                            .border(Color.white)
                    }
                    HStack(spacing: 0) {
                        Text("hours")
                            .frame(width: geometry.size.width * 0.4, height: 30)
                            .border(Color.white)
                            .padding(.trailing, -1)
                        Text("\(self.lesson?.start ?? 0). - \(self.lesson?.end ?? 0).")
                            .frame(width: geometry.size.width * 0.6, height: 30)
                            .border(Color.white)
                    }
                    .padding(.top, -1)
                    HStack(spacing: 0) {
                        Text("email")
                            .frame(width: geometry.size.width * 0.4, height: 100)
                            .border(Color.white)
                            .padding(.trailing, -1)
                        Text(self.lesson?.email.replacingOccurrences(of: ".", with: ".\n").replacingOccurrences(of: "@", with: "\n@") ?? "")
                            .multilineTextAlignment(.center)
                            .frame(width: geometry.size.width * 0.6, height: 100)
                            .border(Color.white)
                    }
                    .padding(.top, -1)
                }
                .font(.system(size: 14))
            }
        }
        .navigationBarTitle(lesson?.course ?? "")
        .onAppear {
            if (self.lesson == nil) {
                let params: [String: Any] = [
                    "id": self.id
                ]
                JSONHandler.load(4, queryParams: params) { lesson in
                    self.lesson = lesson
                }
            }
        }
    }
}
