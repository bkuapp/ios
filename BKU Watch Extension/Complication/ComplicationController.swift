//
//  ComplicationController.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import ClockKit
import WatchKit

/**
 Der Kontroller, welcher die WatchOS Komplikationen auf dem Ziffernblatt verarbeitet.
 */
class ComplicationController: NSObject, CLKComplicationDataSource {
    /**
     Wird aufgerufen, um zu erfahren, ob diese Komplikation in die Zukunft oder Vergangenheit spulen kann.
     
     - Parameters:
        - complication: Die Komplikation, bei welcher man erfahren möchte, ob die vor oder zurück spulen kann.
        - handler: Die Methode, welche aufgerufen wird, um zu übermitteln, ob die Komplikation in die Zukunft oder Vergangenheit spulen kann.
     */
    func getSupportedTimeTravelDirections(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Void) {
        handler([])
    }
    
    /**
     Wird aufgerufen, um zu erfahren, ob diese Komplikation auch im gesperrten Zustand Informationen anzeigt.
    
     - Parameters:
        - complication: Die Komplikation, bei welcher man erfahren möchte, ob diese im gesperrten Zustand Informationen anzeigt.
        - handler: Die Methode, welche aufgerufen wird, um zu übermitteln, ob die Komplikation im gesperrten Zustand Informationen anzeigt.
    */
    func getPrivacyBehavior(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.hideOnLockScreen)
    }
    
    /**
     Wird aufgerufen, um zu erfahren, welche Informationen die Komplikation gerade anzeigt.
    
     - Parameters:
        - complication: Die Komplikation, bei welcher man erfahren möchte, welche Informationen diese anzeigt.
        - handler: Die Methode, welche aufgerufen wird, um zu übermitteln, welche Informationen die Komplikation gerade anzeigt.x
    */
    func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Void) {
        handler(nil)
    }
}
