//
//  RepresentationView.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche den Vertretungsplan anzeigt.
 */
struct RepresentationView: View {
    /**
     Die Vertretungen, welche im Vertretungsplan angezeigt werden sollen.
     */
    @Binding var representations: [Representation]
    
    /**
     Der Körper der Ansicht, welcher dessen Strkutur beinhaltet.
     */
    var body: some View {
        List(representations, id: \.self) { representation in
            NavigationLink(destination: RepresentationDetailView(id: representation.id)) {
                RepresentationRowView(representation: representation)
            }
        }
        .navigationBarTitle("representation")
    }
}
