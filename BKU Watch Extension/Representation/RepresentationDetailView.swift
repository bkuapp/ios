//
//  RepresentationDetailView.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die detalierten Informationen einer Vertretung anzeigt.
 */
struct RepresentationDetailView: View {
    /**
     Die ID der Vertretung, welche angezeigt werden soll.
     */
    let id: Int
    
    /**
     Die Vertretung, welche angezeigt wird.
     */
    @State private var representation: DetailRepresentation? = nil
    
    /**
     Der Körper der Ansicht, welcher dessen Strktur beinhaltet.
     */
    var body: some View {
        Text(representation?.representationRules ?? "")
        .multilineTextAlignment(.center)
        .navigationBarTitle(representation?.course ?? "")
        .onAppear {
            if (self.representation == nil) {
                let params: [String: Any] = [
                    "id": self.id
                ]
                JSONHandler.load(202, queryParams: params) { representation in
                    self.representation = representation
                }
            }
        }
    }
}
