//
//  Representation.swift
//  BKU Watch Extension
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Speichert die Informationen einer einzelnen Vertretung, welche im Vertretungsplan angezeigt wird.
 */
struct Representation: Hashable, Codable {
    /**
     Die ID der Vertretung.
     */
    var id: Int
    /**
     Das Datum der Vertretung.
     */
    var date: String
    /**
     Die Stunden, welche vertreten werden.
     */
    var hour: String
    /**
     Der Kurs, welcher vertreten wird.
     */
    var course: String
    /**
     Der Raum, in dem die Vertretung stattfindet.
     */
    var room: String?
}
