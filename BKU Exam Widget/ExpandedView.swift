//
//  ExpandedView.swift
//  BKU Exam Widget
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

/**
 Die Ansicht, welche angezeigt wird, wenn der Modus der Widgets erweitert ist.
 */
struct ExpandedView: View {
    /**
     Die Klausuren, welche in dem Widget angezeigt werden.
     */
    let exams: [Exam]
    /**
     Der Kontext des Widgets, in dem die Ansicht gezeigt wird.
     */
    let context: NSExtensionContext?
    
    /**
     Der Körper der Ansicht, welche dessen Struktur speichert.
     */
    var body: some View {
        VStack(spacing: 0) {
            ForEach(exams, id: \.self) { exam in
                ExpandedRowView(exam: exam, context: self.context)
                    .frame(height: 80)
            }
        }
    }
}
