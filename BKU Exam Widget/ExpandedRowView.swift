//
//  ExpandedRowView.swift
//  BKU Exam Widget
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

/**
 Die Ansicht, welche eine einzelne Zeile in der erweiterten Ansicht anzeigt.
 */
struct ExpandedRowView: View {
    /**
     Die Klausur, welche in der Zeile angezeigt werden soll.
     */
    let exam: Exam
    /**
     Der Kontext des Widgets, zu welchem die Ansicht gehört.
     */
    let context: NSExtensionContext?
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        GeometryReader { geometry in
            HStack(spacing: 0) {
                VStack {
                    Text(self.exam.time.split(separator: "-")[0])
                    Text(self.exam.time.split(separator: "-")[1])
                }
                .font(.system(size: 20))
                .frame(width: geometry.size.width / 3)
                VStack {
                    Text(self.exam.date)
                    .frame(width: geometry.size.width * 2 / 3, height: geometry.size.height / 4)
                    Text(self.exam.course)
                    .frame(width: geometry.size.width * 2 / 3, height: geometry.size.height / 4)
                    .font(.system(size: 17))
                    Text(self.exam.room)
                    .frame(width: geometry.size.width * 2 / 3, height: geometry.size.height / 4)
                }
                .font(.system(size: 15))
                .frame(width: geometry.size.width * 2 / 3)
            }
        }
        .onTapGesture {
            if let url = URL(string: "exam://" + String(self.exam.id)) {
                self.context?.open(url, completionHandler: nil)
            }
        }
    }
}
