//
//  SmallView.swift
//  BKU Exam Widget
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import SwiftUI

/**
 Die Ansicht, welche im Widget angezeigt wird, wenn der Modus normal ist.
 */
struct SmallView: View {
    /**
     Die Klausur, welche in dem Widget angezeigt wird.
     */
    let exam: Exam
    /**
     Die Größe des Widgets, in welchem die Ansicht angezeigt wird.
     */
    let size: CGSize
    /**
     Der Kontext des Widgets in dem die Ansicht angezeigt wird.
     */
    let context: NSExtensionContext?
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        HStack(spacing: 0) {
            VStack {
                Text(exam.time.split(separator: "-")[0])
                Text(exam.time.split(separator: "-")[1])
            }
            .font(.system(size: 30))
            .frame(width: size.width / 3)
            VStack {
                Text(exam.course.replacingOccurrences(of: "\n", with: ""))
                .frame(width: size.width * 2 / 3, height: size.height / 2)
                .font(.system(size: 25))
                Text(exam.date)
                .frame(width: size.width * 2 / 3, height: size.height / 4)
                Text(exam.room)
                .frame(width: size.width * 2 / 3, height: size.height / 4)
            }
            .font(.system(size: 20))
            .frame(width: size.width * 2 / 3)
        }
        .onTapGesture {
            if let url = URL(string: "exam://" + String(self.exam.id)) {
                self.context?.open(url, completionHandler: nil)
            }
        }
    }
}
