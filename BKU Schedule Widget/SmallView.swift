//
//  SmallView.swift
//  BKU Schedule Widget
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche im Widget angezeigt wird, wenn der Modus normal ist.
 */
struct SmallView: View {
    /**
     Die Stunde, welche in dem Widget angezeigt wird.
     */
    let lesson: ScheduleLesson
    /**
     Die Größe des Widgets, in welchem die Ansicht angezeigt wird.
     */
    let size: CGSize
    /**
     Der Kontext des Widgets in dem die Ansicht angezeigt wird.
     */
    let context: NSExtensionContext?
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        HStack(spacing: 0) {
            VStack {
                Text(TimeHandler.buildStartTime(start: lesson.start))
                Text(TimeHandler.buildEndTime(end: lesson.end))
            }
            .font(.system(size: 30))
            .frame(width: size.width / 3)
            VStack {
                Text(lesson.course)
                .frame(width: size.width * 2 / 3, height: size.height / 2)
                .font(.system(size: 25))
                
                Group {
                    if (UserDefaults.isTeacher) {
                        Text(lesson.nameOfClass)
                    }
                    else {
                        Text("\(lesson.title) \(lesson.lastName)")
                    }
                }
                .frame(width: size.width * 2 / 3, height: size.height / (lesson.room != nil ? 4 : 2))
                
                if (lesson.room != nil) {
                    Text(lesson.room ?? "")
                    .frame(width: size.width * 2 / 3, height: size.height / 4)
                }
            }
            .font(.system(size: self.lesson.room != nil ? 20 : 25))
            .frame(width: size.width * 2 / 3)
        }
        .foregroundColor(self.lesson.represented ? .green : self.lesson.isCancelled ? .red : .primary)
        .onTapGesture {
            if let url = URL(string: "schedule://" + String(self.lesson.id)) {
                self.context?.open(url, completionHandler: nil)
            }
        }
    }
}
