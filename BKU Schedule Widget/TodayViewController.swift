//
//  TodayViewController.swift
//  BKU Schedule Widget
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import NotificationCenter

/**
 Der Kontroller für das Stundenplanwidget.
 */
class TodayViewController: UIViewController, NCWidgetProviding {
    /**
     Wird aufgerufen, wenn die Ansicht für das Stundenplanwidget auftaucht. Lädt die Daten vom Server.
     
     - Parameter animated: Ob das Auftauchen animiert war.
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let mode = extensionContext?.widgetActiveDisplayMode, let size = extensionContext?.widgetMaximumSize(for: mode) {
            loadData(withMode: mode, withSize: size)
        }
    }
    
    /**
     Wird aufgerufen, wenn die Widgetgröße sich ändert (normal oder erweitert).
     
     - Parameters:
        - activeDisplayMode: Der Modus des Widgets (normal oder erweitert).
        - maxSize: Die maximale Größe des Widgets.
     */
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        loadData(withMode: activeDisplayMode, withSize: maxSize)
    }
    
    /**
     Lädt die Stundenplandaten vom Server.
     
     - Parameters:
        - mode: Der Modus des Widgets (normal oder erweitert).
        - maxSize: Die maximale Größe des Widgets.
     */
    private func loadData(withMode mode: NCWidgetDisplayMode, withSize maxSize: CGSize) {
        JSONHandler.load(200) { lessons in
            self.createView(withMode: mode, withSize: maxSize, withLessons: lessons)
        }
    }
    
    /**
     Erstellt die Ansicht für das Widget.
     - Parameters:
        - displayMode: Der Modus des Widgets (normal oder erweitert).
        - maxSize: Die maximale Größe des Widgets.
        - lessons: Die Stunde, mit den die Ansicht erstellt werden soll.
     */
    private func createView(withMode displayMode: NCWidgetDisplayMode, withSize maxSize: CGSize, withLessons lessons: [ScheduleLesson]) {
        view.removeAllContraints()
        view.removeAllSubviews()
        
        let content: UIViewController
        
        if (UserDefaults.shared?.bool(forKey: "loggedIn") ?? false) {
            // Nur eine Stunde = nur normale Größe.
            if (lessons.count > 1) {
                extensionContext?.widgetLargestAvailableDisplayMode = .expanded
            }
            else {
                extensionContext?.widgetLargestAvailableDisplayMode = .compact
            }
            
            // Erweiterte Größe.
            if (displayMode == .expanded) {
                preferredContentSize = CGSize(width: maxSize.width, height: CGFloat(80 * lessons.count))
                content = UIHostingController(rootView: ExpandedView(lessons: lessons, context: self.extensionContext))
            }
            // Normale Größe
            else {
                preferredContentSize = maxSize
                
                if (lessons.count != 0) {
                    content = UIHostingController(rootView: SmallView(lesson: lessons[0], size: maxSize, context: self.extensionContext))
                }
                else {
                    content = UIHostingController(rootView: InfoView(size: maxSize, text: "there are no more courses today.".localized))
                }
            }
        }
        // Nicht eingeloggt.
        else {
            extensionContext?.widgetLargestAvailableDisplayMode = .compact
            content = UIHostingController(rootView: InfoView(size: maxSize, text: "please login and activate stay logged in.".localized))
        }
        
        content.view.backgroundColor = UIColor.black.withAlphaComponent(0)
        
        addConstraints(left: content.view, leftRelation: .leading, top: view, topRelation: .top, width: 1, height: 1, to: content.view, in: view, withSize: maxSize)
    }
    
    /**
     Fügt die Richtlinien für die Position und Größe, sowie die Beziehung zur äußeren Ansicht, der Ansicht des Widgets hinzu.
     
     - Parameters:
        - left: Die Ansicht, welche links vom Widget ist.
        - leftRelation: Die Beziehung zur linken Ansicht.
        - top: Die Ansicht, welche über dem Widget ist.
        - topRelation: Die Verbindung zur oberen Ansicht.
        - width: Die Breite der Widget-Ansicht (prozentuell).
        - height: Die Höhe der Widget-Ansicht (prozentuell).
        - view: Die Ansicht, bei der die Richtlininen angewendet werden sollen.
        - superView: Die Ansicht, in der sich die angegebene Ansicht befindet.
        - maxSize: Die maximale Größe des Widgets.
     */
    private func addConstraints(left: UIView, leftRelation: NSLayoutConstraint.Attribute, top: UIView, topRelation: NSLayoutConstraint.Attribute, width: CGFloat, height: CGFloat, to view: UIView, in superView: UIView, withSize maxSize: CGSize) {
        
        let widthValue = width * maxSize.width
        
        let leftConstraint = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: left, attribute: leftRelation, multiplier: 1, constant: 0)
        let topConstraint = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: top, attribute: topRelation, multiplier: 1, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: superView, attribute: .height, multiplier: height, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: widthValue)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        superView.addSubview(view)
        superView.addConstraints([leftConstraint, topConstraint, heightConstraint, widthConstraint])
    }
}
