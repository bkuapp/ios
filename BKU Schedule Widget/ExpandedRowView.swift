//
//  ExpandedRowView.swift
//  BKU Schedule Widget
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche eine einzelne Zeile in der erweiterten Ansicht anzeigt.
 */
struct ExpandedRowView: View {
    /**
     Die Stunde, welche in der Zeile angezeigt werden soll.
     */
    let lesson: ScheduleLesson
    /**
     Der Kontext des Widgets, zu welchem die Ansicht gehört.
     */
    let context: NSExtensionContext?
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        GeometryReader { geometry in
            HStack(spacing: 0) {
                VStack {
                    Text(TimeHandler.buildStartTime(start: self.lesson.start))
                    Text(TimeHandler.buildEndTime(end: self.lesson.end))
                }
                .font(.system(size: 20))
                .frame(width: geometry.size.width / 3)
                VStack {
                    Group {
                        if (UserDefaults.isTeacher) {
                            Text(self.lesson.nameOfClass)
                        }
                        else {
                            Text("\(self.lesson.title) \(self.lesson.lastName)")
                        }
                    }
                    .frame(width: geometry.size.width * 2 / 3, height: geometry.size.height / (self.lesson.room != nil ? 4 : 3))
                    
                    Text(self.lesson.course)
                    .frame(width: geometry.size.width * 2 / 3, height: geometry.size.height / (self.lesson.room != nil ? 4 : 3))
                    .font(.system(size: self.lesson.room != nil ? 17 : 20))
                    if (self.lesson.room != nil) {
                        Text(self.lesson.room ?? "")
                        .frame(width: geometry.size.width * 2 / 3, height: geometry.size.height / 4)
                    }
                }
                .font(.system(size: self.lesson.room != nil ? 15 : 18))
                .frame(width: geometry.size.width * 2 / 3)
            }
            .foregroundColor(self.lesson.represented ? .green : self.lesson.isCancelled ? .red : .primary)
        }
        .onTapGesture {
            if let url = URL(string: "schedule://" + String(self.lesson.id)) {
                self.context?.open(url, completionHandler: nil)
            }
        }
    }
}
