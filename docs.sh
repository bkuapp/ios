rm -r docs_json
rm -r docs
mkdir docs_json
mkdir docs

jazzy --clean --min-acl private --copyright Copyright\ @\ Sven\ Op\ de\ Hipt --output docs/BKU -- -workspace BKU.xcworkspace --scheme BKU

sourcekitten doc -- -workspace BKU.xcworkspace -scheme BKU\ Schedule\ Widget > docs_json/BKUScheduleWidget.json
jazzy --clean --sourcekitten-sourcefile docs_json/BKUScheduleWidget.json --copyright Copyright\ @\ Sven\ Op\ de\ Hipt --min-acl private --output docs/BKU_Schedule_Widget

sourcekitten doc -- -workspace BKU.xcworkspace -scheme BKU\ Exam\ Widget > docs_json/BKUExamWidget.json
jazzy --clean --sourcekitten-sourcefile docs_json/BKUExamWidget.json --copyright Copyright\ @\ Sven\ Op\ de\ Hipt --min-acl private --output docs/BKU_Exam_Widget
