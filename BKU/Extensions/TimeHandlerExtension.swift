//
//  TimeHandlerExtension.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

extension TimeHandler {
    /**
     Gibt die erste Stunde für den Stundenplan mit der angegebenen Woche zurück.
     
     - Parameter week: die Woche, zu der die erste Stunde berechnet werden soll.
     
     - Returns: Die erste Stunde für den Stundenplan mit der angegebenen Woche.
     */
    static func getMinHours(forWeek week: ScheduleWeek) -> Int {
        var minHours = 18
        
        for day in week.days {
            if let first = day.courses.first, minHours > first.start {
                minHours = first.start
            }
        }
        
        if (1...8 ~= minHours || minHours == 18) {
            minHours = 1
        }
        else {
            minHours = 9
        }
        
        return minHours
    }
    
    /**
     Gibt die letzte Stunde für den Stundenplan mit der angegebenen Woche zurück.
    
     - Parameter week: die Woche, zu der die letzte Stunde berechnet werden soll.
    
     - Returns: Die letzte Stunde für den Stundenplan mit der angegebenen Woche.
    */
    static func getMaxHours(forWeek week: ScheduleWeek) -> Int {
        var maxHours = 0
        
        for day in week.days {
            if let last = day.courses.last, maxHours < last.end {
                maxHours = last.end
            }
        }
        
        if (maxHours == 0) {
            maxHours = 17
        }
        return maxHours
    }
}
