//
//  StringExtension.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

extension String {
    /**
     Gibt einen Teilstring zurück, dessen Indizes innerhalb der angegeben Reichweite liegen.
     - Parameter range: Die Reichweite, in der die Indizes liegen soll.
     - Returns: Der Teilstring, dessen Indizes innerhalb der angegeben Reichweite liegen.
     */
    subscript(_ range: CountableRange<Int>) -> String {
        let idx1 = index(startIndex, offsetBy: max(0, range.lowerBound))
        let idx2 = index(startIndex, offsetBy: min(self.count, range.upperBound))
        return String(self[idx1..<idx2])
    }
    
    /**
     Die Größe des Strings mit der angegebenen Schriftart, wenn man ihn zeichnen würde.
     - Parameter font: Die Schriftart, mit der die Größe berechnet werden soll.
     - Returns: Die Größe des Strings mit der angegebenen Schriftart, wenn man ihn zeichnen würde.
     */
    func size(withFont font: UIFont) -> CGSize {
        return size(withAttributes: [NSAttributedString.Key.font: font])
    }
    
    /**
     Ersetzt bei dem String den gesuchten Buchstaben, welcher am nächsten an dem Buchstaben ist, welcher alle x Zeichen vorkommt, mit dem angegeben Buchstaben.
     - Parameters:
        - search: Der Buchstabe, welcher ersetzt werden soll.
        - replace: Der Buchstabe, welcher eingesetzt werden soll.
        - charCount: Die Anzahl, die die Abstände angibt, in der nach dem angegeben Buchstaben gesucht werden soll.
     */
    func replace(_ search: Character, with replace: Character, everyXChars charCount: Int) -> (Match: String, Value: Int) {
        if (count > charCount) {
            var ret = self
            
            var counter = 1
            
            for i in 1...count/charCount {
                var j = charCount * i
                var char = self[index(startIndex, offsetBy: j)]
                while (char != search && j > charCount * (i - 1)) {
                    j -= 1
                    char = self[index(startIndex, offsetBy: j)]
                }
                if (j > charCount * (i - 1)) {
                    var chars = Array(ret)
                    chars[j] = replace
                    ret = String(chars)
                    counter += 1
                }
            }
            
            return (ret, counter)
        }
        else {
            return (self, 1)
        }
    }
}
