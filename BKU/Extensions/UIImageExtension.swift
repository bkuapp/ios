//
//  UIImageExtension.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

extension UIImage {
    // Farben sind in Assets.xcassets definiert.
    /**
     Dynamisches schwarz, welches im Nachtmodus weiß und ansonsten schwarz ist.
    */
    private static let dynamicBlack = UIColor(named: "DynamicBlack") ?? UIColor.black
    /**
     Die Hauptfrabe des Vertretungsplans, welcher sich im ausgeschalteten und angeschalteten Nachmodus unterscheidet.
     */
    private static let representationPlanMainColor = UIColor(named: "RepresentationPlanMainColor") ?? UIColor.gray
    
    /**
     * Erzeugt ein Vertretungsplanbild in der angegeben Größe mit den angegeben Vertretungsdaten.
     * - Parameters:
     *   - geometry: Die Größe, welche das Bild haben soll.
     *   - representation: Die Vertretungsplandaten
     * - Returns: Das Vertretungsplanbild in der angegeben Größe, welches mit den Vertretungsplandaten erzeugt wurde.
    */
    static func createImage(windowSize: CGSize, withRepresentation representation: DetailRepresentation) -> UIImage {
        // MARK: - set up
        
        var image = UIImage()
        
        var representationRules = representation.representationRules
        let representationRulesTuple = representationRules.replace(" ", with: "\n", everyXChars: 20)
        representationRules = representationRulesTuple.Match
        
        var rowsPerEntry = Double(representationRulesTuple.Value) * 3 / 4
        if (representation.course.contains("L-") && representationRulesTuple.Value == 1) {
            rowsPerEntry += 3.0 / 4
        }
        
        let numberOfRow = rowsPerEntry * Double(representation.hours.count) + 3
        
        var width = Double(windowSize.width)
        
        // Berechnet die Höhe für eine normale Vertretungsplanzeile.
        var normalHeight = width / 18.545454545454545
        var expandedHeight = 2 * normalHeight
        
        var height = numberOfRow * normalHeight
        
        // Überprüfung, ob das Bild zu hoch ist, was eine erneute Berechnung der Werte mit sich zieht.
        if (CGFloat(height) > windowSize.height) {
            height = Double(windowSize.height)
            normalHeight = height / numberOfRow
            expandedHeight = 2 * normalHeight
            width = normalHeight * 18.545454545454545
        }
        
        let heightPerEntry = normalHeight * rowsPerEntry
        
        image = image.resize(newSize: CGSize(width: width, height: height)) ?? image
        
        let textSize = CGFloat(width / 40)
        
        let normalFont = UIFont.systemFont(ofSize: textSize)
        
        // Die Mittelpunkte der Spalten
        var rows = [width / 2.0]
        rows.append(width / 14.30617283950617)
        rows.append(width / 3.3615837301587302)
        rows.append(width / 4.728724279835391)
        rows.append(width / 3.008854748603352)
        rows.append(width / 2.572407407407407)
        rows.append(width / 1.8780986001913171)
        rows.append(width / 1.331239722370529)
        rows.append(width / 1.056895353886235)
        
        // Die Breiten der Spalten
        var rowWidth = [rows[1] * 2]
        rowWidth.append((rows[5] - rows[4] + rows[3] - rowWidth[0]) * 2.55)
        rowWidth.append(rowWidth[1] / 2)
        rowWidth.append(rowWidth[1] / 4)
        rowWidth.append(rowWidth[3])
        rowWidth.append(rowWidth[0])
        rowWidth.append(rowWidth[1] * 0.9)
        rowWidth.append(width - rowWidth[0] - rowWidth[1] - rowWidth[5] - rowWidth[6])
        
        // Die Startpunkte der Spalten
        var rowBegin = [0.0]
        for i in 0..<6 {
            let secondIndex: Int
            if (i == 0) {
                secondIndex = i
            }
            else {
                secondIndex = i + 1
            }
            rowBegin.append(rowBegin[i] + rowWidth[secondIndex])
        }
        
        // Der Startpunkt der ersten drei Zeilenanfänge (Überschrift-Zeilen) und der ersten Vertretungszeile
        var lineBegin = [0.0]
        for i in 0...2 {
            lineBegin.append(lineBegin[i] + normalHeight)
        }
        
        // Mittelpunkt der Überschriftzeile inklusive der, welche größer ist.
        var lines = [normalHeight * 0.5]
        lines.append(normalHeight * 1.5)
        lines.append(expandedHeight / 2 + normalHeight)
        lines.append(normalHeight * 2.5)
        
        // Mittelpunkt der ersten Vertretungszeile
        lines.append((lines.last ?? 0) + (heightPerEntry / normalHeight + 1) / 2.0 * normalHeight)
        
        // MARK: - Überschriftrahmen zeichnen
        image = draw(rectangle: CGRect(x: rowBegin[0], y: lineBegin[0], width: width, height: normalHeight), onImage: image, inColor: dynamicBlack) ?? image
        image = draw(rectangle: CGRect(x: rowBegin[0] + 1, y: lineBegin[0] + 1, width: width - 2, height: normalHeight - 2), onImage: image, inColor: UIColor(named: "RepresentationPlanTitleColor") ?? UIColor.blue) ?? image
        
        image = draw(rectangle: CGRect(x: rowBegin[0], y: lineBegin[1], width: rowWidth[0], height: expandedHeight), onImage: image, inColor: dynamicBlack) ?? image
        image = draw(rectangle: CGRect(x: rowBegin[0] + 1, y: lineBegin[1], width: rowWidth[0] - 2, height: expandedHeight - 1), onImage: image, inColor: representationPlanMainColor) ?? image
        
        image = draw(rectangle: CGRect(x: rowBegin[1], y: lineBegin[1], width: rowWidth[1], height: normalHeight), onImage: image, inColor: dynamicBlack) ?? image
        image = draw(rectangle: CGRect(x: rowBegin[1], y: lineBegin[1], width: rowWidth[1] - 1, height: normalHeight - 1), onImage: image, inColor: representationPlanMainColor) ?? image
        
        image = draw(rectangle: CGRect(x: rowBegin[1], y: lineBegin[2], width: rowWidth[2], height: normalHeight), onImage: image, inColor: dynamicBlack) ?? image
        image = draw(rectangle: CGRect(x: rowBegin[1], y: lineBegin[2], width: rowWidth[2] - 1, height: normalHeight - 1), onImage: image, inColor: representationPlanMainColor) ?? image
        
        image = draw(rectangle: CGRect(x: rowBegin[2], y: lineBegin[2], width: rowWidth[3], height: normalHeight), onImage: image, inColor: dynamicBlack) ?? image
        image = draw(rectangle: CGRect(x: rowBegin[2], y: lineBegin[2], width: rowWidth[3] - 1, height: normalHeight - 1), onImage: image, inColor: representationPlanMainColor) ?? image
        
        image = draw(rectangle: CGRect(x: rowBegin[3], y: lineBegin[2], width: rowWidth[4], height: normalHeight), onImage: image, inColor: dynamicBlack) ?? image
        image = draw(rectangle: CGRect(x: rowBegin[3], y: lineBegin[2], width: rowWidth[4] - 1, height: normalHeight - 1), onImage: image, inColor: representationPlanMainColor) ?? image
        
        image = draw(rectangle: CGRect(x: rowBegin[4], y: lineBegin[1], width: rowWidth[5], height: expandedHeight), onImage: image, inColor: dynamicBlack) ?? image
        image = draw(rectangle: CGRect(x: rowBegin[4], y: lineBegin[1], width: rowWidth[5] - 1, height: expandedHeight - 1), onImage: image, inColor: representationPlanMainColor) ?? image
        
        image = draw(rectangle: CGRect(x: rowBegin[5], y: lineBegin[1], width: rowWidth[6], height: expandedHeight), onImage: image, inColor: dynamicBlack) ?? image
        image = draw(rectangle: CGRect(x: rowBegin[5], y: lineBegin[1], width: rowWidth[6] - 1, height: expandedHeight - 1), onImage: image, inColor: representationPlanMainColor) ?? image
        
        image = draw(rectangle: CGRect(x: rowBegin[6], y: lineBegin[1], width: rowWidth[7], height: expandedHeight), onImage: image, inColor: dynamicBlack) ?? image
        image = draw(rectangle: CGRect(x: rowBegin[6], y: lineBegin[1], width: rowWidth[7] - 1, height: expandedHeight - 1), onImage: image, inColor: representationPlanMainColor) ?? image
        
        // MARK: - Überschrifttexte zeichnen
        image = draw(text: "representation for class ".localized + representation.nameOfClass, onImage: image, atPoint: CGPoint(x: rows[0], y: lines[0]), withFont: UIFont.boldSystemFont(ofSize: textSize), inColor: representationPlanMainColor)
        image = draw(text: "date".localized, onImage: image, atPoint: CGPoint(x: rows[1], y: lines[2]), withFont: normalFont)
        image = draw(text: "planned lesson".localized, onImage: image, atPoint: CGPoint(x: rows[2] * 1.01, y: lines[1]), withFont: normalFont)
        image = draw(text: "teacher".localized, onImage: image, atPoint: CGPoint(x: rows[3] * 1.03, y: lines[3]), withFont: normalFont)
        image = draw(text: "hour".localized, onImage: image, atPoint: CGPoint(x: rows[4] * 1.03, y: lines[3]), withFont: normalFont)
        image = draw(text: "course".localized, onImage: image, atPoint: CGPoint(x: rows[5] * 1.085, y: lines[3]), withFont: normalFont)
        image = draw(text: "representation through".localized, onImage: image, atPoint: CGPoint(x: rows[6], y: lines[2]), withFont: normalFont)
        image = draw(text: "representation rules".localized, onImage: image, atPoint: CGPoint(x: rows[7], y: lines[2]), withFont: normalFont)
        image = draw(text: "in room".localized, onImage: image, atPoint: CGPoint(x: rows[8], y: lines[2]), withFont: normalFont)
        
        // MARK: - Vertretungen mit Rahmen und Texten zeichnen
        
        for hour in representation.hours {
            image = draw(rectangle: CGRect(x: rowBegin[0], y: lineBegin.last ?? 0, width: rowWidth[0], height: heightPerEntry), onImage: image, inColor: dynamicBlack) ?? image
            image = draw(rectangle: CGRect(x: rowBegin[0] + 1, y: lineBegin.last ?? 0, width: rowWidth[0] - 2, height: heightPerEntry - 1), onImage: image, inColor: representationPlanMainColor) ?? image
            
            image = draw(rectangle: CGRect(x: rowBegin[1], y: lineBegin.last ?? 0, width: rowWidth[2], height: heightPerEntry), onImage: image, inColor: dynamicBlack) ?? image
            image = draw(rectangle: CGRect(x: rowBegin[1], y: lineBegin.last ?? 0, width: rowWidth[2] - 1, height: heightPerEntry - 1), onImage: image, inColor: representationPlanMainColor) ?? image
            
            image = draw(rectangle: CGRect(x: rowBegin[2], y: lineBegin.last ?? 0, width: rowWidth[3], height: heightPerEntry), onImage: image, inColor: dynamicBlack) ?? image
            image = draw(rectangle: CGRect(x: rowBegin[2], y: lineBegin.last ?? 0, width: rowWidth[3] - 1, height: heightPerEntry - 1), onImage: image, inColor: representationPlanMainColor) ?? image
            
            image = draw(rectangle: CGRect(x: rowBegin[3], y: lineBegin.last ?? 0, width: rowWidth[4], height: heightPerEntry), onImage: image, inColor: dynamicBlack) ?? image
            image = draw(rectangle: CGRect(x: rowBegin[3], y: lineBegin.last ?? 0, width: rowWidth[4] - 1, height: heightPerEntry - 1), onImage: image, inColor: representationPlanMainColor) ?? image
            
            image = draw(rectangle: CGRect(x: rowBegin[4] , y: lineBegin.last ?? 0, width: rowWidth[5], height: heightPerEntry), onImage: image, inColor: dynamicBlack) ?? image
            image = draw(rectangle: CGRect(x: rowBegin[4], y: lineBegin.last ?? 0, width: rowWidth[5] - 1, height: heightPerEntry - 1), onImage: image, inColor: representationPlanMainColor) ?? image
            
            image = draw(rectangle: CGRect(x: rowBegin[5], y: lineBegin.last ?? 0, width: rowWidth[6], height: heightPerEntry), onImage: image, inColor: dynamicBlack) ?? image
            image = draw(rectangle: CGRect(x: rowBegin[5], y: lineBegin.last ?? 0, width: rowWidth[6] - 1, height: heightPerEntry - 1), onImage: image, inColor: representationPlanMainColor) ?? image
            
            image = draw(rectangle: CGRect(x: rowBegin[6], y: lineBegin.last ?? 0, width: rowWidth[7], height: heightPerEntry), onImage: image, inColor: dynamicBlack) ?? image
            image = draw(rectangle: CGRect(x: rowBegin[6], y: lineBegin.last ?? 0, width: rowWidth[7] - 1, height: heightPerEntry - 1), onImage: image, inColor: representationPlanMainColor) ?? image
            
            image = draw(text: representation.date, onImage: image, atPoint: CGPoint(x: rows[1], y: lines.last ?? 0), withFont: normalFont)
            image = draw(text: representation.teacher, onImage: image, atPoint: CGPoint(x: rows[3] * 1.03, y: lines.last ?? 0), withFont: normalFont)
            image = draw(text: hour, onImage: image, atPoint: CGPoint(x: rows[4] * 1.03, y: lines.last ?? 0), withFont: normalFont)
            image = draw(text: representation.representation, onImage: image, atPoint: CGPoint(x: rows[6], y: lines.last ?? 0), withFont: normalFont)
            image = draw(text: representationRules, onImage: image, atPoint: CGPoint(x: rows[7], y: lines.last ?? 0), withFont: normalFont)
            
            if let room = representation.room {
                image = draw(text: room, onImage: image, atPoint: CGPoint(x: rows[8], y: lines.last ?? 0), withFont: normalFont)
            }
            
            var course = representation.course
            if (course.contains("L-")) {
                let courseParts = course.split(separator: "-")
                course = courseParts[0] + "-\n" + courseParts[1]
            }
            image = draw(text: course, onImage: image, atPoint: CGPoint(x: rows[5] * 1.085, y: lines.last ?? 0), withFont: normalFont)
            
            // Zeilenbeginn und -mittelpunt für die nächste Vertretung hinzufügen
            lineBegin.append((lineBegin.last ?? 0) + heightPerEntry)
            lines.append((lines.last ?? 0) + heightPerEntry)
        }
        
        return image
    }
    
    /**
     Zeichnet das angegebene Rechteck auf das angegebenen Bild in der angegebenen Farbe und gibt es zurück.
     
     - Parameters:
        - rectangle: Das Rechteck, welche gezeichnet werden soll.
        - image: Das Bild, auf welches das Rechteck gezeichnet werden soll.
        - color: Die Farbe, in der das Rechteck gezeichnet werden soll.
     
     - Returns: Das Bild, auf das das Rechteck in der angegebenen Farbe gezeichnet wurde.
     */
    private static func draw(rectangle: CGRect, onImage image: UIImage, inColor color: UIColor) -> UIImage? {
        // Den Bildcontext mit dem angegeben Bild einrichten.
        UIGraphicsBeginImageContextWithOptions(image.size, true, image.scale)
        
        image.draw(at: CGPoint.zero)
        
        color.setFill()
        UIRectFill(rectangle)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    /**
     Zeichnet den angegebenen Text auf das angegebenen Bild in der angegebenen Farbe mit der angegebenen Schriftart und gibt es zurück.
    
     - Parameters:
        - text: Der Text, welche gezeichnet werden soll.
        - image: Das Bild, auf welches der Text gezeichnet werden soll.
        - point: Der Punkt, an dem der Text gezeichnet werden soll.
        - textFont: Die Schriftart, in der der Text gezeichnet werden soll.
        - textColor: Die Farbe, in der der Text gezeichnet werden soll.
    
     - Returns: Das Bild, auf dem der Text in der angegebenen Farbe mit der angegebenen Schriftart an dem angegeben Punkt gezeichnet wurde.
    */
    private static func draw(text: String, onImage image: UIImage, atPoint point: CGPoint, withFont textFont: UIFont, inColor textColor: UIColor = dynamicBlack) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(image.size, true, image.scale)
        
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
        ]
        
        image.draw(at: CGPoint.zero)
        
        let textSize = text.size(withFont: textFont)
        
        var startHeight = point.y - textSize.height / 2
        
        if (text.contains("\n")) {
            let textParts = text.split(separator: "\n")
            let heightPerRow = String(textParts[0]).size(withFont: textFont).height
            for textPart in textParts {
                let textPart = String(textPart)
                
                // Erstellt einen Rechteck, in das der Text gezeichnet wird.
                let rect: CGRect = CGRect(x: point.x - textPart.size(withFont: textFont).width / 2, y: startHeight, width: textSize.width, height: textSize.height)
                // Zeichnet den Text in dem angegebene Rechteck in das Bild.
                textPart.draw(in: rect, withAttributes: textFontAttributes)
                
                startHeight += heightPerRow
            }
        }
        else {
            // Erstellt einen Rechteck, in das der Text gezeichnet wird.
            let rect: CGRect = CGRect(x: point.x - textSize.width / 2, y: startHeight, width: textSize.width, height: textSize.height)
            // Zeichnet den Text in dem angegebene Rechteck in das Bild.
            text.draw(in: rect, withAttributes: textFontAttributes)
        }
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext() ?? image
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    /**
     Ändert die Größe des Bildes auf die neue Größe und gibt es zurück.
     
     - Parameter newSize: Die neue Größe des Bildes.
     
     - Returns: Das Bild mit der geänderten Größe.
     */
    func resize(newSize:CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(newSize, true, UIScreen.main.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
