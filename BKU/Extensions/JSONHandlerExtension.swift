//
//  JSONHandlerExtension.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import UIKit
import FirebaseMessaging

extension JSONHandler {
    /**
     Konvertiert eine Serverantwort für den angegeben Typ zu einem Objekt vom Typ T und ruft mit ihm die Abschlussmethode auf. Behandlet die Fehlermeldungen für die iPhone und iPad App.

     - Parameters:
        - T: Der Typ des Objektes, welches aus der JSON Antwort konvertiert wird.
        - typeIndex: Der Typ der Serveranfrage.
        - type: Der Typ des Objektes, welches aus der JSON Antwort konvertiert wird. Kann anstelle von T gesetzt werden.
        - queryParams: Die Parameter, welche mit der Anfrage übermittelt werden.
        - model: Das Model der aktuellen Szene (Fenster).
        - completitionHandler: Die Methode, welche aufgerufen wird, wenn die Anfrage erfolgreich konvertiert wurde.
    */
    static func load<T: Decodable>(_ typeIndex: Int, as type: T.Type = T.self, queryParams: [String: Any] = [:], sceneModel model: SceneModel, completitionHandler: Action<T>? = nil) {
        load(typeIndex, queryParams: queryParams, completitionHandler:  completitionHandler) { error in
            
            model.showLoadingCircle = false
            
            switch (error) {
            case "invalid token":
                ApplicationModel.shared.loggedIn = false
                UIApplication.shared.shortcutItems = []
                Messaging.messaging().unsubscribe(fromTopic: UserDefaults.shared?.string(forKey: "username")?[0..<4].uppercased(with: Locale.german) ?? "")
                model.alertTitle = "session expired".localized
                model.alertMessage = "your session expired. please login again.".localized
            case "no network":
                model.alertTitle = "no network".localized
                model.alertMessage = "there is no active network connection".localized
            case "parsing error":
                fallthrough
            case "server error":
                model.alertTitle = "server error".localized
                model.alertMessage = "error at communciating with the server".localized
            default:
                return
            }
            
            model.showAlert = true
        }
    }
}
