//
//  RoomDetailView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche den Raumplan zeigt.
 */
struct RoomDetailView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die ID des Raumes, dessen Plan angezeigt werden soll.
     */
    let id: Int
    
    /**
     Die niedrigste Stunde des Raumplanes, der angezeigt wird.
     */
    @State private var minHours = 0
    /**
     Die höchste Stunde des Raumplanes, der angezeigt wird.
     */
    @State private var maxHours = 0
    /**
     Die Raumplandaten, welche angezeigt werden.
     */
    @State private var scheduleData = ScheduleWeek.empty
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        VStack {
            ScheduleHeaderView(loadSchedule: {}, name: $scheduleData.name, week: $scheduleData.week, currentTeacherID: .constant(0), differentTeacher: .constant(false))
            ScheduleTableView(loadScheduleData: {}, touchGesturesEnabled: false, currentWeek: .constant(0), currentHour: $scheduleData.currentHour, days: $scheduleData.days, detailsShownBefore: .constant(false), minHours: $minHours, maxHours: $maxHours, currentTeacherID: .constant(0))
        }
        .navigationBarTitle(scheduleData.name)
        .onAppear {
            if (self.scheduleData == ScheduleWeek.empty) {
                self.sceneModel.showLoadingCircle = true
                
                // Laden der Raumplan-Informationen
                let params = ["id": self.id]
                JSONHandler.load(111, as: ScheduleWeek.self, queryParams: params, sceneModel: self.sceneModel) { scheduleWeek in
                    self.scheduleData = scheduleWeek
                    self.minHours = TimeHandler.getMinHours(forWeek: scheduleWeek)
                    self.maxHours = TimeHandler.getMaxHours(forWeek: scheduleWeek)
                    self.sceneModel.showLoadingCircle = false
                }
            }
        }
    }
}
