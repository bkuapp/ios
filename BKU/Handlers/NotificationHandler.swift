//
//  NotificationHandler.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import UserNotifications

/**
 Der Handler. welche Benachrichtigung sendet und plant.
 */
class NotificationHandler {
    /**
     Überprüft, ob der Nutzer Benachrichtigungen für diese App erlaubt hat.
     
     - Parameter onSuccess: Die Methode, welche aufgerufen wird, wenn der Benutzer-Benachrichtigungen aktiviert hat.
     */
    static func checkAuthorization(onSuccess: @escaping () -> ()) {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            switch settings.authorizationStatus {
            case .notDetermined:
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
                    if (granted && error == nil) {
                        onSuccess()
                    }
                }
            case .authorized, .provisional:
                onSuccess()
            default:
                break
            }
        }
    }
    
    /**
     Plant eine Benachrichtigung zu der angegeben Zeit.
     
     - Parameters:
        - title: Der Titel der Benachrichtigung.
        - message: Die Nachricht der Benachrichtigung.
        - time: Die Zeit, zu der die Benachrichtigung angezeigt werden soll.
        - id: Die ID der Benachrichtigung.
        - infoID: Der ID der Benachrichtigungsinformationen.
     */
    static func scheduleNotification(withTitle title: String, andMessage message: String, atTime time: DateComponents, notificationID id: String, userInfoID infoID: Int, withSound sound: UNNotificationSound = .default) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        content.sound = sound
        
        if (infoID != 0) {
            content.userInfo = ["type": "exam", "id": infoID]
        }

        let trigger = UNCalendarNotificationTrigger(dateMatching: time, repeats: false)

        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)

        UNUserNotificationCenter.current().add(request) { error in
            guard error == nil else { return }
        }
    }
    
    /**
     Lädt Daten vom Server und plant damit alle Benachrichtigungen für die Klausuren.
     
     - Parameter sceneModel: Das Model von dem Fenster, bei dem die Benachrichtigungen geplant werden.
     */
    static func scheduleExamNotification(sceneModel: SceneModel) {
        deleteAllExamNotification()
        
        checkAuthorization {
            let options = ["1h", "3h", "6h", "12h", "1d", "3d", "7d"]
            let currentOption = UserDefaults.standard.string(forKey: "examNotificationTime") ?? ""
            guard let index = options.firstIndex(of: currentOption) else {
                return
            }
            
            JSONHandler.load(105, as: [NotificationExam].self, queryParams: ["mode": index], sceneModel: sceneModel) { exams in
                for exam in exams {
                    let date = Date(timeIntervalSince1970: Double(exam.notificationTimestamp))
                    
                    let id = "Exam Notification \(exam.id)"
                    let title = "approaching exam".localized
                    let message = exam.course + " at ".localized + exam.examDate
                    
                    let dateTimeComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date)
                    
                    if (dateTimeComponents.isValidDate(in: Calendar.current)) {
                        scheduleNotification(withTitle: title, andMessage: message, atTime: dateTimeComponents, notificationID: id, userInfoID: exam.id)
                    }
                }
            }
        }
    }
    
    /**
     Löscht alle Klausurbenachrichtigungen, welche momentan geplannt sind.
     */
    static func deleteAllExamNotification() {
        let notificationCenter = UNUserNotificationCenter.current()
        var ids: [String] = []
        notificationCenter.getPendingNotificationRequests() { requests in
            for request in requests {
                if (request.identifier.starts(with: "Exam Notification")) {
                    ids.append(request.identifier)
                }
            }
            notificationCenter.removePendingNotificationRequests(withIdentifiers: ids)
        }
    }
}
