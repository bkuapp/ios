//
//  NotificationExam.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Die Struktur für eine Benachrichtigung für Klausuren.
 */
struct NotificationExam: Hashable, Codable {
    /**
     Die ID der Klausur, zu welcher die Benachrichtigung gesendet werden soll.
     */
    var id: Int
    /**
     Der Name des Kurses über den die Klausur geht.
     */
    var course: String
    /**
     Der Zeitpunkt, zu dem die Benachrichtigung geschickt wird.
     */
    var notificationTimestamp: Int
    /**
     Das Datum, an dem die Klausur stattfindet.
     */
    var examDate: String
}
