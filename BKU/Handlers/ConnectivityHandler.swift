//
//  ConnectivityHandler.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import WatchConnectivity

/**
 Der Handler, welcher Nachrichten an die verbundene Apple Watch schickt und welche von ihr empfängt.
 */
class ConnectivityHandler: NSObject, WCSessionDelegate {
    /**
     Die Singleton-Instanz des Konnektivitäts-Handler.
     */
    static let shared = ConnectivityHandler()

    /**
     Die aktuelle Sitzung mit der Apple Watch.
     */
    private var session = WCSession.default

    /**
     Der Konstruktur des Handlers, welcher die Sitzung instanziert.
     */
    private override init() {
        super.init()
        session.delegate = self
        session.activate()
    }

    /**
     Wird aufgerufen, wenn die Sitzung mit der Apple Watch mit oder ohne Erfolg aktiviert wurde.
     
     - Parameters:
        - session: Die Sitzung mit der Apple Watch.
        - activationState: Der Status der Aktivierung.
        - error: Der Fehler, welcher bei der AKtivierung aufgetreten ist. Ist null, wenn kein Fehler aufgertreten ist.
     */
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        NSLog("%@", "activationDidCompleteWith activationState:\(activationState) error:\(String(describing: error))")
    }
    
    /**
     Wird aufgerufen, wenn eine Nachricht von der Apple Watch eingetroffen ist.
     
     - Parameters:
        - session: Die Sitzung mit der Apple Watch.
        - message: Die Nachricht, welche eingetroffen ist.
        - replyHandler: Die Methode, welche zum Antworten auf die Nachricht genutzt werden sollte.
     */
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        if (message["key"] as? String == "loggedIn") {
            let jwt = UserDefaults.shared?.string(forKey: "token") ?? ""
            
            let jwtPayload = JWTHandler.decode(jwtToken: jwt)
            let time = Double(jwtPayload["exp"] as? Int ?? 0)
            
            if (time > Date().timeIntervalSince1970) {
                replyHandler([
                    "type": "loggedIn",
                    "username": UserDefaults.shared?.string(forKey: "username") ?? "",
                    "loggedIn": UserDefaults.shared?.bool(forKey: "loggedIn") ?? false,
                    "token": UserDefaults.shared?.string(forKey: "token") ?? ""
                ])
            } else {
                replyHandler([
                    "error": "your session on phone has expired too".localized
                ])
            }
        }
    }
    
    /**
     Wird aufgerufen, wenn die Sitzung inaktiv wurde. Aktiviert die Sitzung erneut.
     
     - Parameter session: Die Sitzung, welche inaktiv wurde.
     */
    func sessionDidBecomeInactive(_ session: WCSession) {
        session.activate()
        NSLog("Session did become active")
    }
    
    /**
     Wird aufgerufen, wenn die Sitzung deaktiviert wurde. Aktiviert die Sitzung erneut.
    
     - Parameter session: Die Sitzung, welche deaktiviert wurde.
     */
    func sessionDidDeactivate(_ session: WCSession) {
        session.activate()
    }
    
    /**
     Sendet eine Nachricht mit den Daten von der Login-Anfrage (Nuzername, Einloggen erfolgreich und Servertoken) an die Apple Watch.
     
     - Parameter username: Der Nutzername, welcher mit übermittelt werden soll.
     */
    func sendLoginMessage(withUsername username: String) {
        let info: [String: Any] = [
            "type": "loggedIn",
            "username": username,
            "loggedIn": true,
            "token": UserDefaults.shared?.string(forKey: "token") ?? ""
        ]
        
        transferUserInfo(info)
    }

    /**
     Übermittelt Nuzerdaten an die Apple Watch.
     
     - Parameters:
        - userInfo: Die Nutzerdaten, welche übermittelt werden soll.
        - counter: Die Anzahl an Versuchen, wie häufig der Vorgang erneut versucht werden soll, wenn er fehlschlägt.
     */
    private func transferUserInfo(_ userInfo: [String : Any], _ counter: Int = 3) {
        if (session.isPaired && session.isWatchAppInstalled) {
            if (session.activationState == .activated) {
                if (session.isReachable) {
                    session.sendMessage(userInfo, replyHandler: nil, errorHandler: nil)
                }
                else {
                    session.transferUserInfo(userInfo)
                }
            }
            else if (counter > 0) {
                session.activate()
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5)) {
                    self.transferUserInfo(userInfo, counter - 1)
                }
            }
        }
    }
}
