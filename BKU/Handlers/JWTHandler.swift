//
//  JWTHandler.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Der Handler, welche den JWT-Token in seine Bestandteile zerlegt um zu überprüfen, ob dieser schon abgelaufen ist.
 */
class JWTHandler {
    /**
     Dekodiert den angegeben JWT-Token und gibt seinen Informationsteil zurück.
     
     - Parameter jwt: Der JWT-Token, welcher dekodiert werden soll.
     
     - Returns: Den mittleren Teil des JWT-Tokens in dekodierter Form.
     */
    static func decode(jwtToken jwt: String) -> [String: Any] {
        let segments = jwt.components(separatedBy: ".")
        if (segments.count > 1) {
            return decodeJWTPart(segments[1]) ?? [:]
        }
        return [:]
    }

    /**
     Dekodiert einen angegeben String asu Base64-Ebene und gibt ihn als Daten zurück.
     
     - Parameter value: Der String, welcher dekodiert werden soll.
     
     - Returns: Der Base64 dekodierte String als Daten.
     */
    static func base64UrlDecode(_ value: String) -> Data? {
        var base64 = value
        .replacingOccurrences(of: "-", with: "+")
        .replacingOccurrences(of: "_", with: "/")

        let length = Double(base64.lengthOfBytes(using: String.Encoding.utf8))
        let requiredLength = 4 * ceil(length / 4.0)
        let paddingLength = requiredLength - length
        if paddingLength > 0 {
            let padding = "".padding(toLength: Int(paddingLength), withPad: "=", startingAt: 0)
            base64 = base64 + padding
        }
        return Data(base64Encoded: base64, options: .ignoreUnknownCharacters)
    }

    /**
     Dekodiert einen Teil des JWT-Tokens und gibt ihn zurück.
     
     - Parameter jwtPart: Der Teil des JWT-Tokens, welcher dekodiert werden soll.
     
     - Returns: Den dekodierten Teil des JWT-Tokens.
     */
    static func decodeJWTPart(_ jwtPart: String) -> [String: Any]? {
        guard let bodyData = base64UrlDecode(jwtPart),
        let json = try? JSONSerialization.jsonObject(with: bodyData, options: []), let payload = json as? [String: Any] else {
          return nil
        }

        return payload
    }
}
