//
//  ActivityView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Erstellt und zeigt die Ansicht, welche aufgerufen wird, wenn etwas geteilt wird (Das Share-Sheet).
 */
struct ActivityView: UIViewControllerRepresentable {
    /**
     Die Items, welche geteilt werden sollen.
     */
    let activityItems: [Any]

    /**
     Wird aufgerufen, wenn Ansicht erstellt wird.
     
     - Parameter context: Der Kontext der Ansicht.
     
     - Returns: Die erstelle Ansicht.
     */
    func makeUIViewController(context: UIViewControllerRepresentableContext<ActivityView>) -> UIActivityViewController {
        return UIActivityViewController(activityItems: activityItems,
                                        applicationActivities: nil)
    }

    /**
     Wird aufgerufen, wenn die Ansicht überarbeitet wird.
     
     - Parameters:
        - uiViewController: Die Ansicht, welche überarbeitet werden soll.
        - context: Der Kontext der Ansicht.
     */
    func updateUIViewController(_ uiViewController: UIActivityViewController,
                                context: UIViewControllerRepresentableContext<ActivityView>) {}
}
