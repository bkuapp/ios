//
//  ApplicationModel.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Das Model, welches Informationen für die ganze App beinhaltet.
 */
class ApplicationModel: ObservableObject {
    /**
     Die Singletoninstanz des Models, welches für die ganze App (alle Fenster) genutzt wird.
     */
    static let shared = ApplicationModel()
    
    /**
     Ob der Nachtmodus aktiviert ist.
     */
    @Published var darkMode = false
    /**
     Ob der Nutzer gerade eingeloggt ist.
     */
    @Published var loggedIn = UserDefaults.shared?.bool(forKey: "loggedIn") ?? false
    
    /**
     Initalisiert das Model der Anwendung, damit es auf Veränderung der Nachtmodus Einstellung reagieren kann.
     */
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeEnabled(notification:)), name: .darkModeEnabled, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(darkModeDisabled(notification:)), name: .darkModeDisabled, object: nil)
    }
    
    /**
     Wird aufgerufen, wenn eine Benachrichtigung bezüglich der Aktivierung des Nachtmodus eingetroffen ist.
     
     - Parameter notification: Die Benachrichtigung, welche eingetroffen ist.
     */
    @objc func darkModeEnabled(notification: Notification) {
        darkMode = true
    }
    
    /**
     Wird aufgerufen, wenn eine Benachrichtigung bezüglich der Deaktivierung des Nachtmodus eingetroffen ist.
    
     - Parameter notification: Die Benachrichtigung, welche eingetroffen ist.
    */
    @objc func darkModeDisabled(notification: Notification) {
        darkMode = false
    }
}
