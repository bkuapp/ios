//
//  AppDelegate.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import UIKit
import FirebaseCore
import FirebaseMessaging
import FirebaseInstanceID

/**
 Das Delegat, welches den Lebenszyklus der App regelt.
 */
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate  {
    
    // MARK: - App Lebenszyklus
    
    /**
     Wird aufgerufen, wenn die App gestartet wird. Richtet die Benachrichtiungen und die Nutzerstandards ein.
     - Parameters:
        - application: Die gestartete App
        - launchOptions: Die Startoptionen, mit welcher die App gestartet wurden (z.B. Klick auf Benachrichtigung).
     
     - Returns: Wahr,  wenn die App URLs verarbeiten kann.
     */
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        registerUserDefaults()
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                NSLog("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                NSLog("Remote instance ID token: \(result.token)")
            }
        }
        
        return true
    }

    /**
     Wird aufgerufen, wenn das Registieren von den Push Benarichtigungen fehlschlägt.
     
     - Parameters:
        - application: Die App, bei der die Registrierung fehlgeschlagen ist.
        - error: Der Fehler, welcher aufgetreten ist.
     */
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
       NSLog("Unable to register for remote notifications: \(error.localizedDescription)")
    }

    /**
     Wird aufgerufen, wenn die App eine Push-Benachrichtigung erhält, während sie offen ist.
    
     - Parameters:
        - application: Die App, welche die Benachrichtigung erhält.
        - userInfo: Die Push-Benachrichtigung.
        - completionHandler: Die Methode, welche aufgerufen wird, wenn die Daten fertig verarbeitet sind um diese weiter zu reichen.
     */
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo["gcm.message_id"] {
           NSLog("Message ID: \(messageID)")
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }

    // MARK: - Benarichtigungen
    
    /**
     Wird aufgerufen, wenn ein neuer Benachrichtigungstoken benutzt wird.
     
     - Parameters:
        - messaging: Der Benachrichtigungsclient, bei dem ein neuer Token verfügbar ist.
        - fcmToken: Der neue Benachrichtigungstoken.
     */
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        NSLog("Firebase registration token: \(fcmToken)")
        
        if (UserDefaults.standard.bool(forKey: "loggedIn")) {
            let params: [String: Any] = [
                "oldToken": UserDefaults.standard.string(forKey: "token") ?? "",
                "newToken": fcmToken
            ]
            NetworkHandler.load(3, queryParams: params)
            UserDefaults.standard.set(fcmToken, forKey: "token")
            
            if let username = UserDefaults.shared?.string(forKey: "username"), UserDefaults.shared?.bool(forKey: "loggedIn") ?? false && !UserDefaults.isTeacher {
                messaging.subscribe(toTopic: (username.split(separator: "-").first ?? "").uppercased(with: Locale.german))
            }
        }
    }

    // MARK: - UISceneSession Lebenszyklus

    /**
     Wird aufgerufen, wenn eine neues Fenster erzeugt wird, um dieses zu konfigurieren.
     
     - Parameters:
        - application: Die App, für die das Fenster erzeugt werden soll.
        - connectingSceneSession: Das Fenster, welches erstellt wurde.
        - options: Die Optionen für das Fenster.
     
     - Returns: Die Konfiguration des Fensters.
     */
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    // MARK: - Nutzerstandards

    /**
     Initialisert die Nutzerstandards.
     */
    private func registerUserDefaults() {
        UserDefaults.shared?.register(defaults: ["loggedIn": false])
        UserDefaults.shared?.register(defaults: ["token": ""])
        UserDefaults.shared?.register(defaults: ["username": ""])
        
        UserDefaults.standard.register(defaults: ["authentication": false])
        UserDefaults.standard.register(defaults: ["token": ""])
        UserDefaults.standard.register(defaults: ["terms of use and privacy policy shown": false])
        
        UserDefaults.standard.register(defaults: ["representationNotification": true])
        // UserDefaults.standard.register(defaults: ["tutoringNotification": true])
        UserDefaults.standard.register(defaults: ["examNotification": true])
        UserDefaults.standard.register(defaults: ["examNotificationTime": "1d"])
        // UserDefaults.standard.register(defaults: ["homeWorkNotification": true])
        // UserDefaults.standard.register(defaults: ["homeWorkNotificationTime": "1d"])
    }
}

