//
//  LoadingView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ladekreis-Ansicht, welche den Ladekreis beinhaltet.
 */
struct LoadingView<Content>: View where Content: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die Ansicht, über der die Ladekreis-Ansicht angezeigt werden soll.
     */
    var content: () -> Content

    /**
     Der Körper der Ladekreis-Ansicht, welcher den Ladekreis und die darunter liegende Ansicht enthält.
     */
    var body: some View {
        ZStack(alignment: .center) {
            self.content()
                .disabled(self.sceneModel.showLoadingCircle)
                .blur(radius: self.sceneModel.showLoadingCircle ? 3 : 0)

            VStack {
                Text("loading...")
                ActivityIndicatorViewPresentable(isAnimating: self.$sceneModel.showLoadingCircle)
            }
            .frame(width: sceneModel.windowSize.width * 0.5, height: sceneModel.windowSize.height * 0.2)
            .background(Color.secondary.colorInvert())
            .foregroundColor(Color.primary)
            .cornerRadius(20)
            .opacity(self.sceneModel.showLoadingCircle ? 1 : 0)
        }
    }

}
