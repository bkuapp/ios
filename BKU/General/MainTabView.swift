//
//  MainTabView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Tabansicht, welche den Stundenplan, den Vertretungsplan, den Klausurtab für TG-Schüler, den Personentab und den Raumtab für Lehrer anzeigt.
 */
struct MainTabView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Ob die Einstellungen angezeigt werden.
     */
    @State private var showSettings = false
    
    /**
     DIe aktuellen Vertretungsdaten.
     */
    @State private var representations: [[Representation]] = []
    /**
     Die aktuellen Stundenplandaten.
     */
    @State private var week: ScheduleWeek = ScheduleWeek.empty
    /**
     Die aktuellen Klausurdaten.
     */
    @State private var examSections: [ExamQuarter] = []
    /**
     Die aktuellen Persondaten.
     */
    @State private var persons: [[Person]] = []
    /**
     Die aktuellen Raumdaten.
     */
    @State private var rooms: [Room] = []

    /**
     Die aktuelle Stundenplanwoche, welche dem Nutzer angezeigt wird.
     */
    @State private var currentWeek = 0
    /**
     Der aktuelle LehrerID, welche gesetzt wird, wenn der Lehrer über die Lehrerliste des Stundenplans einen anderen Lehrer auswählt.
     */
    @State private var currentTeacherID = 0
    /**
     Ob ein Lehrer einen anderen Lehrer als sich selber ausgewählt hat.
     */
    @State private var differentTeacher = false
    
    /**
     Die erste Stunde der aktuellen Schulwoche.
     */
    @State private var minHours = 1
    /**
     Die letzte Stunde der aktuellen Schulwoche.
     */
    @State private var maxHours = 17
    
    /**
     Ob die Stundenplandaten gerade geladen werden.
     */
    @State private var scheduleLoading = true
    /**
     Ob die Vertretungsplandaten gerade geladen werden.
     */
    @State private var representationLoading = true
    /**
     Ob die Klausurdaten gerade geladen werden.
     */
    @State private var examLoading = true
    /**
     Ob die Personendaten gerade geladen werden.
     */
    @State private var personLoading = true
    /**
     Ob die Raumdaten gerade geladen werden.
     */
    @State private var roomsLoading = true
    
    /**
     Der Körper der Tab-Ansicht, welcher die Tabbar und die zum aktuellen Tab passende Ansicht beinhaltet.
     */
    var body: some View {
        VStack {
            if (showSettings) {
                SettingsView(showSettings: $showSettings)
            }
            else {
                TabView(selection: $sceneModel.tabIndex) {
                    //Stundenplan
                    NavigationView {
                        ScheduleView(reload: loadData, loadScheduleData: loadScheduleData, showSettings: $showSettings, currentWeek: $currentWeek, week: $week, currentTeacherID: $currentTeacherID, differentTeacher: $differentTeacher, minHours: $minHours, maxHours: $maxHours, scheduleLoading: $scheduleLoading)
                    }
                        .tabItem {
                            Group {
                                Image(systemName: "calendar")
                            }
                        }
                        .tag(0)
                        .navigationViewStyle(StackNavigationViewStyle())
                    // Vertretungen
                    NavigationView {
                        RepresentationTableView(loadData: loadData, representations: $representations, representationLoading: $representationLoading, showSettings: $showSettings)
                            .navigationBarTitle("representations")
                    }
                        .tabItem {
                            Group {
                                Image(systemName: "shuffle")
                            }
                        }
                        .tag(1)
                        .navigationViewStyle(StackNavigationViewStyle())
                    // Klausuren
                    if (UserDefaults.isTGStudent) {
                        NavigationView {
                            ExamPlanView(reload: loadData, showSettings: $showSettings, examSections: $examSections, examLoading: $examLoading)
                        }
                        .tabItem {
                            Group {
                                Image(systemName: "book.fill")
                            }
                        }
                        .tag(2)
                        .navigationViewStyle(StackNavigationViewStyle())
                    }
                    // Personen
                    NavigationView {
                        PersonListView(persons: $persons, teacherLoading: $personLoading)
                            .navigationBarTitle(UserDefaults.isTeacher ? "persons" : "teacher list")
                            .navigationBarItems(trailing: StandardMenuIconsView(reload: loadData, showSettings: $showSettings))
                    }
                        .tabItem {
                            Group {
                                Image(systemName: "person.2.fill")
                            }
                        }
                        .tag(3)
                        .navigationViewStyle(StackNavigationViewStyle())
                    // Räume
                    if (UserDefaults.isTeacher) {
                        NavigationView {
                            RoomListView(rooms: $rooms, roomsLoading: $roomsLoading)
                            .navigationBarTitle("rooms")
                            .navigationBarItems(trailing: StandardMenuIconsView(reload: loadData, showSettings: $showSettings))
                        }
                        .tabItem {
                            Group {
                                Image("RoomIcon")
                            }
                        }
                        .tag(4)
                        .navigationViewStyle(StackNavigationViewStyle())
                    }
                    // Neuigkeiten
                    NavigationView {
                        NewsView(reload: loadData, showSettings: $showSettings)
                        .navigationBarTitle("news")
                    }
                    .tabItem {
                        Group {
                            Image(systemName: "doc.plaintext")
                        }
                    }
                    .tag(5)
                    .navigationViewStyle(StackNavigationViewStyle())
                }
            }
        }
        .onAppear {
            if (UIApplication.shared.isRegisteredForRemoteNotifications) {
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, error) in
                    guard error == nil else {
                        NSLog(error?.localizedDescription ?? "")
                        return
                    }
                }
                 
                UIApplication.shared.registerForRemoteNotifications()
            }
            
            if (!self.sceneModel.initialLoaded) {
                self.sceneModel.initialLoaded = true
                self.loadData()
            }
        }
    }
    
    /**
     Lädt die Daten vom Server und speichert sie in den passenden Objekten.
     */
    func loadData() {
        differentTeacher = false
        
        currentWeek = 0
        
        loadScheduleData()

        // Vertretungen
        representationLoading = true
        JSONHandler.load(101, as: [[Representation]].self, sceneModel: sceneModel) { representations in
            self.representations = representations
            self.representationLoading = false
            
            if (self.sceneModel.tabIndex == 1) {
                self.sceneModel.showLoadingCircle = false
            }
        }
        // Klausuren
        if (UserDefaults.isTGStudent) {
            examLoading = true
            JSONHandler.load(103, as: [ExamQuarter].self, sceneModel: sceneModel) { examSections in
                self.examSections = examSections
                self.examLoading = false
                
                if (self.sceneModel.tabIndex == 2) {
                    self.sceneModel.showLoadingCircle = false
                }
            }
            
            if (UserDefaults.standard.bool(forKey: "examNotification")) {
                NotificationHandler.scheduleExamNotification(sceneModel: self.sceneModel)
            }
        }
        
        // Personen
        personLoading = true
        JSONHandler.load(5, as: [[Person]].self, sceneModel: sceneModel) { teachers in
            self.persons = teachers
            self.personLoading = false
            
            if (self.sceneModel.tabIndex == 3) {
                self.sceneModel.showLoadingCircle = false
            }
        }
        
        // Räume
        if (UserDefaults.isTeacher) {
            roomsLoading = true
            JSONHandler.load(110, as: [Room].self, sceneModel: sceneModel) { rooms in
                self.rooms = rooms
                self.roomsLoading = false
                
                if (self.sceneModel.tabIndex == 4) {
                    self.sceneModel.showLoadingCircle = false
                }
            }
        }
    }
    
    /**
     Lädt die Stundenplandaten vom Server und ruft die angegebene Methode auf, wenn die Daten vollständig runtergeladen wurden.
     
     - Parameter onComplete: Die Methode, welche aufgerufen wird, wenn die Daten vollständig runtergeladen wurden.
     */
    func loadScheduleData(onCompelete: (() -> ())? = nil) {
        sceneModel.showLoadingCircle = true
        
        var scheduleParams: [String: Any] = [
            "inWeeks": currentWeek
        ]
        let type: Int
        if (differentTeacher) {
            type = 109
            scheduleParams["teacherID"] = currentTeacherID
        }
        else {
            type = 100
            currentTeacherID = 0
        }
        
        scheduleLoading = true
        JSONHandler.load(type, as: ScheduleWeek.self, queryParams: scheduleParams, sceneModel: sceneModel) { week in
            self.week = week
            
            self.minHours = TimeHandler.getMinHours(forWeek: week)
            self.maxHours = TimeHandler.getMaxHours(forWeek: week)
            
            self.scheduleLoading = false
            
            if (self.sceneModel.tabIndex == 0) {
                self.sceneModel.showLoadingCircle = false
            }
            
            if let onCompelete = onCompelete {
                onCompelete()
            }
        }
    }
}
