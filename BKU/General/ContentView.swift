//
//  ContentView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Hauptansicht, welche entweder die Tab-Ansicht oder die Login-Ansicht anzeigt.
 */
struct ContentView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    /**
     Das aktuelle Model der gesamten App, welches den Login-Status beinhaltet.
     */
    @EnvironmentObject private var applicationModel: ApplicationModel
    
    /**
     Der Körper von der Hauptansicht, welche die Struktur der Hauptansicht wiedergibt.
    */
    var body: some View {
        Group {
            if (self.applicationModel.loggedIn) {
                MainTabView()
            }
            else {
                NavigationView {
                    LoginView()
                }
                .navigationViewStyle(StackNavigationViewStyle())
            }
        }
        // Die Fehlermeldung, welche gegebenfalls dem Nutzer angezeigt wird.
        .alert(isPresented: $sceneModel.showAlert) {
            Alert(title: Text(sceneModel.alertTitle), message: Text(sceneModel.alertMessage), dismissButton: .default(Text("OK")))
        }
    }
}
