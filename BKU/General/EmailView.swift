//
//  EmailView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import MessageUI
import SwiftUI

/**
 Die Email-Ansicht, welche dafür genutzt werden kann, E-Mails zu schreiben.
 */
protocol EmailView: View {}

// Protokolle (Interfaces) können durch Erweiterungen auch vollständige Methoden haben.
extension EmailView {
    /**
     Präsentiert die Ansicht mit den angegebenen E-Mail-Adressen in dem angegebenen Fenster.
     
     - Parameters:
        - mails: Die E-Mail-Adressen, an die die E-Mail gehen soll.
        - window: Das Fenster, in dem die E-Mail Ansicht gezeigt werden soll.
     */
    func presentMailCompose(mails: [String], window: UIWindow?) {
        guard MFMailComposeViewController.canSendMail() else {
            return
        }
        let vc = window?.rootViewController
        let composeVC = MFMailComposeViewController()
        composeVC.setToRecipients(mails)
        composeVC.mailComposeDelegate = MailDelegate.shared

        vc?.present(composeVC, animated: true)
    }
}

/**
 Das Delegat, welches den Ablauf regelt, wenn die Mail-Ansicht fertig ist.
 */
class MailDelegate: NSObject, MFMailComposeViewControllerDelegate {
    /**
     Die Singleton-Instanz der Mail-Ansicht
     */
    static let shared = MailDelegate()
    
    /**
     Wird aufgerufen, wenn die Mail-Ansicht fertig ist.
     
     - Parameters:
        - controller: Der Kontroller der Mail-Ansicht, welche die Mail-Ansicht verschwinden lassen kann.
        - result: Das Ergebnis, welches die Mail-Ansicht erzielt hat.
        - error: Der Fehler, welcher bei der Mail-Ansicht aufgetreten ist. Wenn kein Fehler aufgetreten ist, ist dieser null.
     */
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
