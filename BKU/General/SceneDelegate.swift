//
//  SceneDelegate.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import UIKit
import SwiftUI

/**
 Das Delegat, welches den Lebenszyklus der Fenster regelt.
 */
class SceneDelegate: UIResponder, UIWindowSceneDelegate, UNUserNotificationCenterDelegate {
    /**
     Wahr, wenn der Nutzer FaceID oder TouchID aktiviert hat.
     */
    private var authenticationActivated: Bool {
        return UserDefaults.standard.bool(forKey: "authentication")
    }
    
    /**
     Der Blur-Effekt, welcher das Fenster überdecken kann.
     */
    private let blueEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
    /**
     Der Hauptkontroller des Fenster.
     */
    private var mainController: UIViewController?
    /**
     Das Model des aktuellen Fensters.
     */
    private var sceneModel: SceneModel?
    
    /**
     Das Fenster der aktuellen Szene.
     */
    var window: UIWindow?

    /**
     Wird aufgerufen, wenn ein neues Fenster erzeugt wird.
     
     - Parameters:
        - scene: Die Szene des Fensters
        - session: Die Session des Fensters.
        - connectionOptions: Die Optionen mit welchen das Fenster gestartet wird (Klick auf Benachrichtigung oder Kurzbefehlitem, Öffnung durch URL).
     */
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        if let windowScene = scene as? UIWindowScene {
            let _ = ConnectivityHandler.shared
            
            self.window = UIWindow(windowScene: windowScene)
            
            UNUserNotificationCenter.current().delegate = self
            
            let contentView = createView(scene: windowScene)
            
            mainController = MainUIHostingController(rootView: contentView, sceneModel: sceneModel)
            window?.rootViewController = mainController
            window?.makeKeyAndVisible()
            
            if let shortcutItem = connectionOptions.shortcutItem {
                handle(shortcutItem: shortcutItem)
            }
            
            if let urlContext = connectionOptions.urlContexts.first {
                handle(urlContext: urlContext)
            }
            
            if let notificationResponse = connectionOptions.notificationResponse {
                handle(notificationResponse: notificationResponse)
            }
        }
    }
    
    /**
     Wird aufgerufen, wenn das Fenster über eine URL geöffnet wird, während es im Hintergrund läuft.
     
     - Parameters:
        - scene: Das Fenster, welches mit der URL geöffnet wurde.
        - URLContexts: Die URL, mit der das Fenster geöffnet wurde.
     */
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        sceneModel?.tabIndex = -1
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
            if let urlContext = URLContexts.first {
                self.handle(urlContext: urlContext)
            }
        }
    }
    
    /**
     Wird aufgerufen, wenn das Fenster in den Hintergrund verschoben wird oder der App-Switcher aufgerufen wurde.
     
     - Parameter scene: Das Fenster, welches nicht mehr im Vordergrund ist.
     */
    func sceneWillResignActive(_ scene: UIScene) {
        showBlurView()
    }
    
    /**
     WIrd aufgerufen, wenn das Fenster aus dem App Switcher geöffnet wird, ohne vorher in den Hintergrund verschoben wurde.
     
     - Parameter scene: Das Fenster, welches wieder aktiv wurde.
     */
    func sceneDidBecomeActive(_ scene: UIScene) {
        if let window = self.window, self.blueEffectView.isDescendant(of: window) {
            hideBlurView()
        }
    }
    
    /**
     Wird aufgerufen, wenn das Fenster aus dem Hintergrund in den Vordergrund verschoben wird.
     
     - Parameter scene: Das Fenster, welches wieder in den Vordergrund verschoben wurde.
     */
    func sceneWillEnterForeground(_ scene: UIScene) {
        if (authenticationActivated) {
            hideBlurView()
            if let window = self.window, let rootViewController = window.rootViewController {
                var currentController = rootViewController
                while let presentedController = currentController.presentedViewController {
                    currentController = presentedController
                }
                if let sceneModel = sceneModel, currentController as? UIHostingController<AuthenticationView> == nil {
                    let controller = UIHostingController(rootView: AnyView(AuthenticationView(controller: UIViewController()).environmentObject(sceneModel)))
                    let authenticationView = AuthenticationView(controller: controller).environmentObject(sceneModel)
                    controller.rootView = AnyView(authenticationView)
                    
                    controller.view.backgroundColor = controller.view.backgroundColor?.withAlphaComponent(0)
                    controller.modalPresentationStyle = .overFullScreen
                    currentController.present(controller, animated: true, completion: nil)
                }
            }
        }
        
        sceneModel?.initialLoaded = false
    }
    
    /**
     Wird aufgeruden, wenn das Fenster in den Hintergrund verschoben wird.
     
     - Parameter scene: Das Fenster, welches in den Hintergrund verschoben wurde.
     */
    func sceneDidEnterBackground(_ scene: UIScene) {
        sceneModel?.scheduleID = 0
        sceneModel?.representationID = 0
        sceneModel?.examID = 0
    }
    
    /**
     Erstellt die Ansicht für das angegebene Fenster und gibt sie zurück.
     
     - Parameter scene: Das Fenster, für das die Ansicht erstellt werden soll.
     
     - Returns: Die erstellte Ansicht für das angegebene Fenster.
     */
    func createView(scene: UIWindowScene) -> some View {
        let sceneModel = SceneModel(isLandscape: scene.interfaceOrientation.isLandscape, window: scene.windows.first)
        self.sceneModel = sceneModel
        
        return ContentView().environmentObject(sceneModel).environmentObject(ApplicationModel.shared)
    }
    
    /**
     Wird aufgerufen, wenn das Fenster durch ein Kurzbefehlitem geöffnet wird, während es im Hintergrund ist.
     
     - Parameters:
        - windowScene: Das Fenster, welches geöffnet wurde.
        - shortcutItem: Das Kurzbefehlitem, auf das geklickt wurde.
     */
    func windowScene(_ windowScene: UIWindowScene, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: (Bool) -> Void) {
        sceneModel?.tabIndex = -1
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
            self.handle(shortcutItem: shortcutItem)
        }
    }
    
    /**
     Wird aufgerufen, wenn eine Benachrichtigung angezeigt werden soll, um die Präsentationsoptionen (Sound, Banner, etc.) zu konfigurieren.
     
     - Parameters:
        - center: Das Benachrichtigungscenter, in dem die Benachrichtigung angezeigt wird.
        - notification: Die Benachrichtigung, welche angezeigt wird.
        - completitionHandler: Die Methode, welche aufgerufen wird, um die Präsentationsoptionen zu übergeben.
     */
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    /**
     Wird aufgerufen, wenn der Nutzer auf eine Benachrichtigung klickt, während die App läuft.
     
     - Parameters:
        - center: Das Benachrichtigungscenter, in dem die Benachrichtigung angezeigt wurde.
        - response: Die Benachrichtigung, auf die geklickt wurde.
        - completitionHandler: Die Methode, welche aufgerufen wird, wenn der Klick abgearbeitet wurde.
     */
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        sceneModel?.tabIndex = -1
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
            self.handle(notificationResponse: response)
        }
        completionHandler()
    }
    
    /**
     Verarbeitet einen Klick auf ein Kurzbefehlitem.
     
     - Parameter item: Das Kurzbefehlitem, auf das geklickt wurde.
     */
    private func handle(shortcutItem item: UIApplicationShortcutItem) {
        let title = item.type

        switch title {
        case "de.bkukr.BKU.schedule":
            self.sceneModel?.tabIndex = 0
        case "de.bkukr.BKU.representation":
            self.sceneModel?.tabIndex = 1
        case "de.bkukr.BKU.examplan":
            self.sceneModel?.tabIndex = 2
        case "de.bkukr.BKU.teacherlist":
            fallthrough
        case "de.bkukr.BKU.classlist":
            self.sceneModel?.tabIndex = 3
        case "de.bkukr.BKU.rooms":
            self.sceneModel?.tabIndex = 4
        case "de.bkukr.BKU.news":
            self.sceneModel?.tabIndex = 5
        default:
            break
        }
    }
    
    /**
     Verarbeitet Öffnung durch eine URL.
     
     - Parameter context: die URL, mit der die App geöffnet wurde.
     */
    private func handle(urlContext context: UIOpenURLContext) {
        let url = context.url
        switch (url.scheme) {
        case "schedule":
            sceneModel?.tabIndex = 0
            sceneModel?.scheduleID = Int(url.host ?? "") ?? 0
        case "exam":
            sceneModel?.tabIndex = 2
            sceneModel?.examID = Int(url.host ?? "") ?? 0
            break
        default:
            break
        }
    }
    
    /**
     Verarbeitet einen Klick auf eine Benachrichtigung.
     
     - Parameter notification: Die Benachrichtigung, auf die geklickt wurde.
     */
    private func handle(notificationResponse notification: UNNotificationResponse) {
        let info = notification.notification.request.content.userInfo
        if let type = info["type"] as? String, let id = (info["id"] as? Int) ?? Int(info["id"] as? String ?? "") {
            if (type == "exam") {
                sceneModel?.tabIndex = 2
                sceneModel?.examID = id
            }
            else if (type == "representation") {
                sceneModel?.tabIndex = 1
                sceneModel?.representationID = id
            }
        }
    }
    
    /**
     Überdeckt das aktuelle Fenster mit einem Blur-Effekt.
     */
    private func showBlurView() {
        if let window = self.window, !self.blueEffectView.isDescendant(of: window) && authenticationActivated {
            self.blueEffectView.frame = window.bounds
            window.addSubview(self.blueEffectView)
        }
    }
    
    /**
     Entfernt den Blur-Effekt vom aktuellen Fenster.
     */
    private func hideBlurView() {
        self.blueEffectView.removeFromSuperview()
    }
}
