//
//  ActivityIndicatorViewPresentable.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Der Ladekreis, welcher während eines Ladevorgangs angezeigt wird.
 */
struct ActivityIndicatorViewPresentable: UIViewRepresentable {
    /**
     Ob der Ladekreis gerade am drehen ist oder nicht.
     */
    @Binding var isAnimating: Bool
    
    /**
     Wird aufgerufen, um den Ladekreis zu erstellen.
     
     - Parameter context: Der Kontext des Ladekreises.
     
     - Returns: Die erstellte Ladekreis.
     */
    func makeUIView(context: Context) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: .large)
    }

    /**
     Überarbeitet den Ladekreis (Animationsbeginn oder -ende).
     
     - Parameters:
        - uiView: Der Ladekreis.
        - context: Der Kontext des Ladekreises.
     */
    func updateUIView(_ uiView: UIActivityIndicatorView, context: Context) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
    }
}
