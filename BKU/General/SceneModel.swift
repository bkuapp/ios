//
//  SceneModel.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Das Model, welches Informationen für das aktuelle Fenster beinhaltet.
 */
class SceneModel: ObservableObject {
    /**
     Das Fenster, zu dem dieses Model gehört.
     */
    let window: UIWindow?
    
    /**
     Ob das Gerät momentan im vertikalen oder horizontalen Modus ist.
     */
    @Published var landscape = false
    /**
     Ob das Fensters groß genug ist, um die Texte größer anzuzeigen
     */
    @Published var biggerScreen = false
    /**
     Die Größe des aktuellen Fensters.
     */
    @Published var windowSize: CGSize
    
    /**
     Der aktuelle Tabindex.
     */
    @Published var tabIndex = 0
    /**
     Die Stundenplan ID, die gesetzt wird, wenn auf einen Eintrag in dem Stundenplan-Widget geklickt wird. Wenn sie ungleich 0 ist, wird eine Stunden Detail Ansicht angezeigt.
     */
    @Published var scheduleID = 0
    /**
     Die Vertretungsplan ID, die gesetzt wird, wenn auf eine Vertretungsplan Benachrichtigung geklickt wird. Wenn sie ungleich 0 ist, wird eine Detail Ansicht von einer Vertretung angezeigt.
     */
    @Published var representationID = 0
    /**
     Die Klausur ID, die gesetzt wird, wenn auf einen Eintrag in dem Klausur-Widget geklickt wird. Wenn sie ungleich 0 ist, wird eine Klausur Detail Ansicht angezeigt.
     */
    @Published var examID = 0
    
    /**
     Ob dem Nutzer gerade eine Fehlermeldung gezeigt wird.
     */
    @Published var showAlert = false
    /**
     Der Titel der aktuellen Fehlermeldung.
     */
    @Published var alertTitle = ""
    /**
     Die Nachricht der aktuellen Fehlermeldung.
     */
    @Published var alertMessage = ""
    
    /**
     Ob gerade ein Ladekreis angezeigt wird.
     */
    @Published var showLoadingCircle = false
    
    /**
     Ob die Daten schon einmal geladen wurden.
     */
    @Published var initialLoaded = false

    /**
     Erstellt eine Model mit dem aktuellen Fenster und der Orientierung des Gerätes. Registriert Beobachtermethoden für Benachrichtigungen zum Nachtmodus und zur Veränderung des Fensters.
     
     - Parameters:
        - isLandscape: Ob das Gerät aktuell im Querformat (Orientierung des Gerätes) ist.
        - window: Das aktuelle Fenster des Models.
     */
    init(isLandscape: Bool, window: UIWindow?) {
        self.window = window
        landscape = isLandscape
        biggerScreen = UIDevice.current.userInterfaceIdiom == .pad && (window?.frame.size.width ?? 0) > 700
        
        let safeAreaInsets = window?.safeAreaInsets
        let size = (window?.frame.size ?? CGSize(width: 0, height: 0))
        
        let width = size.width - (safeAreaInsets?.left ?? 0) - (safeAreaInsets?.right ?? 0)
        let height = size.height - (safeAreaInsets?.top ?? 0) - (safeAreaInsets?.bottom ?? 0)
        
        windowSize = CGSize(width: width, height: height)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onViewWillTransition(notification:)), name: .onViewWillTransition, object: nil)
    }

    /**
     Wird aufgerufen, wenn eine Benachrichtigung bezüglich Änderung zur Größe oder Orientierung des aktuellen Fensters eintrifft.
     
     - Parameter notification: Die Benachrichtigung, welche eingetroffen ist.
     */
    @objc func onViewWillTransition(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
            guard let size = self.window?.frame.size else {
                return
            }
            
            self.biggerScreen = UIDevice.current.userInterfaceIdiom == .pad && size.width > 700
            
            self.landscape = size.width > size.height
            
            let safeAreaInsets = self.window?.safeAreaInsets
            
            let width = size.width - (safeAreaInsets?.left ?? 0) - (safeAreaInsets?.right ?? 0)
            let height = size.height - (safeAreaInsets?.top ?? 0) - (safeAreaInsets?.bottom ?? 0)
            
            self.windowSize = CGSize(width: width, height: height)
        }
    }
}
