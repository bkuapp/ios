//
//  MainUIHostingController.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Der Hauptcontroller, welche das Fenster beinhaltet.
 
 - Parameter Content: Der Typ des Fensters, welches angezeigt wird.
 */
class MainUIHostingController<Content>: UIHostingController<Content> where Content: View {
    
    /**
     Das Model der Ansicht, welche mt diesem Kontroller angezeigt wird
     */
    var sceneModel: SceneModel? = nil
    
    /**
     Erstellt einen neuen Controller für das Fenster, welche die Ansicht anzeigt.
     
     - Parameters:
        - rootView: Die Ansicht, welche in dem Fenster angezeigt wird.
        - sceneModel: Das Model der Ansicht.
     */
    init(rootView: Content, sceneModel: SceneModel?) {
        super.init(rootView: rootView)
        
        self.sceneModel = sceneModel
    }

    /**
     Wird benötigt, wenn man einen eigenen Konstruktur erstellt. Ruft den Super-Konstruktur auf.
     
     - Parameter coder: Der Coder, welche an die Superklasse weitergegeben wird.
     */
    @objc required dynamic init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    /**
     Definiert die Tastatur-Shortcuts.
     */
    override var keyCommands: [UIKeyCommand]? {
        if (ApplicationModel.shared.loggedIn) {
            var result: [UIKeyCommand] = [
                UIKeyCommand(title: "schedule".localized, action: #selector(changeTab(sender:)), input: "1", modifierFlags: .command),
                UIKeyCommand(title: "representations".localized, action: #selector(changeTab(sender:)), input: "2", modifierFlags: .command)
            ]
            
            var index = 3
            
            if (UserDefaults.isTGStudent) {
                result.append(UIKeyCommand(title: "exam plan".localized, action: #selector(changeTab(sender:)), input: String(index), modifierFlags: .command))
                
                index += 1
            }
            
            result.append(UIKeyCommand(title: (UserDefaults.isTeacher ? "persons" : "teacher list").localized, action: #selector(changeTab(sender:)), input: String(index), modifierFlags: .command))
            
            index += 1
            
            if (UserDefaults.isTeacher) {
                result.append(UIKeyCommand(title: "rooms".localized, action: #selector(changeTab(sender:)), input: String(index), modifierFlags: .command))
                
                index += 1
            }
            
            result.append(UIKeyCommand(title: "news".localized, action: #selector(changeTab(sender:)), input: String(index), modifierFlags: .command))
            
            return result
        }
        
        return []
    }
    
    /**
     Wird aufgerufen, wenn das Fenster sichtbar ist.
     
     - Parameter animated: Ob das Auftauchen animiert war.
     */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        darkModeNotification()
    }

    /**
     Wird aufgerufen, wenn die Größe des Fensters sich verändert oder es sich dreht.
     
     - Parameters:
        - size: Die neue Größe des Fensters.
        - coordinater: Der Koordinator mit welchem das Fenster sich geändert hat.
     */
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        NotificationCenter.default.post(name: .onViewWillTransition, object: nil, userInfo: ["size": size])
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    /**
     Wird aufgerufen, wenn sich die Nachtmodus-Einstellungen ändern.
     
     - Parameter previousTraitCollection: Die vorherigen Nachtmodus-Enstellungen.
     */
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        darkModeNotification()
    }
    
    /**
     Sendet eine Nachtmodus-Benachrichtigung an das System, wenn die Nachtmodus-Einstellungen sich ändern.
     */
    func darkModeNotification() {
        let notificationName = traitCollection.userInterfaceStyle == .dark ? Notification.Name.darkModeEnabled : .darkModeDisabled
        
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    /**
     Wechselt den Tab passend zu dem gedrückten Tastatur Shortcut.
     
     - Parameter sender: Der Shortcut, welche gedrückt wurde.
     */
    @objc private func changeTab(sender: UIKeyCommand) {
        let tabIndex: Int
        
        switch (sender.title) {
        case "schedule".localized:
            tabIndex = 0
        case "representations".localized:
            tabIndex = 1
        case "exam plan".localized:
            tabIndex = 2
        case (UserDefaults.isTeacher ? "persons" : "teacher list").localized:
            tabIndex = 3
        case "rooms".localized:
            tabIndex = 4
        case "news".localized:
            tabIndex = 5
        default:
            tabIndex = 0
        }
        
        sceneModel?.tabIndex = tabIndex
    }
}
