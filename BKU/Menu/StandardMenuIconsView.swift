//
//  StandardMenuIconsView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import FirebaseMessaging

/**
 Die Ansicht, welche die Standard-Menü-Icons (Neuladen, Einstellungen, Ausloggen) enthält.
 */
struct StandardMenuIconsView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die Methode, welche aufgerufen wird, wenn auf das Neuladen-Icon geklickt wird.
     */
    let reload: () -> ()
    
    /**
     Ob die Einstellungen aktuell angezeigt werden.
     */
    @Binding var showSettings: Bool
    
    /**
     Der Körper der Ansicht, welcher die Icons enthält.
     */
    var body: some View {
        HStack(spacing: 30) {
            Image(systemName: "arrow.clockwise")
                .foregroundColor(.primary)
                .font(.system(size: 24))
                .background(Color.dynamicWhite)
                .onTapGesture {
                    self.reload()
                }
            Image("SettingsIcon")
                .foregroundColor(.primary)
                .background(Color.dynamicWhite)
                .onTapGesture {
                    withAnimation(.interactiveSpring()) {
                        self.showSettings = true
                    }
                }
            Image("LogoutIcon")
                .foregroundColor(.primary)
                .background(Color.dynamicWhite)
                .onTapGesture {
                    ApplicationModel.shared.loggedIn = false
                    UserDefaults.shared?.set(false, forKey: "loggedIn")
                    UIApplication.shared.shortcutItems = []
                    self.sceneModel.showLoadingCircle = false
                    
                    // Token vom Server löschen und Benachrichtigungs-Topic deabonnieren.
                    let params = [
                        "token": UserDefaults.standard.string(forKey: "token") ?? ""
                    ]
                    NetworkHandler.load(2, queryParams: params)
                    Messaging.messaging().unsubscribe(fromTopic: UserDefaults.shared?.string(forKey: "username")?[0..<4] ?? "".uppercased(with: Locale.german))
                    
                    NotificationHandler.deleteAllExamNotification()
                }
        }
    }
}
