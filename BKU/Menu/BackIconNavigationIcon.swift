//
//  BackIconNavigationIcon.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht des Zurück-Knopfes, welcher angezeigt wird, wenn man sich in einer Detailansicht befindet.
 */
struct BackIconNavigationIcon: View {
    /**
     Der Text der bei dem Zurück-Knopf angezeigt werden soll.
     */
    let text: String
    /**
     Die Methode, die ausgeführt wird, wenn auf den Zurück-Knopf gedrückt wird.
     */
    let onClick: ()->()
    
    /**
     Der Körper, welcher den Zurückknopf beinhaltet.
     */
    var body: some View {
        HStack(spacing: 5) {
            Image(systemName: "chevron.left")
                .font(.system(size: 25, weight: .semibold))
            Text(text)
        }
        .foregroundColor(.blue)
        .onTapGesture {
            withAnimation {
                self.onClick()
            }
        }
    }
}
