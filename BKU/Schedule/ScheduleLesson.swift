//
//  ScheduleLesson.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Speichert die Daten für einen Kurs, welcher im Stundenplan oder Raumplan angezeigt wird.
 */
struct ScheduleLesson: Hashable, Codable {
    /**
     Die ID des Kurses.
     */
    var id: Int
    /**
     Der Name der Klasse oder des Lehrers, der an dem Kurs teilnimmt.
     */
    var classOrTeacher: String
    /**
     Die Schulstunde, in der der Kurs anfängt.
     */
    var start: Int
    /**
     Die Schulstunde, in der der Kurs endet.
     */
    var end: Int
    /**
     Der Name des Faches, welches unterrichtet wird.
     */
    var course: String
    /**
     Der Raum oder Lehrer, welcher an diesem Kurs beteiligt ist.
     */
    var roomOrTeacher: String?
    /**
     Ob der Kurs vertreten wird.
     */
    var represented: Bool
    /**
     Ob der Kurs ausfällt.
     */
    var isCancelled: Bool
}
