//
//  ScheduleWeek.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Speichert die Daten für eine Schulwoche, die im Stundenplan oder Raumplan angezeigt wird.
 */
struct ScheduleWeek: Hashable, Codable {
    /**
     Eine Stundenplanwoche, welche benutzt wird, solange die Daten noch nicht geladen wurden
     */
    static let empty = ScheduleWeek(name: "", currentHour: 0, week: "", days: [])
    
    /**
     Der Name des Lehrers oder Schülers dessen Stundenplan bzw. der Name des Raumes dessen Raumplan gerade angezeigt wird.
     */
    var name: String
    /**
     Die aktuelle Schulstunde.
     */
    var currentHour: Int
    /**
     Die Schulwoche (Start- bis Enddatum), welche aktuell angezeigt wird.
     */
    var week: String
    /**
     Die Schultage, welche zu der aktuell angezeigten Schulwoche gehören.
     */
    var days: [ScheduleDay]
}
