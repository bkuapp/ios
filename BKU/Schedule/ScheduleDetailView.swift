//
//  ScheduleDetailView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die Stundenplandetails zeigt.
 */
struct ScheduleDetailView: EmailView {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die ID des Lehrers, dessen Stundenplan gerade angezeigt wird (nur für Lehrer).
     */
    @Binding var teacherID: Int
    
    /**
     Die Stunde, dessen Details gerade angezeigt werden.
     */
    @State private var lesson: DetailScheduleLesson? = nil
    /**
     Die Daten des Klassenstundenplans (Nur für Lehrer).
     */
    @State private var classScheduleData: ScheduleWeek = ScheduleWeek.empty
    /**
     Die niedrigste Stunde des Klassenstundenplans (Nur für Lehrer).
     */
    @State private var minHours = 1
    /**
     Die höchste Stunde des Klassenstundenplans (Nur für Lehrer),
     */
    @State private var maxHours = 17
    
    /**
     Ob die Stundenplandetails gerade am Laden sind.
     */
    @State private var scheduleDetailsLoading = true
    /**
     Ob der Klassenstundenplan gerade am Laden ist (Nur für Lehrer).
     */
    @State private var studentScheduleLoading = UserDefaults.isTeacher
    
    /**
     Die ID der Stunde, zu welcher die Details angezeigt werden sollen.
     */
    let id: Int
    /**
     Die Methode, welche ausgeführt wird, wenn auf den Zurück-Knopf gedrückt wird.
     */
    let onBack: () -> ()
    
    /**
     Erstellt eine Detail Ansicht mit der angegeben Lehrer ID, Kurs ID und Zurück-Funktion.
     
     - Parameters:
        - teacherID: Die IDs des Lehrers, dessen Stundenplan gerade angezeigt wird.
        - id: Die ID der Stunde, dessen Details angezeigt werden soll.
        - onBack: Die Methode, welche aufgerufen wird, wenn auf den Zurück-Knopf gedrückt wird.
     */
    init(teacherID: Binding<Int>, id: Int, onBack: @escaping () -> ()) {
        self._teacherID = teacherID
        self.id = id
        self.onBack = onBack
    }

    /**
     Der Körper der Ansicht, welcher dessen Struktur enthält.
     */
    var body: some View {
        let width = sceneModel.windowSize.width
        let biggerScreen = sceneModel.biggerScreen
        let start = lesson?.start ?? 0
        let end = lesson?.end ?? 0
        let email = lesson?.email ?? ""
        return LoadingView {
            VStack(spacing: 0) {
                HStack(spacing: 0) {
                    Group {
                        if (UserDefaults.isTeacher) {
                            Text("class")
                        }
                        else {
                            Text("teacher")
                        }
                    }
                    .frame(width: width * 0.3, height: biggerScreen ? 50 : 30)
                        .border(Color.primary, width: 2)
                        .padding(.trailing, -2)
                    Text(self.lesson?.name ?? "")
                        .frame(width: width * 0.7, height: biggerScreen ? 50 : 30)
                        .border(Color.primary, width: 2)
                }
                HStack(spacing: 0) {
                    Text("hours")
                        .frame(width: width * 0.3, height: biggerScreen ? 50 : 30)
                        .border(Color.primary, width: 2)
                        .padding(.trailing, -2)
                    Text("\(start). - \(end). (\(TimeHandler.buildStartTime(start: start)) - \(TimeHandler.buildEndTime(end: end)))")
                        .frame(width: width * 0.7, height: biggerScreen ? 50 : 30)
                        .border(Color.primary, width: 2)
                }
                .padding(.top, -2)
                HStack(spacing: 0) {
                    Text("email")
                        .frame(width: width * 0.3, height: biggerScreen ? 50 : 30)
                        .border(Color.primary, width: 2)
                        .padding(.trailing, -2)
                    Text(email)
                        .onTapGesture {
                            self.presentMailCompose(mails: [email], window: self.sceneModel.window)
                        }
                        .foregroundColor(.blue)
                    .frame(width: width * 0.7, height: biggerScreen ? 50 : 30)
                        .border(Color.primary, width: 2)
                }
                .padding(.top, -2)
                .padding(.bottom, 20)
                Button(action: self.sendEmailToAll) {
                    Group {
                        if (UserDefaults.isTeacher) {
                            Text("send email to all classes of this day")
                        }
                        else {
                            Text("send email to all teachers of this day")
                        }
                    }
                    .foregroundColor(Color.dynamicWhite)
                }
                .padding(3)
                .background(Color.primary)
                .cornerRadius(10)
                
                if (UserDefaults.isTeacher) {
                    Spacer()
                    .frame(height: 20)
                    
                    // Klassenstundenplan
                    LoadingView {
                        VStack {
                            Text("schedule for the class")
                                .font(.system(size: biggerScreen ? 55 : 25))
                            ScheduleHeaderView(loadSchedule: {}, name: self.$classScheduleData.name, week: self.$classScheduleData.week, currentTeacherID: .constant(0), differentTeacher: .constant(false))
                            ScheduleTableView(loadScheduleData: {}, touchGesturesEnabled: false, currentWeek: .constant(0), currentHour: self.$classScheduleData.currentHour, days: self.$classScheduleData.days, detailsShownBefore: .constant(false), minHours: self.$minHours, maxHours: self.$maxHours, currentTeacherID: .constant(0))
                        }
                    }
                }
                else {
                    Spacer()
                }
            }
            .font(.system(size: biggerScreen ? 35 : 15))
        }
        .navigationBarTitle(lesson?.course ?? "")
        .navigationBarItems(leading: BackIconNavigationIcon(text: "schedule".localized) {
            self.onBack()
        }, trailing: EmptyView())
        .onAppear {
            if (self.lesson == nil) {
                var params: [String: Any] = [
                    "id": self.id
                ]
                
                if (self.teacherID != 0) {
                    params["teacherID"] = self.teacherID
                }
                
                // Laden der Details
                self.sceneModel.showLoadingCircle = true
                JSONHandler.load(4, as: DetailScheduleLesson.self, queryParams: params, sceneModel: self.sceneModel) { lesson in
                    self.lesson = lesson
                    
                    self.scheduleDetailsLoading = false
                    self.sceneModel.showLoadingCircle = self.studentScheduleLoading
                }
                
                if (UserDefaults.isTeacher) {
                    var params: [String: Any] = [
                        "scheduleID": self.id,
                        "inWeeks": 0
                    ]
                    
                    if (self.teacherID != 0) {
                        params["teacherID"] = self.teacherID
                    }
                    
                    // Laden des Klassenstundenplans
                    JSONHandler.load(107, as: ScheduleWeek.self, queryParams: params, sceneModel: self.sceneModel) { scheduleWeek in
                        self.classScheduleData = scheduleWeek
                        self.minHours = TimeHandler.getMinHours(forWeek: scheduleWeek)
                        self.maxHours = TimeHandler.getMaxHours(forWeek: scheduleWeek)
                        
                        self.studentScheduleLoading = false
                        self.sceneModel.showLoadingCircle = self.scheduleDetailsLoading
                    }
                }
            }
        }
    }
    
    /**
     Sendet eine E-Mail an alle Lehrer oder Klassen, welche man an diesem Tag hat.
     */
    func sendEmailToAll() {
        var params: [String: Any] = [
            "courseID": self.id
        ]
        
        if (teacherID != 0) {
            params["teacherID"] = teacherID
        }
        
        // Laden der E-Mails des Tages
        JSONHandler.load(106, as: [String].self, queryParams: params, sceneModel: sceneModel) { emails in
            self.presentMailCompose(mails: emails, window: self.sceneModel.window)
        }
    }
}
