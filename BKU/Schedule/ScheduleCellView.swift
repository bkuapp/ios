//
//  ScheduleCellView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche einen einzelnen Kurs im Stundenplan anzeigt.
 */
struct ScheduleCellView: View {
    /**
     Der Kurs, welcher in der Ansicht angezeigt wird.
     */
    let lesson: ScheduleLesson
    /**
     Ob die Detail-Ansicht durch einen Klick aufgerufen werden kann.
     */
    let touchGesturesEnabled: Bool
    
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Ob die Detailansicht vorher gezeigt wurde.
     */
    @Binding var detailsShownBefore: Bool
    /**
     Die IDs des Lehrers, dessen Stundenplan gerade angezeigt wird.
     */
    @Binding var currentTeacherID: Int
    
    /**
     Ob die Detailansicht gerade im Moment gezeigt wird.
     */
    @State private var detailsShown = false
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        let isCancelled = lesson.isCancelled
        let biggerScreen = sceneModel.biggerScreen
        return Group {
            NavigationLink(destination: ScheduleDetailView(teacherID: self.$currentTeacherID, id: self.lesson.id) {
                self.detailsShown = false
            }, isActive: self.$detailsShown) {
                EmptyView()
            }
            .frame(height: 0)
            VStack {
                Text(self.lesson.classOrTeacher)
                    .strikethrough(isCancelled, color: nil)
                Text(self.lesson.course)
                .strikethrough(isCancelled, color: nil)
                    .frame(width: sceneModel.windowSize.width)
                Text(self.lesson.roomOrTeacher ?? "")
                .strikethrough(isCancelled, color: nil)
            }
            .frame(width: sceneModel.windowSize.width * 0.19, height: (biggerScreen ? 100 : 60) * CGFloat(self.lesson.end - self.lesson.start + 1))
            .background(Color.dynamicWhite)
            .border(Color.primary)
            .font(.system(size: biggerScreen ? 20 : 13))
            .foregroundColor(self.lesson.represented ? .green : (isCancelled ? .red : .primary))
            .onTapGesture {
                if (self.touchGesturesEnabled) {
                    self.detailsShown = true
                    self.detailsShownBefore = true
                }
            }
        }
    }
}
