//
//  ScheduleHeaderView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die Stundenplan-Kopfzeile beinhaltet.
 */
struct ScheduleHeaderView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
    */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Wird aufgerufen, wenn die Stundenplan-Daten neu geladen werden sollen.
     */
    let loadSchedule: () -> ()
    
    /**
     Der Name des Lehrers, Schülers oder Raumes, dessen Plan gerade angezeigt wird.
     */
    @Binding var name: String
    /**
     Die Woche des aktuell angezeigten Stundenplans (Start- bis Enddatum)
     */
    @Binding var week: String
    /**
     Die ID des Lehrers, dessen Stundenplan gerade angezeigt wird (nur für Lehrer).
     */
    @Binding var currentTeacherID: Int
    /**
     Ob gerade der Stundenplan eines anderen Lehrers angezeigt wird (nur für Lehrer).
     */
    @Binding var differentTeacher: Bool
    
    /**
     Die Lehrerliste, aus der ein Lehrer einen anderen Lehrer wählen kann.
     */
    @State private var teacherList = TeacherSelectionList(currentTeacherID: 0, teachers: [])
    /**
     Ob gerade die Lehrer-Auswahlliste angezeigt wird.
     */
    @State private var showTeacherSelector = false
    
    /**
     Den Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        HStack {
            // aktuelle Woche
            HStack {
                VStack(alignment: .leading) {
                    Text("from")
                    Text("to")
                }
                VStack(alignment: .leading) {
                    Group {
                        if (!week.isEmpty) {
                            Text(week.split(separator: "-").first ?? "")
                            Text(week.split(separator: "-").last ?? "")
                        }
                    }
                }
            }
            .foregroundColor(.blue)
            .padding(.leading, 5)
            Spacer()
            // Name des Lehrers, der Klasse oder des Raumes
            Group {
                if (UserDefaults.isTeacher && name.contains("(") && name.contains(")")) {
                    Text(teacherList.teachers.filter { $0.id == currentTeacherID }.first?.name ?? name)
                    .foregroundColor(.red)
                    .frame(height: sceneModel.biggerScreen ? 80 : 50)
                    .onTapGesture {
                        self.showTeacherSelector = true
                    }
                    .onAppear {
                        if (self.teacherList.teachers == []) {
                            // Lehrerliste vom Server laden
                            JSONHandler.load(108, as: TeacherSelectionList.self, sceneModel: self.sceneModel) { teacherList in
                                self.teacherList = teacherList
                                
                                if (self.currentTeacherID == 0) {
                                    self.currentTeacherID = teacherList.currentTeacherID
                                }
                            }
                        }
                        else if (self.currentTeacherID == 0) {
                            self.currentTeacherID = self.teacherList.currentTeacherID
                            self.loadSchedule()
                        }
                    }
                }
                else {
                    Text(name)
                }
            }
            .padding(.trailing, 5)
        }
        .font(.system(size: sceneModel.biggerScreen ? 45 : 22))
        // Lehrerauswahlliste
        .popover(isPresented: $showTeacherSelector) {
            return TeacherPickerView(loadSchedule: self.loadSchedule, currentTeacherID: self.$currentTeacherID, teacherList: self.$teacherList, showTeacherSelector: self.$showTeacherSelector, differentTeacher: self.$differentTeacher).environmentObject(self.sceneModel)
        }
    }
}
