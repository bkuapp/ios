//
//  TeacherPickerView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Lehrerauswahl-Ansicht, welche einen Lehrer den Stundenplan eines anderen Lehrers ansehen lässt.
 */
struct TeacherPickerView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
    */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Wird aufgerufen, wenn die Stundenplan-Daten neu geladen werden sollen.
     */
    let loadSchedule: () -> ()
    
    /**
     Die ID des Lehrers, dessen Stundenplan gerade angezeigt wird.
     */
    @Binding var currentTeacherID: Int
    /**
     Die Daten, welche alle Lehrer enthalten.
     */
    @Binding var teacherList: TeacherSelectionList
    /**
     Ob die Lehrerauswahlliste gerade angezeigt werden soll.
     */
    @Binding var showTeacherSelector: Bool
    /**
     Ob gerade ein anderer Lehrer ausgewählt, als der der die App gerade benutzt.
     */
    @Binding var differentTeacher: Bool
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        VStack {
            Picker("", selection: Binding<Int>(get: {
                if (self.currentTeacherID == 0) {
                    return self.teacherList.currentTeacherID
                }
                else {
                    return self.currentTeacherID
                }
            }, set: { value in
                self.currentTeacherID = value
                self.differentTeacher = true
                self.loadSchedule()
            })) {
                ForEach(self.teacherList.teachers, id: \.self) { teacher in
                    Text(teacher.name).tag(teacher.id)
                        .foregroundColor(teacher.id == self.currentTeacherID ? .red : .primary)
                }
            }
            Button(action: {
                self.showTeacherSelector = false
            }) {
                Text("select")
            }
        }
        .font(.system(size: self.sceneModel.biggerScreen ? 45 : 22))
    }
}
