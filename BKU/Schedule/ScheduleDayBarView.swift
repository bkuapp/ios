//
//  ScheduleDayBarView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die Tagesleiste (Name des Tages und Datum) beinhaltet.
 */
struct ScheduleDayBarView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die Tage der aktuell angezeigten Stundenplanwoche
     */
    @Binding var days: [ScheduleDay]
    
    /**
     Die Wochentexte (Mo bis Fr), welche in der Leiste angezeigt werden.
     */
    private let weekTexts = ["mo".localized, "tu".localized, "we".localized, "th".localized, "fr".localized]

    /**
     Der Körper der Ansicht, welcher dessen Struktur enthält.
     */
    var body: some View {
        HStack(spacing: 0) {
            Text("")
                .frame(width: sceneModel.windowSize.width * 0.05, height: self.sceneModel.biggerScreen ? 60 : 40)
                .border(Color.primary)
            ForEach<Range<Int>, Int, AnyView>(self.days.indices, id: \.self) { i in
                let date = self.days[i].date
                return AnyView(Text(self.weekTexts[i] + ".\n" + date.replacingOccurrences(of: "c", with: ""))
                    .font(.system(size: self.sceneModel.biggerScreen ? 23 : 15))
                    .foregroundColor(date.contains("c") ? .blue : .primary)
                    .multilineTextAlignment(.center)
                    .frame(width: self.sceneModel.windowSize.width * 0.19, height: self.sceneModel.biggerScreen ? 60 : 40)
                    .border(Color.primary))
            }
        }
        .frame(height: sceneModel.biggerScreen ? 60 : 40)
    }
}
