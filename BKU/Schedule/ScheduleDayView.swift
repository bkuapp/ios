//
//  ScheduleDayView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche einen Tag im Stundenplan beinhaltet.
 */
struct ScheduleDayView: View {
    /**
     Der Tag, welcher angezeigt werden soll.
     */
    let day: ScheduleDay
    /**
     Ob die Detail-Ansicht durch einen Klick aufgerufen werden kann.
     */
    let touchGesturesEnabled: Bool
    
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die niedrigste Stunde, welche im aktuellen Stundenplan angezeigt wird.
     */
    @Binding var minHours: Int
    /**
     Die höchste Stunde, welche im aktuellen Stundenplan angezeigt wird.
     */
    @Binding var maxHours: Int
    /**
     Ob die Detailansicht vorher gezeigt wurde.
     */
    @Binding var detailsShownBefore: Bool
    /**
     Die IDs des Lehrers, dessen Stundenplan gerade gezeigt wird (nur für Lehrer).
     */
    @Binding var currentTeacherID: Int
    
    /**
     Die Stunde, welche vorher geladen wurde, um damit die Differnz zwischen dem Ende dieser und dem Anfang der nächsten zu berechnen.
     */
    @State private var previousLesson = ScheduleLesson(id: 0, classOrTeacher: "", start: 0, end: 0, course: "", roomOrTeacher: "", represented: false, isCancelled: false)
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        let courses = self.day.courses
        let end = courses.last?.end ?? 17
        return VStack(spacing: 0) {
            ForEach(courses, id: \.self) { lesson in
                Group {
                    ScheduleSpacesView(start: self.getPreviousLessonEnd(currentLesson: lesson), end: lesson.start)
                    ScheduleCellView(lesson: lesson, touchGesturesEnabled: self.touchGesturesEnabled, detailsShownBefore: self.$detailsShownBefore, currentTeacherID: self.$currentTeacherID)
                    .onAppear {
                        self.previousLesson = lesson
                    }
                }
            }
            if (end < self.maxHours) {
                ScheduleSpacesView(start: end, end: self.maxHours)
            }
            else if (courses.count == 0) {
                ScheduleSpacesView(start: self.minHours - 1, end: self.maxHours)
            }
        }
    }
    
    /**
     Gibt das Ende der Stunde, welche vor der angegeben Stunde ist, zurück.
     
     - Parameter currentLesson: Die Stunde,  bei der das Ende der vorherigen zurückgegeben werden soll.
     
     - Returns: Das Ende der Stunde, welche vor der angegeben Stunde ist.
     */
    func getPreviousLessonEnd(currentLesson: ScheduleLesson) -> Int {
        let currentIndex = day.courses.firstIndex(of: currentLesson) ?? 0
        if (currentIndex == 0) {
            return minHours
        }
        else {
            return day.courses[currentIndex - 1].end + 1
        }
    }
}
