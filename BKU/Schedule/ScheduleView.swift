//
//  ScheduleView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche den Stundenplan sowie die aktuelle Woche und den Namen der Klasse / des Lehrers an.
 */
struct ScheduleView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die Methode, welche aufgerufen wird, wenn auf den Neuladen-Knpf gedrückt wird.
     */
    let reload: () -> ()
    /**
     Die Methode, welche aufgerufen wird, wenn die Stundenplan-Daten neu geladen werden müssen (Wischen nach rechts oder links).
     */
    let loadScheduleData: (_: (() -> ())?) -> ()
    
    /**
     Ob die Einstellungen gerade gezeigt werden.
     */
    @Binding var showSettings: Bool
    /**
     Die aktuell angezeigte Stundenplan-Woche (0 = aktuelle Woche, 1 = eine Woche in der Zukunft, -1 = eine Woche in der Vergangenheit).
     */
    @Binding var currentWeek: Int
    /**
     Die Daten der Stundenplan-Woche, welche aktuell angezeigt wird.
     */
    @Binding var week: ScheduleWeek
    /**
     Die IDs des Lehrers, dessen Stundenplan gerade angezeigt wird (nur für Lehrer).
     */
    @Binding var currentTeacherID: Int
    /**
     Ob gerade der Stundenplan eines anderen Lehrers angezeigt wird (nur für Lehrer).
     */
    @Binding var differentTeacher: Bool
    
    /**
     Die niedrigste Stunde des aktuell angezeigten Stundenplans.
     */
    @Binding var minHours: Int
    /**
    Die höchste Stunde des aktuell angezeigten Stundenplans.
    */
    @Binding var maxHours: Int
    /**
     Ob die Stundenplan Daten vollständig geladen wurden.
     */
    @Binding var scheduleLoading: Bool
    
    /**
     Ob die Stundenplan-Detailansicht schon einmal gezeigt wurde.
     */
    @State private var detailsShownBefore = false
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur enthält.
     */
    var body: some View {
        Group {
            if (sceneModel.scheduleID != 0) {
                ScheduleDetailView(teacherID: $currentTeacherID, id: sceneModel.scheduleID) {
                    self.sceneModel.scheduleID = 0
                }
            }
            else {
                LoadingView {
                    VStack {
                        ScheduleHeaderView(loadSchedule: self.refreshScheduleData, name: self.$week.name, week: self.$week.week, currentTeacherID: self.$currentTeacherID, differentTeacher: self.$differentTeacher)
                        Spacer().frame(height: 10)
                        ScheduleTableView(loadScheduleData: self.refreshScheduleData, touchGesturesEnabled: true, currentWeek: self.$currentWeek, currentHour: self.$week.currentHour, days: self.$week.days, detailsShownBefore: self.$detailsShownBefore, minHours: self.$minHours, maxHours: self.$maxHours, currentTeacherID: self.$currentTeacherID)
                    }
                }
                .navigationBarTitle("schedule")
                .navigationBarItems(leading: EmptyView(), trailing: StandardMenuIconsView(reload: reload, showSettings: $showSettings))
            }
        }
        .onAppear {
            if (self.currentWeek != 0 && !self.detailsShownBefore) {
                self.currentWeek = 0
                self.loadScheduleData(nil)
            }
            self.detailsShownBefore = false
            
            self.sceneModel.showLoadingCircle = self.scheduleLoading
        }
    }
    
    /**
     Lädt die Stundenplandaten neu.
     */
    private func refreshScheduleData() {
        loadScheduleData(nil)
    }
}
