//
//  ScheduleDay.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Speichert die Daten für einen Schultag, der im Stundenplan oder Raumplan angezeigt.
*/
struct ScheduleDay: Hashable, Codable {
    /**
     Das Datum des Schultages.
     */
    var date: String
    /**
     Die Kurse, welche an dem Schultag stattfinden.
     */
    var courses: [ScheduleLesson]
}
