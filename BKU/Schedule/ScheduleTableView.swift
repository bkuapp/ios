//
//  ScheduleTableView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche den Stundenplan beinhaltet.
 */
struct ScheduleTableView: View {
    /**
     Die Methode, welche aufgerufen wird, wenn die Stundenplan-Daten neu geladen werden sollen.
     */
    let loadScheduleData: () -> ()
    /**
     Ob Wischen oder Klicks auf einen Kurs um die Detail-Ansicht zu öffnen aktiviert sind.
     */
    let touchGesturesEnabled: Bool
    
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die aktuelle Stundenplan-Woche (0 = aktuelle Woche, 1 = eine Woche in der Zukunft, -1 = eine Woche in der Vergangenheit).
     */
    @Binding var currentWeek: Int
    /**
     Die aktuelle Schulstunde.
     */
    @Binding var currentHour: Int
    /**
     Die Tage, welche in dem Stundenplan angezeigt werden.
     */
    @Binding var days: [ScheduleDay]
    /**
     Ob die Details vorher gezeigt wurden.
     */
    @Binding var detailsShownBefore: Bool
    /**
     Die niedrigste Stunde des Stundenplans.
     */
    @Binding var minHours: Int
    /**
     Die höchste Stunde des Stundenplans.
     */
    @Binding var maxHours: Int
    /**
     Die IDs des Lehrers, dessen Stundenplan gerade gezeigt wird (nur für Lehrer).
     */
    @Binding var currentTeacherID: Int
    
    /**
     Der Körper der Ansicht, welche dessen Strukutr beinhaltet.
     */
    var body: some View {
        let width = sceneModel.windowSize.width
        let biggerScreen = sceneModel.biggerScreen
        return Group {
            if (self.days.count != 0) {
                VStack(spacing: 0) {
                    ScheduleDayBarView(days: self.$days)
                    ScrollView {
                        HStack(spacing: 0) {
                            ScheduleHourBarView(minHours: self.$minHours, maxHours: self.$maxHours, currentHour: self.$currentHour)
                            .frame(width: width * 0.05)
                            ScheduleDayView(day: days[0], touchGesturesEnabled: self.touchGesturesEnabled, minHours: self.$minHours, maxHours: self.$maxHours, detailsShownBefore: self.$detailsShownBefore, currentTeacherID: self.$currentTeacherID)
                                .frame(width: width * 0.19, height: CGFloat((self.maxHours - self.minHours + 1) * (biggerScreen ? 100 : 60)))
                            ScheduleDayView(day: days[1], touchGesturesEnabled: self.touchGesturesEnabled, minHours: self.$minHours, maxHours: self.$maxHours, detailsShownBefore: self.$detailsShownBefore, currentTeacherID: self.$currentTeacherID)
                                .frame(width: width * 0.19, height: CGFloat((self.maxHours - self.minHours + 1) * (biggerScreen ? 100 : 60)))
                            ScheduleDayView(day: days[2], touchGesturesEnabled: self.touchGesturesEnabled, minHours: self.$minHours, maxHours: self.$maxHours, detailsShownBefore: self.$detailsShownBefore, currentTeacherID: self.$currentTeacherID)
                                .frame(width: width * 0.19, height: CGFloat((self.maxHours - self.minHours + 1) * (biggerScreen ? 100 : 60)))
                            ScheduleDayView(day: days[3], touchGesturesEnabled: self.touchGesturesEnabled, minHours: self.$minHours, maxHours: self.$maxHours, detailsShownBefore: self.$detailsShownBefore, currentTeacherID: self.$currentTeacherID)
                                .frame(width: width * 0.19, height: CGFloat((self.maxHours - self.minHours + 1) * (biggerScreen ? 100 : 60)))
                            ScheduleDayView(day: days[4], touchGesturesEnabled: self.touchGesturesEnabled, minHours: self.$minHours, maxHours: self.$maxHours, detailsShownBefore: self.$detailsShownBefore, currentTeacherID: self.$currentTeacherID)
                                .frame(width: width * 0.19, height: CGFloat((self.maxHours - self.minHours + 1) * (biggerScreen ? 100 : 60)))
                        }
                        .frame(width: width, height: CGFloat((self.maxHours - self.minHours + 1) * (biggerScreen ? 100 : 60)))
                    }
                }
                .frame(minWidth: width)
                .gesture(DragGesture().onEnded { gesture in
                    if (self.touchGesturesEnabled) {
                        let heightDistance = abs(gesture.location.y - gesture.startLocation.y)
                        let widthDistance = gesture.location.x - gesture.startLocation.x
                        
                        if (abs(widthDistance) > abs(heightDistance)) {
                            if (widthDistance < 0) {
                                self.currentWeek += 1
                            }
                            else {
                                self.currentWeek -= 1
                            }
                            self.loadScheduleData()
                        }
                    }
                })
            }
            else {
                Spacer()
            }
        }
    }
}
