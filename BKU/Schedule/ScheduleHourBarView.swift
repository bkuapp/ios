//
//  ScheduleHourBarView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die Stundenleiste beinhaltet.
 */
struct ScheduleHourBarView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die niedrigste Stunde des aktuell angezeigten Stundenplans.
     */
    @Binding var minHours: Int
    /**
     Die höchste Stunde des aktuell angezeigten Stundenplans.
     */
    @Binding var maxHours: Int
    /**
     Die aktuelle Schulstunde.
     */
    @Binding var currentHour: Int
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        let biggerScreen = sceneModel.biggerScreen
        return Group {
            if (self.minHours <= self.maxHours) {
                VStack {
                    ForEach(self.minHours...self.maxHours, id: \.self) { hour in
                        Text(String(hour))
                            .foregroundColor(self.currentHour == hour ? .blue : .primary)
                            .frame(width: self.sceneModel.windowSize.width * 0.05, height: (biggerScreen ? 100 : 60))
                            .border(Color.primary)
                    }
                }
                .font(.system(size: biggerScreen ? 20 : 13))
            }
        }
    }
}
