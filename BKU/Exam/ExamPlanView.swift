//
//  ExamPlanView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die Klausurliste anzeigt.
 */
struct ExamPlanView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die Methode, welche aufgerufen wird, wenn die Daten neu geladen werden sollen.
     */
    let reload: () -> ()
    
    /**
     Ob die Einstellungen gerade gezeigt werden.
     */
    @Binding var showSettings: Bool
    /**
     Die Klausurquartale, welche in der List gezeigt werden.
     */
    @Binding var examSections: [ExamQuarter]
    /**
     Ob die Klausurdaten gerade geladen werden.
     */
    @Binding var examLoading: Bool
    
    /**
     Der Körper der Ansicht, welcher dessen Strukur beinhaltet.
     */
    var body: some View {
        Group {
            if (self.sceneModel.examID != 0) {
                ExamDetailView(id: self.sceneModel.examID)
                    .navigationBarItems(leading: BackIconNavigationIcon(text: "exam plan".localized) {
                        self.sceneModel.examID = 0
                    }, trailing: EmptyView())
            }
            else if (sceneModel.tabIndex == 2) {
                LoadingView {
                    List {
                        ForEach(self.examSections, id: \.self) { examSection in
                            Section(header: Text("\("appointment".localized) \(examSection.quarterNumber)")) {
                                ForEach(examSection.data, id: \.self) { exam in
                                    NavigationLink(destination: ExamDetailView(id: exam.id)) {
                                        Text("\(exam.course), \(exam.date[5..<exam.date.count])")
                                    }
                                    .padding(.vertical)
                                }
                            }
                        }
                    }
                }
                .onAppear {
                    self.sceneModel.showLoadingCircle = self.examLoading
                }
                .navigationBarTitle("exam plan")
                .navigationBarItems(leading: EmptyView(), trailing: StandardMenuIconsView(reload: reload, showSettings: $showSettings))
            }
        }
    }
}
