//
//  ExamDetailView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die Klausurdetails anzeigt.
 */
struct ExamDetailView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die ID der Klausur, dessen Details angezeigt werden sollen.
     */
    var id: Int
    
    /**
     Die Klausur, dessen Details gerade angezeigt werden.
     */
    @State private var exam: DetailExam? = nil
    
    /**
     Der Körper der Ansicht, dessen Details gerade angezeigt werden.
     */
    var body: some View {
        VStack(spacing: 0) {
            HStack(spacing: 0) {
                Text("date")
                    .frame(width: sceneModel.windowSize.width * 0.3, height: self.sceneModel.biggerScreen ? 50 : 30)
                    .border(Color.primary, width: 2)
                    .padding(.trailing, -2)
                Text(self.exam?.date ?? "")
                    .frame(width: sceneModel.windowSize.width * 0.7, height: self.sceneModel.biggerScreen ? 50 : 30)
                    .border(Color.primary, width: 2)
            }
            HStack(spacing: 0) {
                Text("time")
                    .frame(width: sceneModel.windowSize.width * 0.3, height: self.sceneModel.biggerScreen ? 50 : 30)
                    .border(Color.primary, width: 2)
                    .padding(.trailing, -2)
                Text(self.exam?.time ?? "")
                    .frame(width: sceneModel.windowSize.width * 0.7, height: self.sceneModel.biggerScreen ? 50 : 30)
                    .border(Color.primary, width: 2)
            }
            .padding(.top, -2)
            HStack(spacing: 0) {
                Text("room")
                    .frame(width: sceneModel.windowSize.width * 0.3, height: self.sceneModel.biggerScreen ? 50 : 30)
                    .border(Color.primary, width: 2)
                    .padding(.trailing, -2)
                Text(self.exam?.room ?? "")
                    .frame(width: sceneModel.windowSize.width * 0.7, height: self.sceneModel.biggerScreen ? 50 : 30)
                    .border(Color.primary, width: 2)
            }
            .padding(.top, -2)
            Spacer()
        }
        .font(.system(size: self.sceneModel.biggerScreen ? 35 : 15))
        .navigationBarTitle(exam?.course ?? "")
        .onAppear {
            if (self.exam == nil) {
                // Laden der Klausurdaten
                let params = [
                    "id": self.id
                ]
                self.sceneModel.showLoadingCircle = true
                JSONHandler.load(104, as: DetailExam.self, queryParams: params, sceneModel: self.sceneModel) { exam in
                    self.exam = exam
                    self.sceneModel.showLoadingCircle = false
                }
            }
        }
    }
}
