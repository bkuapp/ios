//
//  AuthenticationView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import LocalAuthentication

/**
 Die Authenifizierungs-Ansicht, welche bei FaceID oder TouchID Überprüfung auftaucht und das ganze Fenster überdeckt.
 */
struct AuthenticationView: View {
    /**
     Der Authenifizierungstyp des Gerätes (FaceID oder TouchID).
     */
    static let biometryType = LAContext().biometricType
    
    /**
     Zeigt den FaceID oder TouchID Dialog dem User an und überprüft seine biometrischen Daten.
     
     - Parameters:
        - onSuccess: Die Methode, welche aufgerufen wird, wenn die Überprüfung erfolgreich war.
        - onFail: Die Methode, welche aufgerufen wird, wenn die Überprüfung fehlgschlagen ist.
     */
    static func useAuthentication(onSuccess: @escaping () -> (), onFail: @escaping () -> ()) {
        let context = LAContext()
        
        if (context.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthenticationWithBiometrics,
            error: nil)) {
            
            // Device can use biometric authentication
            context.evaluatePolicy(
                LAPolicy.deviceOwnerAuthenticationWithBiometrics,
                localizedReason: "access requires authentication".localized,
                reply: {(success, _) in
                    DispatchQueue.main.async {
                        if (success) {
                            onSuccess()
                        }
                        else {
                            onFail()
                        }
                    }
            })
        }
        else {
            onFail()
        }
    }
    
    /**
     Der Kontroller der Ansicht.
     */
    let controller: UIViewController
    
    /**
     Ob die Authenifzierung fehlgeschlagen ist.
     */
    @State private var authenticationFailed = false
    
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
    */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Der Körper der Ansicht, welcher einen verschwommenen Hintergrund und einen Knopf zum erneuten Überprüfen anzeigt.
     */
    var body: some View {
        let size = sceneModel.window?.frame.size ?? CGSize(width: 0, height: 0)
        return ZStack {
            BlurView(style: .regular)
            .frame(width: size.width, height: size.height)
            Button(action: self.authenticate) {
                Text(self.authenticationFailed ? (AuthenticationView.biometryType == .touchID ? "use TouchID" : "use FaceID") : "")
            }
        }
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            self.authenticate()
        }
    }
    
    /**
     Führt eine Authentifizierung mit den biometrischen Daten des Nutzers durch.
     */
    func authenticate() {
        AuthenticationView.useAuthentication(onSuccess: {
            self.controller.dismiss(animated: true, completion: nil)
        }, onFail: {
            self.authenticationFailed = true
        })
    }
}
