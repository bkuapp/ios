//
//  BlurView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Verschommenheits-Ansicht, welche im Hintergrund von der Authenifizierungs-Ansicht angezeigt wird.
 */
struct BlurView: UIViewRepresentable {
    /**
     Der Stil der Ansicht (dunkel oder hell).
     */
    let style: UIBlurEffect.Style

    /**
     Erstellt die Verschwommenheits-Ansicht und gibt sie zurück.
     
     - Parameter context: Der Kontext bei dem die Ansicht erstellt werden soll.
     
     - Returns: Die erstellte Ansicht.
     */
    func makeUIView(context: UIViewRepresentableContext<BlurView>) -> UIView {
        let view = UIView(frame: .zero)
        view.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: style)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(blurView, at: 0)
        NSLayoutConstraint.activate([
            blurView.heightAnchor.constraint(equalTo: view.heightAnchor),
            blurView.widthAnchor.constraint(equalTo: view.widthAnchor),
        ])
        return view
    }

    /**
     Überarbeitet die Ansicht.
     
     - Parameters:
        - uiView: Die Ansicht, welche überarbeitet werden soll.
        - context: Der Kontext bei dem die Ansicht überarbeitet werden soll.
     */
    func updateUIView(_ uiView: UIView,
                      context: UIViewRepresentableContext<BlurView>) {}
}
