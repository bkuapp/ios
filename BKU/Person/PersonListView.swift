//
//  PersonListView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Struktur, welche die Personenliste (Lehrer oder Klassen) beinhaltet.
 */
struct PersonListView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die Personen, welche in der Liste angezeigt werden.
     */
    @Binding var persons: [[Person]]
    /**
     Ob die Personendaten gerade geladen werden.
     */
    @Binding var teacherLoading: Bool
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        LoadingView {
            Group {
                if (UserDefaults.isTeacher) {
                    List {
                        Section(header: Text("classes")) {
                            ForEach(self.persons.first ?? [], id: \.self) { teacherOrClass in
                                NavigationLink(destination: PersonDetailsView(id: teacherOrClass.id, type: "class")) {
                                    Text(teacherOrClass.name)
                                }
                            }
                        }
                        Section(header: Text("teachers")) {
                            ForEach(self.persons.last ?? [], id: \.self) { teacherOrClass in
                                NavigationLink(destination: PersonDetailsView(id: teacherOrClass.id, type: "teacher")) {
                                    Text(teacherOrClass.name)
                                }
                            }
                        }
                    }
                }
                else {
                    List(self.persons.first ?? [], id: \.self) { teacherOrClass in
                        NavigationLink(destination: PersonDetailsView(id: teacherOrClass.id, type: "")) {
                            Text(teacherOrClass.name)
                            .padding(.vertical)
                        }
                    }
                }
            }
        }
        .onAppear {
            self.sceneModel.showLoadingCircle = self.teacherLoading
        }
    }
}
