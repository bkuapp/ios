//
//  PersonDetailsView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die Personendetails (Lehrer oder Schüler) anzeigt.
 */
struct PersonDetailsView: EmailView {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die Person, dessen Details gerade gezeigt werden.
     */
    @State private var person: DetailPerson? = nil
    /**
     Die Daten des Klassenstundenplans der Klasse, dessen Details gerade angezeigt werden (nur für Lehrer).
     */
    @State private var classScheduleData = ScheduleWeek.empty
    /**
     Die niedrigste Stunde, welche gerade im Klassenstundenplan angezeigt wird (nur für Lehrer).
     */
    @State private var minHours = 1
    /**
     Die höchste Stunde, welche gerade im Klassenstundenplan angezeigt wird (nur für Lehrer).
     */
    @State private var maxHours = 17
    /**
     Ob die Detaildaten schon geladen wurden.
     */
    @State private var personDetailsLoading = true
    /**
     Ob die Klassenstundenplandaten schon geladen wurden (nur für Lehrer).
     */
    @State private var studentScheduleLoading = UserDefaults.isTeacher
    
    /**
     Die ID der Person, dessen Details angezeigt werden sollen.
     */
    let id: Int
    /**
     Der Typ der Person (Klasse oder Schüler) die angezeigt werden soll (nur für Lehrer),
     */
    let type: String
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        LoadingView {
            VStack(spacing: 0) {
                HStack(spacing: 0) {
                    Text("email")
                        .frame(width: self.sceneModel.windowSize.width * 0.3, height: self.sceneModel.biggerScreen ? 50 : 30)
                        .border(Color.primary, width: 2)
                        .padding(.trailing, -2)
                    Text(self.person?.email ?? "")
                        .onTapGesture {
                            self.presentMailCompose(mails: [self.person?.email ?? ""], window: self.sceneModel.window)
                        }
                        .foregroundColor(.blue)
                    .frame(width: self.sceneModel.windowSize.width * 0.7, height: self.sceneModel.biggerScreen ? 50 : 30)
                        .border(Color.primary, width: 2)
                }
                // Kurse mit dem Lehrer oder der Klasse
                if (!UserDefaults.isTeacher || self.type == "class") {
                    HStack(spacing: 0) {
                        Text("courses")
                            .frame(width: self.sceneModel.windowSize.width * 0.3, height: CGFloat(self.person?.courses.count ?? 1) * (self.sceneModel.biggerScreen ? 50 : 30) - CGFloat((self.person?.courses.count ?? 1) - 1) * (self.sceneModel.biggerScreen ? 25 : 15))
                            .border(Color.primary, width: 2)
                            .padding(.trailing, -2)
                   
                        VStack {
                            ForEach(self.person?.courses ?? [], id: \.self) { course in
                                Text(course)
                                    .frame(width: self.sceneModel.windowSize.width * 0.7)
                            }
                        }
                        .frame(width: self.sceneModel.windowSize.width * 0.7, height: CGFloat(self.person?.courses.count ?? 1) * (self.sceneModel.biggerScreen ? 50 : 30) - CGFloat((self.person?.courses.count ?? 1) - 1) * (self.sceneModel.biggerScreen ? 25 : 15))
                            .border(Color.primary, width: 2)
                    }
                    .padding(.top, -2)
                }
                
                // Klassenstundenplan
                if (UserDefaults.isTeacher && self.type == "class") {
                    Spacer()
                    .frame(height: 20)
                    
                    LoadingView {
                        VStack {
                            Text("schedule for the class")
                                .font(.system(size: self.sceneModel.biggerScreen ? 55 : 25))
                            ScheduleHeaderView(loadSchedule: {}, name: self.$classScheduleData.name, week: self.$classScheduleData.week, currentTeacherID: .constant(0), differentTeacher: .constant(false))
                            ScheduleTableView(loadScheduleData: {}, touchGesturesEnabled: false, currentWeek: .constant(0), currentHour: self.$classScheduleData.currentHour, days: self.$classScheduleData.days, detailsShownBefore: .constant(false), minHours: self.$minHours, maxHours: self.$maxHours, currentTeacherID: .constant(0))
                        }
                    }
                }
                else {
                    Spacer()
                }
            }
            .font(.system(size: self.sceneModel.biggerScreen ? 35 : 15))
        }
        .navigationBarTitle(person?.name ?? "")
        .onAppear {
            if (self.person == nil) {
                var params: [String: Any] = [
                    "id": self.id
                ]
                
                if (UserDefaults.isTeacher) {
                    params["personType"] = self.type
                }
                
                // Laden der Detaildaten
                self.sceneModel.showLoadingCircle = true
                JSONHandler.load(6, as: DetailPerson.self, queryParams: params, sceneModel: self.sceneModel) { person in
                    self.person = person
                    self.personDetailsLoading = false
                    self.sceneModel.showLoadingCircle = self.studentScheduleLoading && self.type == "class"
                }
                
                if (UserDefaults.isTeacher && self.type == "class") {
                    let params: [String: Any] = [
                        "classID": self.id,
                        "inWeeks": 0
                    ]
                    
                    // Laden der Klassenstundenplandaten
                    JSONHandler.load(107, as: ScheduleWeek.self, queryParams: params, sceneModel: self.sceneModel) { scheduleWeek in
                        self.classScheduleData = scheduleWeek
                        self.minHours = TimeHandler.getMinHours(forWeek: scheduleWeek)
                        self.maxHours = TimeHandler.getMaxHours(forWeek: scheduleWeek)
                        self.studentScheduleLoading = false
                        self.sceneModel.showLoadingCircle = self.personDetailsLoading
                    }
                }
            }
        }
    }
}
