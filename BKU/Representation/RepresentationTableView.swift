//
//  RepresentationTableView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die Vertretungsliste beinhaltet.
 */
struct RepresentationTableView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die Methode, welche aufgerufen wird, wenn die Daten neu geladen werden.
     */
    let loadData: () -> ()
    
    /**
     Die Vertretungen, welcher in der Liste angezeigt werden.
     */
    @Binding var representations: [[Representation]]
    /**
     Ob die Vertretungsplandaten gerade vom Server geladen werden.
     */
    @Binding var representationLoading: Bool
    
    /**
     Ob dem Nutzer die Einstellungen gerade gezeigt werden.
     */
    @Binding var showSettings: Bool
    
    /**
     Der Körper der Struktur, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        Group {
            if (sceneModel.representationID != 0) {
                RepresentationDetailView(id: sceneModel.representationID, leadingMenuView: BackIconNavigationIcon(text: "representation plan".localized) {
                    self.sceneModel.representationID = 0
                })
            }
            else if (sceneModel.tabIndex == 1) {
                LoadingView {
                    Group {
                        if (self.representations.count >= 1) {
                            List {
                                Section(header: Text("representation since".localized + Date().toString(dateFormat: " dd.MM."))) {
                                    if (UserDefaults.isTeacher && self.representations.count == 2) {
                                        Section(header: Text("I represent")) {
                                            ForEach(self.representations.first ?? [], id: \.self) { representation in
                                                NavigationLink(destination: RepresentationDetailView<EmptyView>(id: representation.id, leadingMenuView: nil)){
                                                    RepresenationRowView(representation: representation)
                                                }
                                            }
                                        }
                                        Section(header: Text("I will be represented or informed")) {
                                            ForEach(self.representations.last ?? [], id: \.self) { representation in
                                                NavigationLink(destination: RepresentationDetailView<EmptyView>(id: representation.id, leadingMenuView: nil)){
                                                    RepresenationRowView(representation: representation)
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        ForEach(self.representations.first ?? [], id: \.self) { representation in
                                            NavigationLink(destination: RepresentationDetailView<EmptyView>(id: representation.id, leadingMenuView: nil)){
                                                RepresenationRowView(representation: representation)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                .onAppear {
                    self.sceneModel.showLoadingCircle = self.representationLoading
                }
                .navigationBarItems(leading: EmptyView(), trailing: StandardMenuIconsView(reload: loadData, showSettings: $showSettings))
            }
        }
    }
}
