//
//  DetailRepresentation.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import Foundation

/**
 Speichert die Daten einer Vertretung, welche in der Detail Ansicht gezeigt wird.
 */
struct DetailRepresentation: Hashable, Codable {
    /**
     Das Datum der Vertretung.
     */
    var date: String
    /**
     Die Stunden der Vertretung.
     */
    var hours: [String]
    /**
     Der Name der Klasse, welche vertreten wird.
     */
    var nameOfClass: String
    /**
     Der Name des Kurses, welcher vertreten wird.
     */
    var course: String
    /**
     Der Raum, in dem der Unterricht jetzt stattfinden wird.
     */
    var room: String?
    /**
     Der Lehrer welcher den Kurs vertritt.
     */
    var representation: String
    /**
     Der Lehrer, welcher vertreten wird.
     */
    var teacher: String
    /**
     Die Regelungen der Vertretung.
     */
    var representationRules: String
}
