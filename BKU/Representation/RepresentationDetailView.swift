//
//  RepresentationDetailView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import UIKit

/**
 Die Ansicht, welche die Vertretungsdetails anzeigt.
 
 - Parameter T: Der Typ der Zurückknopf-Ansicht
 */
struct RepresentationDetailView<T>: View where T: View {
    /**
     Die ID der Vertretung, welche angezeigt werden soll.
     */
    let id: Int
    /**
     Die Ansicht des Zurückknopfes.
     */
    let leadingMenuView: T?
    
    /**
     Ob das Menü zum Teilen gerade angezeigt wird.
     */
    @State var showingShareSheet = false
    
    /**
     Die Vertretung, welche gerade angezeigt wird.
     */
    @State private var representation: DetailRepresentation? = nil
    /**
     Das Bild, welches geteilt werden kann.
     */
    @State private var imageToShare = UIImage()
    
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    /**
     Das aktuelle Model der Anwednung.
     */
    @EnvironmentObject private var applicationModel: ApplicationModel
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        LoadingView {
            VStack {
                Image(uiImage: self.imageToShare)
                .sheet(isPresented: self.$showingShareSheet, content: { ActivityView(activityItems: [self.imageToShare]) })
                Group {
                    // Neuladen bei Nachtmodus-Änderungen
                    if (self.applicationModel.darkMode) {
                        Text("")
                        .onAppear {
                            self.createImage()
                        }
                    }
                    else {
                        Text("")
                        .onAppear {
                            self.createImage()
                        }
                    }
                    // Neuladen bei Orientationsänderungen
                    if (self.sceneModel.landscape) {
                        Text("")
                        .onAppear {
                            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(10)) {
                                self.createImage()
                            }
                        }
                    }
                    else {
                        Text("")
                        .onAppear {
                            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(10)) {
                                self.createImage()
                            }
                        }
                    }
                }
                .frame(height: 0)
                Group {
                    if (self.leadingMenuView != nil) {
                        Text("")
                            .navigationBarItems(leading: self.leadingMenuView, trailing:
                            Image(systemName: "square.and.arrow.up")
                            .font(.system(size: 23))
                            .padding()
                            .onTapGesture {
                                self.showingShareSheet = true
                            }
                        )
                    }
                    else {
                        Text("")
                       .navigationBarItems(trailing:
                           Image(systemName: "square.and.arrow.up")
                           .font(.system(size: 23))
                           .padding()
                           .onTapGesture {
                               self.showingShareSheet = true
                           }
                       )
                    }
                }
                .frame(height: 0)
            }
            .onAppear {
                // Laden der Vertretungsplan Daten.
                let params: [String: Any] = [
                    "id": self.id
                ]
                self.sceneModel.showLoadingCircle = true
                JSONHandler.load(102, as: DetailRepresentation.self, queryParams: params, sceneModel: self.sceneModel) { representation in
                    self.representation = representation
                    self.createImage()
                    self.sceneModel.showLoadingCircle = false
                }
            }
        }
        .navigationBarTitle("representation plan")
    }
    
    /**
     Erstellt das Vertretungsbild mit der aktuellen Vertretung.
     */
    private func createImage() {
        if let representation = representation {
            self.imageToShare = UIImage.createImage(windowSize: sceneModel.windowSize, withRepresentation: representation)
        }
    }
}
