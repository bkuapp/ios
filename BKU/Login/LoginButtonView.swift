//
//  LoginButtonView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Der Login-Knopf, welchen der Nutzer drück, wenn er sich einloggen will.
 */
struct LoginButtonView: View {
    /**
     Die Methode, welche aufgerufen wird, wenn der Nutzer den Knopf drückt.
     */
    var login: () -> ()
    
    /**
     Der Körper des Login-Knopf, welcher dessen Strukutr beinhaltet.
     */
    var body: some View {
        Button(action: login) {
            Text("LOGIN")
                .font(.headline)
                .foregroundColor(.white)
                .padding()
                .frame(width: 220, height: 60)
                .background(Color("LoginButtonBackground"))
                .cornerRadius(15.0)
        }
    }
}
