//
//  TextFieldRepresentable.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Der Textfeld-Representant, welches das Textfeld von UIKit zu SwiftUI konvertiert.
 */
struct TextFieldRepresentable: UIViewRepresentable {
    /**
     Der Typ des Textfeldes (Passwort oder Nutzername).
     */
    let contentType: UITextContentType
    /**
     Der Typ des Enter-Knopfes (Weiter oder Öffnen).
     */
    let returnVal: UIReturnKeyType
    /**
     Der Platzhaltertext des Textfeldes.
     */
    let placeholder: String
    /**
     Die Methode, welche aufgerufen wird, wenn der Nutzer auf Enter drückt.
     */
    let onReturn: () -> ()
    
    /**
     Ob das Textfeld gerade im Fokus ist.
     */
    @Binding var isFirstResponder: Bool
    /**
     Der Text des Textfeldes.
     */
    @Binding var text: String
    /**
     Ob das Textfeld sicher ist (Passwort-Ansicht oder nicht).
     */
    @Binding var isSecure: Bool

    /**
     Erstellt das Textfeld, welches konfiguriert und zurückgegeben wird.
     
     - Parameter context: Der Kontext des Representanten.
     
     - Returns: Das erstellte Textfeld.
     */
    func makeUIView(context: Context) -> UITextField {
        let textField: UITextField = UITextField(frame: .zero)
        textField.returnKeyType = self.returnVal
        textField.isSecureTextEntry = isSecure
        textField.delegate = context.coordinator
        textField.autocorrectionType = .no
        textField.placeholder = placeholder.localized
        textField.text = text

        return textField
    }

    /**
     Überarbeitet das Textfeld (in den Fokus setzen oder Passwort-Sichtbarkeit ändern).
     
     - Parameters:
        - textField: Das Textfeld, welches überarbeitet wird.
        - context: Der Kontext des Textfeldes.
     
     */
    func updateUIView(_ textField: UITextField, context: Context) {
        if (isFirstResponder) {
            textField.becomeFirstResponder()
        }
        
        if (textField.isSecureTextEntry != isSecure) {
            textField.togglePasswordVisibility()
        }
        
        textField.isSecureTextEntry = isSecure
    }
    
    /**
     Erstellt das Delegat für das Textfeld und gibt es zurück.
     
     - Returns: Das erstellte Delegat für das Textfeld.
     */
    func makeCoordinator() -> TextFieldDelegate {
        TextFieldDelegate(self)
    }

    /**
     Das Delegat für das Textfeld, welches es überwacht.
     */
    class TextFieldDelegate: NSObject, UITextFieldDelegate {
        /**
         Das Textfeld, zu dem dieses Delegat gehört.
         */
        private let parent: TextFieldRepresentable

        /**
         Erstellt das Delegat mit dem dazu gehörigen Textfeld.
         
         - Parameter textField: Das Textfeld, zu dem das Delegat gehört.
         */
        init(_ textField: TextFieldRepresentable) {
            self.parent = textField
        }
        
        /**
         Wird aufgerufen, wenn der Nutzer das Textfeld verlassen hat.
         
         - Parameter textField: Das Textfeld, welches der Nutzer verlassen hat.
         */
        func textFieldDidChangeSelection(_ textField: UITextField) {
            withAnimation {
                parent.text = textField.text ?? ""
            }
        }
        
        /**
         Wird aufgerufen, wenn das Textfeld mit der Bearbeitung fertig ist.
         
         - Parameter textField: Das Textfeld, welches mit der Bearbeitung fertig ist.
         */
        func textFieldDidEndEditing(_ textField: UITextField) {
            parent.isFirstResponder = false
        }

        /**
         Wird aufgerufen, wenn der Nutzer in dem Textfeld auf den Enter-Knopf gedrückt hat.
         
         - Parameter textField: Das Textfeld, bei dem der Nutzer auf Enter gedrückt hat.
         
         - Returns: Ob das Textfeld fertig bearbeitet ist.
         */
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            parent.onReturn()
            return true
        }
    }
}
