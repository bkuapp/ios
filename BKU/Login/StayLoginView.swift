//
//  StayLoginView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Der Schalter, welcher anzeigt, ob der Nutzer eingeloggt bleiben will.
 */
struct StayLoginView: View {
    /**
     Ob der Nutzer eingeloggt bleiben will.
     */
    @Binding var stayLogin: Bool
    
    /**
     Der Körper des Schalters, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        Toggle(isOn: $stayLogin)
        {
            Text("stay login?")
        }
        .padding(.leading, 30)
        .padding(.bottom, 20)
        .font(.title)
    }
}
