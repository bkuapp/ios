//
//  PasswordTextFieldView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Das Passwort-Textfeld, welches in der Login-Ansicht angezeigt wird.
 */
struct PasswordTextFieldView: View {
    /**
     Das aktuelle eingegebene Passwort.
     */
    @Binding var text: String
    /**
     Ob das Passwort-Textfeld gerade im Fokus ist.
     */
    @Binding var isFirstResponder: Bool
    /**
     Ob das Passwort-Textfeld sicher ist, also ob das Passwort sichtbar ist oder nicht.
     */
    @State private var isSecure = true
    
    /**
     Die Methode, welche aufgerufen wird, wenn der Nutzer weiter klickt (Enter (Hardware-Keyboard) oder Knopf unten rechts (Software-Keyboard)).
     */
    let onReturn: () -> ()
    
    /**
     Der Körper des Password-Textfelds, welcher dessen Struktur enthält.
     */
    var body: some View {
        VStack(alignment: .leading) {
            TextFieldView(text: $text, isSecure: $isSecure, isFirstResponder: $isFirstResponder, contentType: .password, returnVal: .go, placeholder: "password", onReturn: onReturn)
            Group {
                if (text.count != 0) {
                    Text(isSecure ? "show password" : "hide password")
                }
            }
            .font(.footnote)
            .onTapGesture {
                self.isSecure.toggle()
            }
            .padding(.leading, 15)
        }
        .padding(.bottom, 20)
    }
}
