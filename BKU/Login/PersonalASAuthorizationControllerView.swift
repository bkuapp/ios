//
//  PersonalASAuthorizationControllerDelegate.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import AuthenticationServices

/**
 Die Ansicht, welches den Autofill-Kontroller anzeigt.
 */
struct PersonalASAuthorizationControllerView: UIViewRepresentable {
    /**
     Die Methode, welche aufgerufen wird, wenn der Nutzer sich über den Kontroller eingeloggt hat.
     */
    let loginWithCrenditials: (_ username: String, _ password: String) -> ()
    
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Erstellt die Ansicht mit dem gegeben Kontext.
     
     - Parameter context: Der Kontext der Ansicht
     
     - Returns: Die erstellt Ansicht
     */
    func makeUIView(context: Context) -> UIView {
        context.coordinator.createRequest()
        return UIView()
    }
    
    /**
     Überarbeitet die Ansicht.
     
     - Parameters:
        - view: Die Ansicht, welche überarbeitet wird.
        - context: Der Kontext der Ansicht
     */
    func updateUIView(_ view: UIView, context: Context) {}
    
    /**
     Erstellt das Delegat für das Textfeld und gibt es zurück.
     
     - Returns: Das erstellte Delegat für das Textfeld.
     */
    func makeCoordinator() -> PersonalASAuthorizationControllerDelegate {
        return PersonalASAuthorizationControllerDelegate(parent: self)
    }
    
    /**
     Das Delegat, welches den Verlauf des Logins über den Autofill-Controller regelt.
     */
    class PersonalASAuthorizationControllerDelegate: NSObject, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
        
        /**
         Die Ansicht, zu der dieses Delegat gehört.
         */
        let parent: PersonalASAuthorizationControllerView
        
        /**
         Initalisert das Delegat mit der Ansicht.
         
         - Parameter parent: Die Ansicht, zu der dieses Delegat gehört.
         */
        init(parent: PersonalASAuthorizationControllerView) {
            self.parent = parent
        }
        
        /**
         Wird aufgerufen, wenn der Nutzer sich über den Kontroller eingeloggt hat. Leitet den Nuzernamen und das Password an die `loginWithCrenditials` Methode weiter.
         
         - Parameters:
            - controller: Der Kontroller, von dem der Nutzer sich eingeloggt hat.
            - authorization: Die Login-Daten, mit denen der Nutzer sich eingeloggt hat.
         */
        func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
            switch (authorization.credential) {
            case let passwordCredential as ASPasswordCredential:
                parent.loginWithCrenditials(passwordCredential.user, passwordCredential.password)
            default:
                break
            }
        }
        
        /**
         Gibt die Ansicht zurück, in der der Kontroller angezeigt wird.
         
         - Parameter controller: Der Kontroller, welcher angezeigt werden soll.
         
         - Returns: Die Ansicht, in der der Kontroller angezeigt wird.
         */
        func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
            return parent.sceneModel.window ?? UIWindow()
        }
        
        /**
         Erstellt die Autofillanfragen.
         */
        func createRequest() {
            let request = ASAuthorizationPasswordProvider().createRequest()

            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.presentationContextProvider = self
            authorizationController.delegate = self
            authorizationController.performRequests()
        }
    }
}
