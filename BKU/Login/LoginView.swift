//
//  LoginView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import AuthenticationServices

/**
 Die Login- und Willkommens-Ansicht.
 */
struct LoginView: EmailView {
    /**
     Der Konnektivätshandler, welcher mit der Apple Watch kommunziert.
     */
    private let connectivityHandler = ConnectivityHandler.shared
    
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Der aktuelle Nutzername, welcher eingegeben wurde.
     */
    @State private var username = UserDefaults.shared?.string(forKey: "username") ?? ""
    /**
     Das aktuelle Passwort, welches eingegeben wurde.
     */
    @State private var password = ""
    /**
     Ob der Nutzer eingeloggt bleiben möchte.
     */
    @State private var stayLogin = true
    /**
     Die Fehlermeldung, welche bei Fehler während des Einloggens gezeigt wird.
     */
    @State private var errorMessage = ""
    /**
     Ob das Benutzernamen-Textfeld gerade im Fokus ist.
     */
    @State private var usernameTextFieldFirstResponder = UserDefaults.shared?.string(forKey: "username") == ""
    /**
     Ob das Password-Textfeld gerade im Fokus ist.
     */
    @State private var passwordTextFieldFirstResponder = UserDefaults.shared?.string(forKey: "username") != ""
    /**
     Ob die Nutzungsbedingungen und Datenschutzerklärungen schon gezeigt wurden.
     */
    @State private var termsAndPrivacyShown = UserDefaults.standard.bool(forKey: "terms of use and privacy policy shown")
    
    /**
     Der Körper der Login-Ansicht, welcher das Login-Formular und den Erstellungstext enthält.
     */
    var body: some View {
        Group {
            /*if (!self.termsAndPrivacyShown) {
                TermsAndPrivacyView(termsAndPrivacyShown: self.$termsAndPrivacyShown)
            }
            else {*/
                LoadingView {
                    ScrollView {
                        VStack {
                            BkuLogoView()
                            VStack {
                                TextFieldView(text: self.$username, isSecure: .constant(false), isFirstResponder: self.$usernameTextFieldFirstResponder, contentType: .username, returnVal: .next, placeholder: "username", onReturn: self.focusPasswordTextField)
                                    .frame(width: self.sceneModel.biggerScreen ? 400 : self.sceneModel.windowSize.width - 10)
                                    .padding(.bottom, 20)
                                PasswordTextFieldView(text: self.$password, isFirstResponder: self.$passwordTextFieldFirstResponder, onReturn: self.login)
                                    .frame(width: self.sceneModel.biggerScreen ? 400 : self.sceneModel.windowSize.width - 10)
                                StayLoginView(stayLogin: self.$stayLogin)
                                    .frame(width: self.sceneModel.biggerScreen ? 360 : self.sceneModel.windowSize.width - 30)
                                if (self.errorMessage != "") {
                                    Text(self.errorMessage)
                                    .offset(y: -10)
                                    .foregroundColor(.red)
                                }
                                LoginButtonView(login: self.login)
                            }
                            .padding(.vertical)
                            
                            Spacer()
                                .frame(height: 10)
                            
                            Text("A Maker Space project")
                                .multilineTextAlignment(.center)
                                .onTapGesture {
                                    self.presentMailCompose(mails: ["bkuapp@bkukr.de"], window: self.sceneModel.window)
                                }
                                .foregroundColor(.blue)
                            PersonalASAuthorizationControllerView(loginWithCrenditials: self.loginWithCrendentials).frame(height: 0)
                        }
                        .frame(minHeight: self.sceneModel.windowSize.height)
                    }
                }
                .navigationBarTitle ("welcome")
            //}
        }
    }
    
    /**
     Führt einen Login-Versuch aus.
     */
    func login() {
        checkLoginData()
    }
    
    /**
     Setzt das Passwort-Textfeld in den Fokus.
     */
    func focusPasswordTextField() {
        passwordTextFieldFirstResponder = true
    }
    
    /**
     Sendet die Nutzerdaten an die Apple Watch.
     */
    func sendDataToWatch() {
        connectivityHandler.sendLoginMessage(withUsername: self.username)
    }

    /**
     Versucht  den Nutzer mit den angegeben Zugangsdaten einzuloggen (Password-Autofill).
     
     - Parameters:
        - username: Der Nutzername des Nutzers, welcher sich einloggen will.
        - password: Das Passwort des Nutzer, welcher sich einloggen will.
     */
    func loginWithCrendentials(_ username: String, _ password: String) {
        self.username = username
        self.password = password
        login()
    }
    
    /**
     Zeigt die Tab-Ansicht, wenn das EInloggen erfolgreich war.
     */
    func showMainView() {
        sceneModel.initialLoaded = false
        withAnimation(.easeInOut) {
            ApplicationModel.shared.loggedIn = true
        }
    }
    
    /**
     Setzt die Kurzbefehleinträge für den Nutzer mit dem angegeben Benutzernamen.
     
     - Parameter username: Der Nutzername des Nutzers für den die Kurzbefehleinträge angelegt werden sollen.
     */
    func setShortcutItems(username: String) {
        // Stundenplan
        let scheduleShortcutItem = UIApplicationShortcutItem(type: "de.bkukr.BKU.schedule", localizedTitle: "schedule".localized, localizedSubtitle: nil, icon: UIApplicationShortcutIcon(systemImageName: "calendar"), userInfo: nil)
        // Vertretungen
        let representationShortcutItem = UIApplicationShortcutItem(type: "de.bkukr.BKU.representation", localizedTitle: "representations".localized, localizedSubtitle: nil, icon: UIApplicationShortcutIcon(systemImageName: "shuffle"), userInfo: nil)
        // Neuigkeiten
        let newsShortcutItem = UIApplicationShortcutItem(type: "de.bkukr.BKU.news", localizedTitle: "news".localized, localizedSubtitle: "", icon: UIApplicationShortcutIcon(systemImageName: "doc.plaintext"), userInfo: nil)
        UIApplication.shared.shortcutItems = [scheduleShortcutItem, representationShortcutItem, newsShortcutItem]
        
        // Person
        if (!UserDefaults.isTeacher) {
            let teacherListShortcutItem = UIApplicationShortcutItem(type: "de.bkukr.BKU.teacherlist", localizedTitle: "teacher list".localized, localizedSubtitle: nil, icon: UIApplicationShortcutIcon(systemImageName: "person.2.fill"), userInfo: nil)
            UIApplication.shared.shortcutItems?.insert(teacherListShortcutItem, at: 2)
            
            let roomListShortcutItem = UIApplicationShortcutItem(type: "de.bkukr.BKU.rooms", localizedTitle: "rooms".localized, localizedSubtitle: nil, icon: UIApplicationShortcutIcon(templateImageName: "RoomIcon"), userInfo: nil)
            UIApplication.shared.shortcutItems?.insert(roomListShortcutItem, at: 3)
        }
        else {
            let classListShortcutItem = UIApplicationShortcutItem(type: "de.bkukr.BKU.classlist", localizedTitle: "classes".localized, localizedSubtitle: nil, icon: UIApplicationShortcutIcon(systemImageName: "person.2.fill"), userInfo: nil)
            UIApplication.shared.shortcutItems?.insert(classListShortcutItem, at: 2)
        }

        // Klausur
        if (UserDefaults.isTGStudent) {
            let examPlanShortcutItem = UIApplicationShortcutItem(type: "de.bkukr.BKU.examplan", localizedTitle: "exam plan".localized, localizedSubtitle: nil, icon: UIApplicationShortcutIcon(systemImageName: "book.fill"), userInfo: nil)
            UIApplication.shared.shortcutItems?.insert(examPlanShortcutItem, at: 2)
        }
    }
    
    /**
     Überprüft die Login-Daten mit dem Server und schickt den Benachrichtigungstoken an den Server.
     */
    func checkLoginData() {
        let params: [String: Any] = [
            "username": self.username,
            "password": self.password
        ]

        // Login Daten Überprüfung
        self.sceneModel.showLoadingCircle = true
        JSONHandler.load(1, as: LoginResult.self, queryParams: params, sceneModel: self.sceneModel) { result in
            self.sceneModel.showLoadingCircle = false
            if (result.success) {
                // Momentan nicht beötigt, wpäter aber vielleicht irgendwann.
                UserDefaults.standard.set(result.name, forKey: "fullName")
                UserDefaults.shared?.set(self.username, forKey: "username")
                if (self.stayLogin) {
                    self.setShortcutItems(username: self.username)
                    UserDefaults.shared?.set(true, forKey: "loggedIn")
                }
                self.showMainView()
                self.sendDataToWatch()
            }
            else {
                self.show(errorMessage: result.error)
            }
        }
    }

    /**
     Zeigt die angegeben Fehlermeldung an, welche während des Login-Vorgangs aufgertreten ist.
     
     - Parameter message: Der Fehler, welcher aufgetreten ist.
     */
    func show(errorMessage message: String) {
        self.errorMessage = message
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(7)) {
            self.errorMessage = ""
        }
    }
}
