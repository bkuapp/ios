//
//  PrivacyView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die Nutzungsbedingungen und die Datenschutzerklärung zeigt.
 */
struct TermsAndPrivacyView: View {
    /**
     Ob die Nutzungsbedingungen und  Datenschutzerklärungen noch angezeigt werden sollen.
     */
    @Binding var termsAndPrivacyShown: Bool
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur enthält.
     */
    var body: some View {
        VStack {
            Text("if you want to use the app, please agree to the terms of use and the privacy policy.")
            PDFViewRepresentable(pdfFileName: "terms of use and privacy policy")
                .navigationBarTitle("terms of use and privacy", displayMode: .inline)
            Button(action: {
                self.termsAndPrivacyShown = true
                UserDefaults.standard.set(true, forKey: "terms of use and privacy policy shown")
            }) {
                Text("agree")
            }
            .padding(.vertical, 10)
        }
    }
}
