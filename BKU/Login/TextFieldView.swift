//
//  TextFieldView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Das Textfeld, welches sowohl als Passwort-Textfeld als auch als Nutzernamen-Textfeld eingesetzt.
 */
struct TextFieldView: View {
    /**
     Der Text, welcher aktuell in dem Textfeld steht.
     */
    @Binding var text: String
    /**
     Ob das Textfeld sicher ist (Password-Ansicht). Beim Nutzernamen-Textfeld ist es konstant nicht sicher, während es beim Password-Textfeld dynamisch ist.
     */
    @Binding var isSecure: Bool
    /**
     Ob das Textfeld gerade fokusiert ist.
     */
    @Binding var isFirstResponder: Bool
    
    /**
     Der Typ des Textfeldes (Nutzername oder Password), welcher für Autovervollständigung genutzt wird.
     */
    let contentType: UITextContentType
    /**
     Welchen Typ der Enter-Knopf hat (Weiter oder Öffnen).
     */
    let returnVal: UIReturnKeyType
    /**
     Der Platzhalter-Text, welcher angezeigt wird, wenn man noch nichts eingegeben hat.
     */
    let placeholder: String
    /**
     Die Methode, welche ausgeführt wird, wenn man auf den Enter-Knopf drückt.
     */
    let onReturn: () -> ()
    
    /**
     Der Körper des Textfeldes, welcher dessen Struktur speichert.
     */
    var body: some View {
        TextFieldRepresentable(contentType: contentType, returnVal: returnVal, placeholder: placeholder, onReturn: onReturn, isFirstResponder: $isFirstResponder, text: $text, isSecure: $isSecure)
            .padding()
            .background(Color.gray)
            .cornerRadius(5.0)
            .frame(height: 60)
    }
}
