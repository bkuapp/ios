//
//  PDFView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import PDFKit

/**
 Die Ansicht, welche ein PDF Dokument anzeigt.
 */
struct PDFViewRepresentable: UIViewRepresentable {
    /**
     Der Name der PDF-Datei, welche angezeigt werden soll.
     */
    let pdfFileName: String
    
    /**
     Erstellt die PDF-Ansicht, welche das PDF Dokument dem Nutzer anzeigt und gibt sie zurück.
     
     - Parameter context: Der Kontext der Ansicht.
     
     - Returns: Die erstellte PDF-Ansicht, welche das PDF Dokument dem Nutzer anzeigt.
     */
    func makeUIView(context: Context) -> PDFView {
        let pdfView = PDFView()
        
        if let path = Bundle.main.path(forResource: pdfFileName, ofType: "pdf"), let pdfDocument = PDFDocument(url: URL(fileURLWithPath: path)) {
            pdfView.displayMode = .singlePageContinuous
            pdfView.autoScales = true
            pdfView.displayDirection = .vertical
            pdfView.document = pdfDocument
        }
        return pdfView
    }
    
    /**
     Überarbeitet die PDF Ansicht.
     - Parameters:
        - uiView: Die PDF-Ansicht, welche überarbeitet werden soll.
        - context: Der Kontext der Ansicht, welche überarbeitet wird.
     */
    func updateUIView(_ uiView: PDFView, context: Context) {}
}
