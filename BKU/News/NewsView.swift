//
//  NewsView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WebKit

/**
 Die Ansicht, welche die Neuigkeiten-Webseite anzeigt.
 */
struct NewsView: View {
    /**
     Die Webseitenansicht, welche die Webseite anzeigt.
     */
    private let webView = WKWebView()
    /**
     Die Funktion, welche aufgerufen wird, wenn die Daten neugeladen werden sollen.
     */
    let reload: () -> ()
    
    /**
     Ob die Einstellungen aktuell gezeigt werden.
     */
    @Binding var showSettings: Bool
    
    /**
     Die URL, welche gerade gezeigt wird.
     */
    @State private var url = "https://bkukr.de?id=5"
    /**
     Ob der Zurück-Knopf gerade gezeigt wird.
     */
    @State private var showBack = false
    /**
     Ob der Nutzer gerade zurück blättern kann.
     */
    @State private var canGoBack = false
    /**
     Ob der Nutzer gerade vorwärts blättern kann.
     */
    @State private var canGoForward = false
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        WebView(view: self.webView, url: self.$url, showBack: self.$showBack, canGoBack: self.$canGoBack, canGoForward: self.$canGoForward)
        .navigationBarItems(leading: Group {
            if (showBack) {
                BackIconNavigationIcon(text: "back".localized) {
                    self.webView.goBack()
                }
            }
            else {
                HStack {
                    Spacer()
                    Image(systemName: "chevron.left")
                        .opacity(canGoBack ? 1 : 0.3)
                        .onTapGesture {
                            if (self.canGoBack) {
                                self.webView.evaluateJavaScript("document.getElementsByClassName('previous')[0].children[0].click()", completionHandler: nil)
                            }
                        }
                        .padding(.trailing)
                    Image(systemName: "chevron.right")
                        .opacity(canGoForward ? 1 : 0.3)
                        .onTapGesture {
                            if (self.canGoForward) {
                                self.webView.evaluateJavaScript("document.getElementsByClassName('next')[0].children[0].click()", completionHandler: nil)
                            }
                        }
                    Spacer()
                }
                .font(.system(size: 25))
            }
        }, trailing: StandardMenuIconsView(reload: reload, showSettings: $showSettings))
    }
}
