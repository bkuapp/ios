//
//  WebView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import WebKit
import SafariServices

/**
 Die Ansicht, welche eine Webseite anzeigt.
 */
struct WebView : UIViewRepresentable {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Die Webseitenansicht, welche die Webseite anzeigt.
     */
    let view: WKWebView
    /**
     Das Delegat, welches die Webseitenansicht überwacht.
     */
    private let delegate = WebDelegate()
    
    /**
     Die URL, welche momentan angezeigt werden soll.
     */
    @Binding var url: String
    /**
     Ob der Zurück-Knopf gezeigt werden soll.
     */
    @Binding var showBack: Bool
    /**
     Ob der Nutzer zurück blättern kann.
     */
    @Binding var canGoBack: Bool
    /**
     Ob der Nutzer nach vorne blättern kann.
     */
    @Binding var canGoForward: Bool
    
    /**
     Erstellt die Ansicht und gibt sie zurück.
     
     - Parameter context: Der Kontext der Ansicht, die erstellt wird.
     
     - Returns: Die erstellte Ansicht.
     */
    func makeUIView(context: Context) -> WKWebView  {
        delegate.parent = self
        delegate.sceneModel = sceneModel
        view.allowsLinkPreview = false
        view.allowsBackForwardNavigationGestures = false
        view.navigationDelegate = delegate
        return view
    }
    
    /**
     Überarbeitet die Ansicht.
     
     - Parameters:
        - uiView: Die Ansicht, welche überarbeitet werden soll.
        - context: Der Kontext der Ansicht, welche überarbeitet wird.
     */
    func updateUIView(_ uiView: WKWebView, context: Context) {
        if let url = URL(string: url) {
            uiView.load(URLRequest(url: url))
        }
    }
}

/**
 Das Delegat, welches die Webseiten Ansicht überwacht.
 */
class WebDelegate: NSObject, WKNavigationDelegate {
    /**
     Die Webseitenansicht, zu der das Delegat gehört.
     */
    var parent: WebView?
    /**
     Das aktuelle Model des Fensters, zu der die Ansicht des Delegates gehört.
     */
    var sceneModel: SceneModel?
    
    /**
     Wird aufgerufen, wenn der Nutzer eine Navigation gestartet hat, um zu bestimmen, ob die Seite dahinter geladen werden soll.
     
     - Parameters:
        - webView: Die Webseiten-Ansicht, bei der die Navigation gestartet wurde.
        - naviogationAction: Die Navigation, welche durchgeführt wurde.
        - decisionHandler: Die Methode, welche aufgerufen wird, um die Entscheidung zu übermitteln.
     */
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let request = navigationAction.request
        
        switch (navigationAction.navigationType) {
        case .linkActivated:
            if let url = request.url {
                let urlString = url.absoluteString
                if let id = getQueryStringParameter(url: urlString, param: "id") {
                    switch (id) {
                    case "36":
                        parent?.showBack = true
                        fallthrough
                    case "5":
                        parent?.url = url.absoluteString
                        decisionHandler(.allow)
                        return
                    default:
                        break
                    }
                }
                let webVC = SFSafariViewController(url: url)
                webVC.modalPresentationStyle = .popover
                sceneModel?.window?.rootViewController?.present(webVC, animated: true)
            }
            fallthrough
        case .formSubmitted:
            fallthrough
        case .formResubmitted:
            decisionHandler(.cancel)
        case .backForward:
            parent?.showBack = false
            fallthrough
        case .reload:
            fallthrough
        case .other:
            fallthrough
        @unknown default:
            if let url = request.url {
                parent?.url = url.absoluteString
            }
            decisionHandler(.allow)
        }
    }
    
    /**
     Wird aufgerufen, wenn eine Navigation beendet wurde (Seite wurde vollständig geladen). Entfernt Kopf- und Fußzeilen von der Seite und bestimmt, ob vor oder zurück blättern möglich ist.
     
     - Parameters:
        - webView: Die Webseiten-Ansicht, bei welcher die Navigation beendet wurde.
        - navigation: Die Navigation, welche beendet wurde.
     */
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation?) {
        webView.evaluateJavaScript("const headers = document.getElementsByClassName('cs-header-top page-top');" +
        "if (headers.length != 0) headers[0].remove();" +
        "const logoHeader = document.getElementsByClassName('main-header bg-top page-top bg-color');" +
        "if (logoHeader.length != 0) logoHeader[0].remove();" +
        "const breadcrumb = document.getElementsByClassName('breadcrumb');" +
        "if (breadcrumb.length != 0) breadcrumb[0].remove();" +
        "const footer = document.getElementsByClassName('cs-footer-area cs-default-overlay animatedParent animateOnce');" +
        "if (footer.length != 0) footer[0].remove();" +
        "const copyrightFooter = document.getElementsByClassName('cs-copyright-area');" +
        "if (copyrightFooter.length != 0) copyrightFooter[0].remove();" +
        "const title = document.getElementsByTagName('header');" +
        "if (title.length != 0) title[0].remove();" +
        "const cookieHeader = document.getElementById('cc-notification');" +
        "if (cookieHeader != null) cookieHeader.remove();" +
        "const privacySettings = document.getElementById('cc-tag');" +
        "if (privacySettings != null) privacySettings.remove();", completionHandler: nil)
        
        webView.evaluateJavaScript("document.getElementsByClassName('previous').length != 0", completionHandler: { canGoBack, error in
            self.parent?.canGoBack = (canGoBack as? Bool) ?? false
        })
        
        webView.evaluateJavaScript("document.getElementsByClassName('next').length != 0", completionHandler: { canGoForward, error in
            self.parent?.canGoForward = (canGoForward as? Bool) ?? false
        })
    }
    
    /**
     Gibt einen Parameter aus dem HTTP-Anfrage-Parameter-String zurück.
     
     - Parameters:
        - url: Die URL, aus dem der Parameter zurückgegeben werden soll.
        - param: Der Parameter, welcher zurückgegeben werden soll.
     
     - Returns: Einen Parameter aus dem HTTP-Anfrage-Parameter-String
     */
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
}
