//
//  NotificationSettingsView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht, welche die Benachrichtigungseinstellungen anzeigt.
 */
struct NotificationSettingsView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Ob die Klausurbenachrichtigungen im Moment aktiviert sind.
     */
    @State private var examNotificationEnabled = UserDefaults.standard.bool(forKey: "examNotification")
    // @State private var homeworkNotificationEnabled = UserDefaults.standard.bool (forKey: "homeWorkNotification")
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        Group {
            Toggle(isOn: Binding<Bool>(get: {
                return UserDefaults.standard.bool (forKey: "representationNotification")
            }, set: { value in
                if (value) {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                else {
                    UIApplication.shared.unregisterForRemoteNotifications()
                }
                UserDefaults.standard.set(value, forKey: "representationNotification")
            })) {
                Text("representation-notification")
            }
            
            /*Toggle(isOn: Binding<Bool>(get: {
                return UserDefaults.standard.bool(forKey: "tutoringNotification")
            }, set: { value in
                UserDefaults.standard.set(value, forKey: "tutoringNotification")
            })) {
                Text("tutoring-notification")
            }*/
            
            if (UserDefaults.isTGStudent) {
                Toggle(isOn: Binding<Bool>(get: {
                    return self.examNotificationEnabled
                }, set: { value in
                    UserDefaults.standard.set(value, forKey: "examNotification")
                    withAnimation {
                        self.examNotificationEnabled.toggle()
                    }
                    
                    if (value) {
                        NotificationHandler.scheduleExamNotification(sceneModel: self.sceneModel)
                    }
                    else {
                        NotificationHandler.deleteAllExamNotification()
                    }
                })) {
                    Text("exam-notification")
                }
                if (examNotificationEnabled) {
                    NotificationTimePickerView(timeKey: "examNotificationTime")
                }
            }
            
            /*Toggle(isOn: Binding<Bool>(get: {
                return self.homeworkNotificationEnabled
            }, set: { value in
                UserDefaults.standard.set(value, forKey: "homeWorkNotification")
                withAnimation{
                    self.homeworkNotificationEnabled.toggle()
                }
            })) {
                Text("homework-notification")
            }
            if (homeworkNotificationEnabled) {
                NotificationTimePicker(timeKey: "homeWorkNotificationTime")
            }*/
        }
    }
}
