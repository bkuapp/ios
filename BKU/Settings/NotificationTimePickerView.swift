//
//  NotificationTimePickerView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI

/**
 Die Ansicht der Benachrichtigungs-Zeitauswahl.
 */
struct NotificationTimePickerView: View {
    /**
     Das aktuelle Model des Fensters, zu der diese Ansicht gehört.
     */
    @EnvironmentObject private var sceneModel: SceneModel
    
    /**
     Der Nutzerstandards-Schlüssel für die Benachrichtigungszeit
     */
    var timeKey: String
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        Picker("notification time", selection: Binding<String>(
            get: {
                UserDefaults.standard.string(forKey: self.timeKey) ?? "1d"
            }, set: { value in
                UserDefaults.standard.set(value, forKey: self.timeKey)
                if (self.timeKey == "examNotificationTime") {
                    NotificationHandler.scheduleExamNotification(sceneModel: self.sceneModel)
                }
            })) {
        Text("1 hour before").tag("1h")
        Text("3 hours before").tag("3h")
        Text("6 hours before").tag("6h")
        Text("12 hours before").tag("12h")
        Text("1 day before").tag("1d")
        Text("3 days before").tag("3d")
        Text("7 days before").tag("7d")
        }
    }
}
