//
//  GeneralSettingsView.swift
//  BKU
//
//  Copyright (C) 2020  Sven Op de Hipt
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

import SwiftUI
import LocalAuthentication

/**
 Die Ansicht, welche die Algemeinen Einstellungen anzeigt.
 */
struct GeneralSettingsView: View {
    /**
     Der biometrische Authentifizierungstyp des Gerätes.
     */
    private let biometryType = LAContext().biometricType
    
    /**
     Ob die Authentifikation im Moment aktviert ist.
     */
    @State private var authEnabled = UserDefaults.standard.bool(forKey: "authentication")
    
    /**
     Der Körper der Ansicht, welcher dessen Struktur beinhaltet.
     */
    var body: some View {
        Group {
            if (LAContext().biometricType != .none) {
                Toggle(isOn: Binding<Bool>(get: {
                    return self.authEnabled
                }, set: { value in
                    self.authEnabled = value
                    if (value) {
                        AuthenticationView.useAuthentication(onSuccess: {
                            UserDefaults.standard.set(true, forKey: "authentication")
                        }, onFail: {
                            self.authEnabled = false
                            UserDefaults.standard.set(false, forKey: "authentication")
                        })
                    }
                    else {
                        UserDefaults.standard.set(false, forKey: "authentication")
                    }
                }))
                {
                    Group {
                        if (LAContext().biometricType == .touchID) {
                            Text("TouchID")
                        }
                        else if (LAContext().biometricType == .faceID) {
                            Text("FaceID")
                        }
                    }
                }
            }
        }
    }
}
